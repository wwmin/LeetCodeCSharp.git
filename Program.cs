﻿using Microsoft.Extensions.Configuration;

#region 初始化
#if false
//方法一: 直接明文保存LEETCODE_SESSION
// var LEETCODE_SESSION = "";
#endif
// 使用本地机密文件secrets.json 然后从机密中读取登录信息, 此方法更好的保护自己的账号
//方法二: 使用vs IDE
//步骤1: 添加依赖 Microsoft.Extensions.Configuration
//步骤2: vs中右键项目然后选择管理用户机密,然后在打开的secrets.json中添加
/*
{
  "cookie_login": {
    "LEETCODE_SESSION": "your leetcode_session cookie",
  }
}
*/
//方法三: 使用dotnet cli
//步骤1: 添加依赖 Microsoft.Extensions.Configuration
//步骤2: 启用机密,使用.net cli命令 `dotnet user-secrets init`
//步骤3: 设置机密, `dotnet user-secrets set "cookie_login:LEETCODE_SESSION" "your cookie"`
//步骤4: 查看 `dotnet user-secrets list`
// 更多dotnet cli 可参考:https://docs.microsoft.com/zh-cn/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows
var config = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddUserSecrets<Program>().Build();
var LEETCODE_SESSION = config.GetSection("cookie_login:LEETCODE_SESSION")?.Value;
if (!string.IsNullOrEmpty(LEETCODE_SESSION)) await LeetCodeHelper.InitAsync(LEETCODE_SESSION);
else throw new Exception("请传入LEETCODE_SESSION");
#endregion

//1. 传入url或titleSlug获取题目
//2. 传入题号获取题目
//3. 参数为空时获取当天的每日一题
//如果已获取题目则会自动开始测试
await LeetCodeHelper.Run();

// Console.Write("按任意键退出...");
// Console.ReadKey();