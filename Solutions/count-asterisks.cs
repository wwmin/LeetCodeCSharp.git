namespace Solutions;
/// <summary>
/// 2315. 统计星号
/// difficulty: Easy
/// https://leetcode.cn/problems/count-asterisks/
/// </summary>
public class CountAsterisks_2315_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "l|*e*et|c**o|*de|", 2 };
            yield return new object[] { "iamprogrammer", 0 };
            yield return new object[] { "yo|uar|e**|b|e***au|tifu|l", 5 };
        }
    }

    [Data]
    public int CountAsterisks(string s)
    {
        bool isEven = false;
        int count = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '|') isEven = !isEven;
            else if (isEven == false && s[i] == '*') count++;
        }
        return count;
    }
}