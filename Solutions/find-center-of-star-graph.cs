namespace Solutions;
/// <summary>
/// 1916. 找出星型图的中心节点
/// </summary>
public class FindCenterSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[2,3],[4,2]]").ToArray(), 2 };
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[5,1],[1,3],[1,4]]").ToArray(), 1 };
        }
    }

    [Data]
    public int FindCenter(int[][] edges)
    {
        int a1 = edges[0][0];
        int a2 = edges[0][1];
        int b1 = edges[1][0];
        int b2 = edges[1][1];
        if (a1 == b1) return a1;
        if (a1 == b2) return a1;
        return a2;
    }
}