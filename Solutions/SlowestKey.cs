namespace Solutions;
/// <summary>
/// 1751. 按键持续时间最长的键
/// </summary>
public class SlowestKeySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[9,29,49,50]").ToArray(), "cbcd", 'c' };
            yield return new object[] { StringTo<int[]>("[12,23,36,46,62]").ToArray(), "spuda", 'a' };
        }
    }

    [Data]
    public char SlowestKey(int[] releaseTimes, string keysPressed)
    {
        char ans = keysPressed[0];
        int maxTime = releaseTimes[0];
        for (int i = 1; i < keysPressed.Length; i++)
        {
            int t = releaseTimes[i] - releaseTimes[i - 1];
            if (t > maxTime || (t == maxTime && ans < keysPressed[i]))
            {
                maxTime = t;
                ans = keysPressed[i];
            }
        }
        return ans;
    }
}