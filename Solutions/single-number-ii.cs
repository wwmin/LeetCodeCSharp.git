namespace Solutions;
/// <summary>
/// 137. 只出现一次的数字 II
/// https://leetcode.cn/problems/single-number-ii/
/// </summary>
public class SingleNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[2,2,3,2]").ToArray(), 3};
            yield return new object[]{StringTo<int[]>("[0,1,0,1,0,1,99]").ToArray(), 99};
        }
    }

    [Data]
    public int SingleNumber(int[] nums)
    {
        Dictionary<int,int> dic = new  Dictionary<int, int>();

        for(int i = 0; i < nums.Length; i++)
        {
            if(dic.ContainsKey(nums[i]))dic[nums[i]]++;
            else dic.Add(nums[i],1);
        }
        var keys = dic.Keys.ToArray();
        for (int i = 0; i < keys.Length; i++)
        {
            var key = keys[i];
            if(dic[key] ==1)return key;
        }
        return default;
    }
}