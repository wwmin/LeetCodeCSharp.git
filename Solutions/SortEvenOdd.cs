﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6000. 对奇偶下标分别排序
    /// </summary>
    public class Class1Sulution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[4,1,2,3]"), StringTo<int[]>("[2,3,4,1]") };
                yield return new object[] { StringTo<int[]>("[2,1]"), StringTo<int[]>("[2,1]") };
            }
        }
        [Data]
        public int[] SortEvenOdd(int[] nums)
        {
            List<int> eventList = new List<int>();
            List<int> oddList = new List<int>();
            for (int i = 0; i < nums.Length; i++)
            {
                if (i % 2 == 0)
                {
                    eventList.Add(nums[i]);
                }
                else
                {
                    oddList.Add(nums[i]);
                }
            }
            eventList.Sort();
            oddList.Sort();
            oddList.Reverse();
            for (int i = 0; i < nums.Length; i++)
            {
                if (i % 2 == 0)
                {
                    if (eventList.Count == 0) continue;
                    nums[i] = eventList[0];
                    eventList.RemoveAt(0);
                }
                else
                {
                    if (oddList.Count == 0) continue;
                    nums[i] = oddList[0];
                    oddList.RemoveAt(0);
                }
            }

            return nums;
        }
    }
}