﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    public class TwoSumSolution
    {
        Dictionary<int, int> map = new Dictionary<int, int>();
        public TwoSumSolution()
        {

        }

        public void Add(int number)
        {
            if (!map.ContainsKey(number)) map.Add(number, 1);
            else map[number]++;
        }

        public bool Find(int value)
        {
            var keys = map.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                int ele = value - keys[i];
                if (ele != keys[i])
                {
                    if (map.ContainsKey(ele))
                    {
                        return true;
                    }
                }
                else
                {
                    if (map[keys[i]] > 1) return true;
                }
            }
            return false;
        }
    }

    /**
     * Your TwoSum object will be instantiated and called as such:
     * TwoSum obj = new TwoSum();
     * obj.Add(number);
     * bool param_2 = obj.Find(value);
     */
}