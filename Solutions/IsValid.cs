﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 20. 有效的括号
/// </summary>
public class IsValidSolution
{
    private Dictionary<char, char> brackets = new Dictionary<char, char>
        {
            {'(',')'},
            {'{', '}'},
            {'[',']'}
        };
    //[InlineData("()", true)]
    //[InlineData("()[]{}", true)]
    //[InlineData("(]", false)]
    [InlineData("[", false)]
    public bool IsValid(string s)
    {
        Stack<char> res = new Stack<char>();
        for (int i = 0; i < s.Length; i++)
        {
            if (res.Count == 0)
            {
                if (brackets.ContainsKey(s[i])) res.Push(s[i]);
                else return false;
            }
            else
            {
                if (brackets.ContainsKey(s[i]))
                {
                    res.Push(s[i]);
                }
                else
                {
                    var lastChar = res.Peek();
                    var mc = brackets[lastChar];
                    if (mc == s[i]) res.Pop();
                    else return false;
                }
            }
        }
        return res.Count == 0;
    }
}