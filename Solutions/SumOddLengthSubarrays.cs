﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 1588. 所有奇数长度子数组的和
/// </summary>
public class SumOddLengthSubarraysSolution
{
    [InlineData(new int[] { 1, 4, 2, 5, 3 }, 58)]
    public int SumOddLengthSubarrays(int[] arr)
    {
        int sum = 0;
        int n = arr.Length;
        for (int start = 0; start < n; start++)
        {
            for (int length = 1; start + length <= n; length += 2)
            {
                int end = start + length;
                sum += arr[start..end].Sum();
            }
        }
        return sum;
    }
}
