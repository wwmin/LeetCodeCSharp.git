namespace Solutions;
/// <summary>
/// 951. 分割数组
/// https://leetcode.cn/problems/partition-array-into-disjoint-intervals/
/// </summary>
public class PartitionDisjointSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[5,0,3,8,6]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[1,1,1,0,6,12]").ToArray(), 4 };
        }
    }

    [Data]
    public int PartitionDisjoint(int[] nums)
    {
        int n = nums.Length;
        int[] minRight = new int[n];
        minRight[n - 1] = nums[n - 1];
        for (int i = n - 2; i >= 0; i--)
        {
            minRight[i] = Math.Min(nums[i], minRight[i + 1]);
        }
        int maxLeft = 0;
        for (int i = 0; i < n - 1; i++)
        {
            maxLeft = Math.Max(maxLeft, nums[i]);
            if (maxLeft <= minRight[i + 1])
            {
                return i + 1;
            }
        }
        return n - 1;
    }
}