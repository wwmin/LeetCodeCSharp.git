namespace Solutions;
/// <summary>
/// 496. 下一个更大元素 I
/// </summary>
public class NextGreaterElementSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,1,2]").ToArray(), StringTo<int[]>("[1,3,4,2]").ToArray(), StringTo<int[]>("[-1,3,-1]").ToArray() };
            yield return new object[] { StringTo<int[]>("[2,4]").ToArray(), StringTo<int[]>("[1,2,3,4]").ToArray(), StringTo<int[]>("[3,-1]").ToArray() };
        }
    }

    [Data]
    public int[] NextGreaterElement(int[] nums1, int[] nums2)
    {
        int[] res = new int[nums1.Length];
        for (int i = 0; i < nums1.Length; i++)
        {
            bool find = false;
            bool end = true;
            for (int j = 0; j < nums2.Length; j++)
            {
                if (find == false)
                {
                    if (nums1[i] == nums2[j]) find = true;
                    else continue;
                }
                else
                {
                    if (nums2[j] > nums1[i])
                    {
                        res[i] = nums2[j];
                        end = false;
                        break;
                    }
                }
            }
            if (end == true)
            {
                res[i] = -1;
            }
        }
        return res;
    }
}