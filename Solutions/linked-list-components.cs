namespace Solutions;
/// <summary>
/// 835. 链表组件
/// https://leetcode.cn/problems/linked-list-components/
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class NumComponentsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[0,1,2,3]"), StringTo<int[]>("[0,1,3]").ToArray(), 2 };
            yield return new object[] { ToListNodeWithString("[0,1,2,3,4]"), StringTo<int[]>("[0,3,1,4]").ToArray(), 2 };
        }
    }
    [Data]
    public int NumComponents(ListNode head, int[] nums)
    {
        //Dictionary<int, bool> dic = new Dictionary<int, bool>();
        //使用HashSet代替字典值浪费内存空间问题
        HashSet<int> dic = new HashSet<int>();
        for (int i = 0; i < nums.Length; i++)
        {
            dic.Add(nums[i]);
        }
        int res = 0;
        bool isLinked = false;
        while (head != null)
        {
            if (isLinked)
            {
                if (!dic.Contains(head.val))
                {
                    isLinked = false;
                }
            }
            else if (isLinked == false)
            {
                if (dic.Contains(head.val))
                {
                    res++;
                    isLinked = true;
                }
            }
            head = head.next;
        }
        return res;
    }
}