namespace Solutions;
/// <summary>
/// 2185. 统计包含给定前缀的字符串
/// difficulty: Easy
/// https://leetcode.cn/problems/counting-words-with-a-given-prefix/
/// </summary>
public class PrefixCount_2185_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"pay\",\"attention\",\"practice\",\"attend\"]").ToArray(), "at", 2 };
            yield return new object[] { StringTo<string[]>("[\"leetcode\",\"win\",\"loops\",\"success\"]").ToArray(), "code", 0 };
        }
    }

    [Data]
    public int PrefixCount(string[] words, string pref)
    {
        var count = 0;
        foreach (var word in words)
        {
            if (word.StartsWith(pref)) count++;
        }
        return count;
    }
}