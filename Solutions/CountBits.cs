namespace Solutions;
/// <summary>
/// 338. 比特位计数
/// </summary>
public class CountBitsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, StringTo<int[]>("[0,1,1]").ToArray() };
            yield return new object[] { 5, StringTo<int[]>("[0,1,1,2,1,2]").ToArray() };
        }
    }

    [Data]
    public int[] CountBits1(int n)
    {
        int[] ans = new int[n + 1];
        for (int i = 0; i <= n; i++)
        {
            ans[i] = countOnes(i);
        }

        return ans;
    }
    //Brian Kernighan 算法 
    //核心思想 x&(x-1) 会将x二进位的最后一个1变成0
    private int countOnes(int x)
    {
        int ones = 0;
        while (x > 0)
        {
            x &= (x - 1);
            ones++;
        }
        return ones;
    }
    [Data]
    public int[] countBits(int n)
    {
        int[] bits = new int[n + 1];
        int highBit = 0;
        for (int i = 1; i <= n; i++)
        {
            if ((i & (i - 1)) == 0)
            {
                highBit = i;
            }
            bits[i] = bits[i - highBit] + 1;
        }
        return bits;
    }
}