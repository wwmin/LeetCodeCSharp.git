namespace Solutions;
/// <summary>
/// 1814. 统计一个数组中好对子的数目
/// difficulty: Medium
/// https://leetcode.cn/problems/count-nice-pairs-in-an-array/
/// </summary>
public class CountNicePairs_1814_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[42,11,1,97]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[13,10,35,24,76]").ToArray(), 4 };
        }
    }

    [Data]
    public int CountNicePairs(int[] nums)
    {
        const int MOD = 1000000007;
        int res = 0;
        IDictionary<int, int> h = new Dictionary<int, int>();
        foreach (int i in nums)
        {
            int temp = i, j = 0;
            while (temp > 0)
            {
                j = j * 10 + temp % 10;
                temp /= 10;
            }
            h.TryAdd(i - j, 0);
            res = (res + h[i - j]) % MOD;
            h[i - j]++;
        }
        return res;
    }
}