namespace Solutions;
/// <summary>
/// 461. 汉明距离
/// </summary>
public class HammingDistanceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, 4, 2 };
            yield return new object[] { 3, 1, 1 };
        }
    }

    [Data]
    public int HammingDistance(int x, int y)
    {
        int ans = 0;
        int z = x ^ y;
        while (true)
        {
            if (z == 0) break;
            z = z & (z - 1);
            ans++;
        }
        return ans;
    }
}