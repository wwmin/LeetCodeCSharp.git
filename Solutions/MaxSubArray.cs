﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 53. 最大子序和
/// </summary>
public class MaxSubArraySolution
{
    [InlineData(new int[] { 2, -1, 3, -1 }, 4)]
    [InlineData(new int[] { -1, -2, -3, 0 }, 0)]
    [InlineData(new int[] { -2, -1, -2 }, -1)]
    [InlineData(new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 }, 6)]
    [InlineData(new int[] { -1, 0, -2 }, 0)]
    [InlineData(new int[] { 1, 2, -1, -2, 2, 1, -2, 1, 4, -5, 4 }, 6)]
    public int MaxSubArray(int[] nums)
    {
        int n = nums.Length;
        if (n == 1) return nums[0];
        int pre = 0, maxAns = nums[0];
        foreach (var x in nums)
        {
            pre = Math.Max(pre + x, x);
            maxAns = Math.Max(pre, maxAns);
        }
        return maxAns;
    }
}
