﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    public class SubStrHashSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                //yield return new object[] { "leetcode", 7, 20, 2, 0, "ee" };
                //yield return new object[] { "fbxzaad", 31, 100, 3, 32, "fbx" };
                //yield return new object[] { "xmmhdakfursinye", 96, 45, 15, 21, "xmmhdakfursinye" };
                //yield return new object[] { "xqgfatvtlwnnkxipmipcpqwbxihxblaplpfckvxtihonijhtezdnkjmmk", 22, 51, 41, 9, "xqgfatvtlwnnkxipmipcpqwbxihxblaplpfckvxti" };
                yield return new object[] { "cbmzzngpnfyzoexfnzxhhyvzxibaijgfvaleowaqjllkgoercyiptkugzgcxobn", 83, 56, 27, 23, "hyvzxibaijgfvaleowaqjllkgoe" };
            }
        }
        //出错了
        [Data]
        public string SubStrHash(string s, int power, int modulo, int k, int hashValue)
        {
            int n = s.Length;
            int leftK = n - k;
            for (int i = 0; i < leftK; i++)
            {
                string sub = s.Substring(i, k);
                int h = hash(sub, power, modulo);
                if (h == hashValue) return sub;
            }
            return s.Substring(0, k);
        }

        private int hash(string sub, int power, int modulo)
        {
            int n = sub.Length;
            double sum = 0;
            for (int i = 0; i < n; i++)
            {
                int si = sub[i] - 'a' + 1;
                sum += (si * Math.Pow(power, i)) % modulo;
            }
            return (int)(sum % modulo);
        }
    }
}