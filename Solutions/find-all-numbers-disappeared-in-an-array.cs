namespace Solutions;
/// <summary>
/// 448. 找到所有数组中消失的数字
/// </summary>
public class FindDisappearedNumbersSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,3,2,7,8,2,3,1]").ToArray(), StringTo<List<int>>("[5,6]") };
            yield return new object[] { StringTo<int[]>("[1,1]").ToArray(), StringTo<List<int>>("[2]") };
        }
    }

    [Data]
    public IList<int> FindDisappearedNumbers(int[] nums)
    {
        int n = nums.Length;
        IList<int> ans = new List<int>();
        int[] nums_correct = new int[n];
        for (int i = 0; i < n; i++)
        {
            nums_correct[nums[i] - 1] = 1;
        }
        for (int i = 0; i < n; i++)
        {
            if (nums_correct[i] == 0) ans.Add(i + 1);
        }
        return ans;
    }
}