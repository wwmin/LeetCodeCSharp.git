﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 52. N皇后 II
/// </summary>
public class TotalNQueensSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, 2 };
        }
    }
    [Data]
    public int TotalNQueens(int n)
    {
        int sum = 0;
        int[] queens = new int[n];
        Array.Fill(queens, -1);
        HashSet<int> columns = new HashSet<int>();
        HashSet<int> diagonals1 = new HashSet<int>();
        HashSet<int> diagonals2 = new HashSet<int>();
        backtrack(queens, n, 0, columns, diagonals1, diagonals2, ref sum);
        return sum;
    }

    private void backtrack(int[] queens, int n, int row, HashSet<int> columns, HashSet<int> diagonals1, HashSet<int> diagonals2, ref int sum)
    {
        if (row == n)
        {
            sum += 1;
            return;
        }
        for (int i = 0; i < n; i++)
        {
            if (columns.Contains(i)) continue;
            int diagonal1 = row - i;
            if (diagonals1.Contains(diagonal1)) continue;
            int diagonal2 = row + i;
            if (diagonals2.Contains(diagonal2)) continue;
            queens[row] = i;
            columns.Add(i);
            diagonals1.Add(diagonal1);
            diagonals2.Add(diagonal2);
            backtrack(queens, n, row + 1, columns, diagonals1, diagonals2, ref sum);
            columns.Remove(i);
            diagonals1.Remove(diagonal1);
            diagonals2.Remove(diagonal2);
            queens[row] = -1;
        }
    }
}