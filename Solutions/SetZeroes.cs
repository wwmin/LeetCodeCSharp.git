﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 73. 矩阵置零
/// </summary>
public class SetZeroesSolution
{
    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,1,1],[1,0,1],[1,1,1]]").ToArray(), StringTo<List<int[]>>("[[1,0,1],[0,0,0],[1,0,1]]").ToArray() };
        }
    }

    [Data]
    public void SetZeroes(int[][] matrix)
    {
        int m = matrix.Length;
        int n = matrix[0].Length;
        bool flagCol0 = false, flagRow0 = false;
        for (int i = 0; i < m; i++)
        {
            if (matrix[i][0] == 0) flagCol0 = true;
        }
        for (int j = 0; j < n; j++)
        {
            if (matrix[0][j] == 0) flagRow0 = true;
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (matrix[i][j] == 0) matrix[i][0] = matrix[0][j] = 0;
            }
        }

        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++)
            {
                if (matrix[i][0] == 0 || matrix[0][j] == 0) matrix[i][j] = 0;
            }
        }
        if (flagCol0)
        {
            for (int i = 0; i < m; i++)
            {
                matrix[i][0] = 0;
            }
        }
        if (flagRow0)
        {
            for (int j = 0; j < n; j++)
            {
                matrix[0][j] = 0;
            }
        }
    }

    //空间复杂度为O(m+n)
    //[Data]
    public void SetZeroes2(int[][] matrix)
    {
        int m = matrix.Length;
        int n = matrix[0].Length;
        bool[] row = new bool[m];
        bool[] col = new bool[n];
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (matrix[i][j] == 0) row[i] = col[j] = true;
            }
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (row[i] || col[j]) matrix[i][j] = 0;
            }
        }
    }
}
