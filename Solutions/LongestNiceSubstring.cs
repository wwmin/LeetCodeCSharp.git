namespace Solutions;
/// <summary>
/// 1873. 最长的美好子字符串
/// </summary>
public class LongestNiceSubstringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "YazaAay", "aAa" };
            yield return new object[] { "Bb", "Bb" };
            yield return new object[] { "c", "" };
            yield return new object[] { "dDzeE", "dD" };
        }
    }

    [Data]
    public string LongestNiceSubstring(string s)
    {
        string ans = "";
        for (int i = 0; i < s.Length; i++)
        {
            for (int j = i + 2; j <= s.Length; j++)
            {

                string sub = s.Substring(i, j - i);
                if (isLongestSubString(sub))
                {
                    if (ans.Length < sub.Length) ans = sub;
                }
            }
        }

        return ans;
    }
    private bool isLongestSubString(string s)
    {
        Dictionary<char, char> map_a = new Dictionary<char, char>();
        Dictionary<char, char> map_A = new Dictionary<char, char>();
        foreach (char c in s)
        {
            if (c >= 'a' && c <= 'z')
            {
                if (!map_a.ContainsKey(c)) map_a.Add(c, (char)(c - 32));
            }
            else
            {
                if (!map_A.ContainsKey(c)) map_A.Add(c, (char)(c + 32));
                else map_A[c]++;
            }
        }
        var keys1 = map_a.Keys.ToArray();
        var keys2 = map_A.Keys.ToArray();
        if (keys1.Length != keys2.Length) return false;
        foreach (var key in keys1)
        {
            if (!map_A.ContainsKey(map_a[key]))
            {
                return false;
            }
            else
            {
                map_A.Remove(map_a[key]);
            }
        }
        if (map_A.Count > 0) return false;
        return true;
    }
}