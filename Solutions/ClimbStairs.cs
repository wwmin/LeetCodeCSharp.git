﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 70. 爬楼梯
/// </summary>
public class ClimbStairsSolution
{
    //[InlineData(1, 1)]
    //[InlineData(2, 2)]
    //[InlineData(3, 3)]
    [InlineData(4, 5)]
    [InlineData(5, 12)]
    public int ClimbStairs(int n)
    {
        if (n == 1) return 1;
        if (n == 2) return 2;
        int a = 1, b = 2;
        int temp;
        //第n个台阶只能从第n-1或者n-2个上来,所以 第n个台阶 = 第n-1个台阶+第n-2个台阶 走法的总和
        for (int i = 3; i <= n; i++)
        {
            temp = a;
            a = b;
            b = temp + b;
        }
        return b;
    }
}
