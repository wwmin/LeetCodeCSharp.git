namespace Solutions;
/// <summary>
/// 1888. 找到最近的有相同 X 或 Y 坐标的点
/// difficulty: Easy
/// https://leetcode.cn/problems/find-nearest-point-that-has-the-same-x-or-y-coordinate/
/// </summary>
public class NearestValidPointSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 4, StringTo<List<int[]>>("[[1,2],[3,1],[2,4],[2,3],[4,4]]").ToArray(), 2 };
            yield return new object[] { 3, 4, StringTo<List<int[]>>("[[3,4]]").ToArray(), 0 };
            yield return new object[] { 3, 4, StringTo<List<int[]>>("[[2,3]]").ToArray(), -1 };
        }
    }

    [Data]
    public int NearestValidPoint(int x, int y, int[][] points)
    {
        var min_len = int.MaxValue;
        var ans = -1;
        for (int i = 0; i < points.Length; i++)
        {
            if (points[i][0] == x || points[i][1] == y)
            {
                var len = Math.Abs(points[i][0] - x) + Math.Abs(points[i][1] - y);
                if (len < min_len)
                {
                    min_len = len;
                    ans = i;
                }
                else if (len == min_len)
                {
                    ans = Math.Min(ans, i);
                }
            }
        }
        return ans;
    }
}