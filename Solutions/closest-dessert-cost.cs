namespace Solutions;
/// <summary>
/// 1774. 最接近目标价格的甜点成本
/// difficulty: Medium
/// https://leetcode.cn/problems/closest-dessert-cost/
/// </summary>
public class ClosestCost_1774_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,7]").ToArray(), StringTo<int[]>("[3,4]").ToArray(), 10, 10 };
            yield return new object[] { StringTo<int[]>("[2,3]").ToArray(), StringTo<int[]>("[4,5,100]").ToArray(), 18, 17 };
            yield return new object[] { StringTo<int[]>("[3,10]").ToArray(), StringTo<int[]>("[2,5]").ToArray(), 9, 8 };
            yield return new object[] { StringTo<int[]>("[10]").ToArray(), StringTo<int[]>("[1]").ToArray(), 1, 10 };
        }
    }
    int res;

    [Data]
    public int ClosestCost(int[] baseCosts, int[] toppingCosts, int target)
    {
        res = baseCosts.Min();
        foreach (var item in baseCosts)
        {
            DFS(toppingCosts, 0, item, target);
        }
        return res;
    }

    void DFS(int[] toppingCosts, int p, int curCost, int target)
    {
        if (Math.Abs(res - target) < curCost - target)
        {
            return;
        }
        else if (Math.Abs(res - target) >= Math.Abs(curCost - target))
        {
            if (Math.Abs(res - target) > Math.Abs(curCost - target))
            {
                res = curCost;
            }
            else
            {
                res = Math.Min(res, curCost);
            }
        }
        if (p == toppingCosts.Length)
        {
            return;
        }
        DFS(toppingCosts, p + 1, curCost + toppingCosts[p] * 2, target);
        DFS(toppingCosts, p + 1, curCost + toppingCosts[p], target);
        DFS(toppingCosts, p + 1, curCost, target);
    }
}