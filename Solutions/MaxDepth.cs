namespace Solutions;
/// <summary>
/// 774. N 叉树的最大深度
/// </summary>
/*
// Definition for a Node.
public class Node {
    public int val;
    public IList<Node> children;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, IList<Node> _children) {
        val = _val;
        children = _children;
    }
}
*/

public class MaxDepthSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToNodeWithString("[]"), 0 };
            yield return new object[] { ToNodeWithString("[44]"), 1 };
            yield return new object[] { ToNodeWithString("[3,null,1,null,5]"), 3 };
            yield return new object[] { ToNodeWithString("[1,null,3,2,4,null,5,6]"), 3 };
            yield return new object[] { ToNodeWithString("[1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]"), 5 };
        }
    }
    private int ans = 0;
    [Data]
    public int MaxDepth(Node root)
    {
        if (root == null) return 0;
        dfs(root, 0);
        return ans;
    }

    private void dfs(Node node, int dept)
    {
        dept++;
        if (node == null || node.children == null || node.children.Count == 0)
        {
            ans = Math.Max(ans, dept);
            return;
        }
        foreach (var node_child in node.children)
        {
            dfs(node_child, dept);
        }
    }
}