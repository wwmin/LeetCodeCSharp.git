namespace Solutions;
/// <summary>
/// 100162. 字符串轮转
/// https://leetcode.cn/problems/string-rotation-lcci/
/// </summary>
public class IsFlipedStringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "waterbottle", "erbottlewat", true };
            yield return new object[] { "aa", "aba", false };
            yield return new object[] { "abcd", "acdb", false };
        }
    }

    [Data]
    public bool IsFlipedString(string s1, string s2)
    {
        if (s1.Length != s2.Length) return false;
        return (s1 + s1).Contains(s2);
    }
}