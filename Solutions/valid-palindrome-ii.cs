namespace Solutions;
/// <summary>
/// 680. 验证回文串 II
/// https://leetcode.cn/problems/valid-palindrome-ii/solution/cong-liang-ce-xiang-zhong-jian-zhao-dao-bu-deng-de/
/// </summary>
public class ValidPalindromeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "aba", true };
            yield return new object[] { "abca", true };
            yield return new object[] { "abc", false };
        }
    }

    [Data]
    public bool ValidPalindrome(string s)
    {
        int left = 0;
        int right = s.Length - 1;
        bool left_deleted = false;
        bool right_delted = false;
        int left_temp = left;
        int right_temp = right;
        while (left < right)
        {
            if (s[left] == s[right]) { left++; right--; }
            else
            {
                if (left_deleted == true && right_delted == true) return false;
                else
                {
                    //当删除左侧或右侧后，仍不是回文
                    if (left_deleted == false)
                    {
                        //尝试删除左侧left值
                        left_deleted = true;
                        left_temp = left;
                        right_temp = right;
                        left++;
                    }
                    else if (right_delted == false)
                    {
                        //尝试删除右侧right值
                        right_delted = true;
                        left = left_temp;
                        right = right_temp;
                        right--;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}