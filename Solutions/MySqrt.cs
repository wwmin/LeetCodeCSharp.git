﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 69. x 的平方根
/// </summary>
public class MySqrtSolution
{
    //[InlineData(4, 2)]
    [InlineData(2, 1)]
    [InlineData(2147483647, 1)]
    public int MySqrt(int x)
    {
        if (x <= 1) return x;
        int min = 0;
        int max = x;
        while (max - min > 1)
        {
            int m = (max + min) / 2;
            //用x/m<m 判断防止 用m * m > x 导致的int数据溢出
            if (x / m < m)
            {
                max = m;
            }
            else
            {
                min = m;
            }
        }
        return min;
    }

    //超时
    //[InlineData(4, 2)]
    [InlineData(2, 1)]
    [InlineData(2147483647, 1)]
    public int MySqrt2(int x)
    {
        if (x <= 1) return x;
        for (int i = 1; i <= x; i++)
        {
            var c = i * i;
            if (c < x) continue;
            if (c == x) return i;
            else if (i * i > x) return i - 1;
        }
        return 0;
    }
}
