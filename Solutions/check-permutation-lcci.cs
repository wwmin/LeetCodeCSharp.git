namespace Solutions;
/// <summary>
/// 100159. 判定是否互为字符重排
/// https://leetcode.cn/problems/check-permutation-lcci/
/// </summary>
public class CheckPermutationSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abc", "bca", true };
            yield return new object[] { "abc", "bad", false };
        }
    }

    [Data]
    public bool CheckPermutation(string s1, string s2)
    {
        if (s1.Length != s2.Length) return false;
        var ss1 = s1.ToCharArray();
        var ss2 = s2.ToCharArray();
        Array.Sort(ss1);
        Array.Sort(ss2);

        //return ss1.Equals(ss2); //同一实例判断
        //return Array.Equals(ss1, ss2); //同一实例判断
        
        return Enumerable.SequenceEqual(ss1, ss2);//C#使用Enumerable.SequenceEqual 判断两个数组是否相等
    }
}