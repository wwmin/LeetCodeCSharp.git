namespace Solutions;
/// <summary>
/// 2027. 转换字符串的最少操作次数
/// difficulty: Easy
/// https://leetcode.cn/problems/minimum-moves-to-convert-string/
/// </summary>
public class MinimumMoves_2027_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{"XXX", 1};
            yield return new object[]{"XXOX", 2};
            yield return new object[]{"OOOO", 0};
        }
    }

    [Data]
    public int MinimumMoves(string s)
    {
        var count = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == 'X')
            {
                count++;
                i += 2;
            }
        }
        return count;
    }
}