namespace Solutions;
/// <summary>
/// 1812. 判断国际象棋棋盘中一个格子的颜色
/// difficulty: Easy
/// https://leetcode.cn/problems/determine-color-of-a-chessboard-square/
/// </summary>
public class SquareIsWhite_1812_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{"a1", false};
            yield return new object[]{"h3", true};
            yield return new object[]{"c7", false};
        }
    }

    [Data]
    public bool SquareIsWhite(string coordinates)
    {
        var x = coordinates[0] - 'a';
        var y = coordinates[1] - '1';
        return (x + y) % 2 == 1;
    }
}