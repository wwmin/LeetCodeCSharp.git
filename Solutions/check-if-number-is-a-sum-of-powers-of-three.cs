namespace Solutions;
/// <summary>
/// 1780. 判断一个数字是否可以表示成三的幂的和
/// difficulty: Medium
/// https://leetcode.cn/problems/check-if-number-is-a-sum-of-powers-of-three/
/// </summary>
public class CheckPowersOfThree_1780_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 12, true };
            yield return new object[] { 91, true };
            yield return new object[] { 21, false };
        }
    }

    [Data]
    public bool CheckPowersOfThree(int n)
    {
        //如果一个数字可以表示成三的幂的和，那么这个数字转换为三进制后，数字应该只有0和1
        while (n > 0)
        {
            if (n % 3 == 2) return false;
            n /= 3;
        }
        return true;
    }
}