﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5996. 统计数组中相等且可以被整除的数对
    /// </summary>
    public class CountPairsSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                //yield return new object[] { StringTo<int[]>("[3,1,2,2,2,1,3]").ToArray(), 2, 4 };
                //yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 1, 0 };
                yield return new object[] { StringTo<int[]>("[5,5,9,2,5,5,9,2,2,5,5,6,2,2,5,2,5,4,3]").ToArray(), 7, 18 };
            }
        }

        [Data]
        public int CountPairs(int[] nums, int k)
        {
            int ans = 0;
            for (int i = 0; i < nums.Length - 1; i++)
            {
                for (int j = i + 1; j < nums.Length; j++)
                {
                    if (nums[i] == nums[j] && i * j % k == 0)
                    {
                        ans++;
                    }
                }
            }
            return ans;
        }
    }
}
