namespace Solutions;
/// <summary>
/// 417. 太平洋大西洋水流问题
/// difficulty: Medium
/// https://leetcode.cn/problems/pacific-atlantic-water-flow/
/// </summary>
public class PacificAtlantic_417_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2,2,3,5],[3,2,3,4,4],[2,4,5,3,1],[6,7,1,4,5],[5,1,1,2,4]]").ToArray(), StringTo<List<List<int>>>("[[0,4],[1,3],[1,4],[2,2],[3,0],[3,1],[4,0]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[2,1],[1,2]]").ToArray(), StringTo<List<List<int>>>("[[0,0],[0,1],[1,0],[1,1]]").ToArray() };
        }
    }

    static int[][] dirs = { new int[] { -1, 0 }, new int[] { 1, 0 }, new int[] { 0, -1 }, new int[] { 0, 1 } };
    int[][] heights;
    int m, n;

    [Data]
    public IList<IList<int>> PacificAtlantic(int[][] heights)
    {
        this.heights = heights;
        this.m = heights.Length;
        this.n = heights[0].Length;
        bool[][] pacific = new bool[m][];
        bool[][] atlantic = new bool[m][];
        for (int i = 0; i < m; i++)
        {
            pacific[i] = new bool[n];
            atlantic[i] = new bool[n];
        }
        for (int i = 0; i < m; i++)
        {
            BFS(i, 0, pacific);
        }
        for (int j = 1; j < n; j++)
        {
            BFS(0, j, pacific);
        }
        for (int i = 0; i < m; i++)
        {
            BFS(i, n - 1, atlantic);
        }
        for (int j = 0; j < n - 1; j++)
        {
            BFS(m - 1, j, atlantic);
        }
        IList<IList<int>> result = new List<IList<int>>();
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (pacific[i][j] && atlantic[i][j])
                {
                    IList<int> cell = new List<int>();
                    cell.Add(i);
                    cell.Add(j);
                    result.Add(cell);
                }
            }
        }
        return result;
    }

    public void BFS(int row, int col, bool[][] ocean)
    {
        if (ocean[row][col])
        {
            return;
        }
        ocean[row][col] = true;
        Queue<Tuple<int, int>> queue = new Queue<Tuple<int, int>>();
        queue.Enqueue(new Tuple<int, int>(row, col));
        while (queue.Count > 0)
        {
            Tuple<int, int> cell = queue.Dequeue();
            foreach (int[] dir in dirs)
            {
                int newRow = cell.Item1 + dir[0], newCol = cell.Item2 + dir[1];
                if (newRow >= 0 && newRow < m && newCol >= 0 && newCol < n && heights[newRow][newCol] >= heights[cell.Item1][cell.Item2] && !ocean[newRow][newCol])
                {
                    ocean[newRow][newCol] = true;
                    queue.Enqueue(new Tuple<int, int>(newRow, newCol));
                }
            }
        }
    }
}