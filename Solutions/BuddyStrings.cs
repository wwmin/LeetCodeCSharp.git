namespace Solutions;
/// <summary>
/// 889. 亲密字符串
/// </summary>
public class BuddyStringsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "ab", "ba", true };
            yield return new object[] { "ab", "ab", false };
            yield return new object[] { "aa", "aa", true };
            yield return new object[] { "aaaaaaabc", "aaaaaaacb", true };
            yield return new object[] { "abac", "abad", false };
        }
    }
    [Data]
    public bool BuddyStrings(string s, string goal)
    {
        int n = s.Length;
        if (n == 1 || goal.Length == 1 || n != goal.Length) return false;
        char[] diff = new char[2];
        bool hasFind = false;
        int zIndex = 'z' - 'a' + 1;
        int[] charArr = new int[zIndex];
        for (int i = 0; i < n; i++)
        {
            var sc = s[i];
            var gc = goal[i];
            charArr[sc - 'a' + 1]++;
            if (sc != gc)
            {
                if (hasFind) return false;
                if (diff[0] == default)
                {
                    diff[0] = sc;
                    diff[1] = gc;
                }
                else
                {
                    if (diff[0] != gc || diff[1] != sc) return false;
                    hasFind = true;
                }
            }
        }
        if (diff[0] == default)
        {
            for (int i = 0; i < zIndex; i++)
            {
                if (charArr[i] > 1) return true;
            }
            return false;
        }

        return hasFind;
    }
}