namespace Solutions;
/// <summary>
/// 141. 环形链表
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */
public class HasCycleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[3,2,0,-4]"), true };
            yield return new object[] { ToListNodeWithString("[1,2]"), true };
            yield return new object[] { ToListNodeWithString("[1]"), false };
        }
    }
    [Data]
    public bool HasCycle(ListNode head)
    {
        Dictionary<ListNode, bool> dict = new Dictionary<ListNode, bool>();
        while (head != null)
        {
            if (dict.ContainsKey(head)) return true;
            else dict.Add(head, true);
            head = head.next;
        }
        return false;
    }

    //快慢指针, 快指针一次移动两个, 慢指针一次移动一个
    public bool HasCycle1(ListNode head)
    {
        if (head == null || head.next == null) return false;
        ListNode slow = head;
        ListNode fast = head.next;
        while (slow != fast)
        {
            if (slow == null || fast == null) return false;
            slow = slow.next;
            fast = fast.next.next;
        }
        return true;
    }
}