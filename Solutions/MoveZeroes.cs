namespace Solutions;
/// <summary>
/// 283. 移动零
/// </summary>
public class MoveZeroesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[0]").ToArray(), StringTo<int[]>("[0]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0,1]").ToArray(), StringTo<int[]>("[1,0]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0,0,1]").ToArray(), StringTo<int[]>("[1,0,0]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0,1,0,3,12]").ToArray(), StringTo<int[]>("[1,3,12,0,0]").ToArray() };
        }
    }
    [Data]
    public void MoveZeroes(int[] nums)
    {
        int n = nums.Length;
        int left = 0;
        int right = 0;
        while (right<n)
        {
            if (nums[right] != 0)
            {
                swap(nums,left,right);
                left++;
            }
            right++;
        }
    }

    private void swap(int[] nums, int left, int right)
    {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }


    public void MoveZeroes1(int[] nums)
    {
        int n = nums.Length;
        int left = 0;
        int right = nums.Length - 1;
        while (nums[left] != 0)
        {
            if (left == n - 1) break;
            else left++;
        }
        while (nums[right] == 0)
        {
            if (right == 0) break;
            else right--;
        }
        while (left <= right)
        {
            if (nums[left] == 0)
            {
                moveArray(nums, left, right);
                if (nums[left] != 0) left++;
                right--;
            }
            else
            {
                left++;
            }
        }
    }
    private void moveArray(int[] nums, int left, int right)
    {
        for (int i = left; i < right; i++)
        {
            nums[i] = nums[i + 1];
        }
        nums[right] = 0;
    }
}