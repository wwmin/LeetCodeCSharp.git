namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 77. 组合
/// </summary>
public class CombineSolution
{

    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 4, 2, StringTo<List<List<int>>>("[[2,4],[3,4],[2,3],[1,2],[1,3],[1,4],]") };
            //yield return new object[] { 1, 1, StringTo<List<List<int>>>("[[1]]") };
        }
    }
    List<int> temp;
    List<List<int>> ans;
    [Data]
    public IList<IList<int>> Combine(int n, int k)
    {
        temp = new List<int>();
        ans = new List<List<int>>();
        dfs(n, k, 1);
        return ans.ToArray();
    }

    private void dfs(int n, int k, int begin)
    {
        if (temp.Count() == k)
        {
            ans.Add(new List<int>(temp));
            return;
        }

        //考虑选择当前位置
        //temp.Add(cur);
        //dfs(cur + 1, n, k);
        //temp.Remove(temp.Count() - 1);
        ////考虑不选择当前位置
        //dfs(cur + 1, n, k);
        for (int i = begin; i <= n; i++)
        {
            temp.Add(i);
            dfs(n, k, i + 1);
            temp.RemoveAt(temp.Count() - 1);
        }
    }
}
