namespace Solutions;
/// <summary>
/// 2059. 转化数字的最小运算数
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-operations-to-convert-number/
/// </summary>
public class MinimumOperations_2059_Solution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,4,12]").ToArray(), 2, 12, 2 };
            yield return new object[] { StringTo<int[]>("[3,5,7]").ToArray(), 0, -4, 2 };
            yield return new object[] { StringTo<int[]>("[2,8,16]").ToArray(), 0, 1, -1 };
        }
    }

    [Data]
    public int MinimumOperations(int[] nums, int start, int goal)
    {
        HashSet<int> set = new HashSet<int>();
        Queue<int> queue = new Queue<int>();
        set.Add(start);
        queue.Enqueue(start);
        int res = 1;
        while (!queue.Count.Equals(0))
        {
            int size = queue.Count;
            for (int i = 0; i < size; i++)
            {
                int j = queue.Dequeue();

                foreach (var n in nums)
                {
                    int add = j + n;
                    int min = j - n;
                    int xor = j ^ n;
                    if (add == goal || min == goal || xor == goal) return res;
                    if (add >= 0 && add <= 1000 && set.Add(add)) queue.Enqueue(add);
                    if (min >= 0 && min <= 1000 && set.Add(min)) queue.Enqueue(min);
                    if (xor >= 0 && xor <= 1000 && set.Add(xor)) queue.Enqueue(xor);
                }
            }
            res++;
        }
        return -1;
    }
}