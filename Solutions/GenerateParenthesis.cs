﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 22. 括号生成
/// </summary>
public class GenerateParenthesisSolution
{
    [InlineData(true, 3, new string[] { "((()))", "(()())", "(())()", "()(())", "()()()" })]
    //[InlineData(true, 4, new string[] { "(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())",
    //    "(()())()", "(())(())", "(())()()", "()((()))", "()(()())", "()(())()", "()()(())", "()()()()" })]
    public IList<string> GenerateParenthesis(int n)
    {
        List<string> res = new List<string>();
        backtrack(res, new StringBuilder(), 0, 0, n);
        return res.ToArray();
    }

    /// <summary>
    /// 回溯算法
    /// </summary>
    /// <param name="ans"></param>
    /// <param name="cur"></param>
    /// <param name="open"></param>
    /// <param name="close"></param>
    /// <param name="max"></param>
    private void backtrack(List<string> ans, StringBuilder cur, int open, int close, int max)
    {
        if (cur.Length == max * 2)
        {
            ans.Add(cur.ToString());
        }
        if (open < max)
        {
            cur.Append('(');
            backtrack(ans, cur, open + 1, close, max);
            cur.Remove(cur.Length - 1, 1);
        }
        if (close < open)
        {
            cur.Append(')');
            backtrack(ans, cur, open, close + 1, max);
            cur.Remove(cur.Length - 1, 1);
        }
    }
}