namespace Solutions;
/// <summary>
/// 1806. 还原排列的最少操作步数
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-number-of-operations-to-reinitialize-a-permutation/
/// </summary>
public class ReinitializePermutation_1806_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, 1 };
            yield return new object[] { 4, 2 };
            yield return new object[] { 6, 4 };
        }
    }

    [Data]
    public int ReinitializePermutation(int n)
    {
        int[] perm = new int[n];
        int[] target = new int[n];
        for (int i = 0; i < n; i++)
        {
            perm[i] = i;
            target[i] = i;
        }
        int step = 0;
        while (true)
        {
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                if ((i & 1) != 0)
                {
                    arr[i] = perm[n / 2 + (i - 1) / 2];
                }
                else
                {
                    arr[i] = perm[i / 2];
                }
            }
            perm = arr;
            step++;
            if (perm.SequenceEqual(target))
            {
                break;
            }
        }
        return step;
    }
}