namespace Solutions;
/// <summary>
/// 1825. 求出 MK 平均值
/// difficulty: Hard
/// https://leetcode.cn/problems/finding-mk-average/
/// </summary>
public class MKAverageSolution
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"MKAverage\",\"addElement\",\"addElement\",\"calculateMKAverage\",\"addElement\",\"calculateMKAverage\",\"addElement\",\"addElement\",\"addElement\",\"calculateMKAverage\"]", "[[3,1],[3],[1],[],[10],[],[5],[5],[5],[]]", "[null,null,null,-1,null,3,null,null,null,5]" };

        }
    }
    public class Node
    {
        //flag的意义
        //0表示在最小的k个元素堆
        //1表示在最大的k个元素堆
        //2表示在中间元素堆
        //3表示已移除队列
        public int flag;
        public int val;
        public Node(int val)
        {
            this.val = val;
        }
    }

    public class MKAverage
    {
        private int m;
        private int k;
        private Queue<Node> queue; //保存最后m个元素
        private PriorityQueue<Node, int> pqLow; //保存最小的k个元素 最大堆
        private PriorityQueue<Node, int> pqHigh; //保存最大的k个元素 最小堆
        private PriorityQueue<Node, int> pqMidMin; //保留中间m - 2k个元素 最小堆
        private PriorityQueue<Node, int> pqMidMax; //保留中间m - 2k个元素 最大堆
        private int sumMid; //当前中间和
        public MKAverage(int m, int k)
        {
            this.m = m;
            this.k = k;
            queue = new Queue<Node>();
            pqLow = new PriorityQueue<Node, int>();
            pqMidMin = new PriorityQueue<Node, int>();
            pqMidMax = new PriorityQueue<Node, int>();
            pqHigh = new PriorityQueue<Node, int>();
        }

        public void AddElement(int num)
        {
            Node node = new Node(num);
            queue.Enqueue(node);
            while (pqLow.Count > 0 && pqLow.Peek().flag != 0) pqLow.Dequeue();
            while (pqHigh.Count > 0 && pqHigh.Peek().flag != 1) pqHigh.Dequeue();
            while (pqMidMin.Count > 0 && pqMidMin.Peek().flag != 2) pqMidMin.Dequeue();
            while (pqMidMin.Count > 0 && pqMidMax.Peek().flag != 2) pqMidMax.Dequeue();
            if (queue.Count <= k)
            {
                //<=k个元素 直接添加到pqLow
                pqLow.Enqueue(node, -node.val);
                node.flag = 0;
            }
            else if (queue.Count <= k * 2)
            {
                //<=2k个元素 多余部分放入pqHigh
                Node nLowMax = pqLow.Peek();
                if (nLowMax.val >= node.val)
                {
                    pqLow.Dequeue();
                    pqLow.Enqueue(node, -node.val);
                    node.flag = 0;
                    pqHigh.Enqueue(nLowMax, nLowMax.val);
                    nLowMax.flag = 1;
                }
                else
                {
                    pqHigh.Enqueue(node, node.val);
                    node.flag = 1;
                }
            }
            else
            {
                //多余部分放入中间堆
                Node nLowMax = pqLow.Peek();
                Node nHighMin = pqHigh.Peek();
                if (nLowMax.val >= node.val)
                {
                    pqLow.Dequeue();
                    pqLow.Enqueue(node, -node.val);
                    node.flag = 0;
                    pqMidMin.Enqueue(nLowMax, nLowMax.val);
                    pqMidMax.Enqueue(nLowMax, -nLowMax.val);
                    nLowMax.flag = 2;
                    sumMid += nLowMax.val;
                }
                else if (nHighMin.val <= node.val)
                {
                    pqHigh.Dequeue();
                    pqHigh.Enqueue(node, node.val);
                    node.flag = 1;
                    pqMidMin.Enqueue(nHighMin, nHighMin.val);
                    pqMidMax.Enqueue(nHighMin, -nHighMin.val);
                    nHighMin.flag = 2;
                    sumMid += nHighMin.val;
                }
                else
                {
                    pqMidMin.Enqueue(node, node.val);
                    pqMidMax.Enqueue(node, -node.val);
                    node.flag = 2;
                    sumMid += node.val;
                }
            }
            if (queue.Count > m)
            {
                //移除旧的
                Node oldNode = queue.Dequeue();
                if (oldNode.flag == 0)
                {
                    //如果之前在最小的k个队列中 此时需要从mid中获取一个最小值
                    Node nMidMin = pqMidMin.Dequeue();
                    nMidMin.flag = 0;
                    pqLow.Enqueue(nMidMin, -nMidMin.val);
                    sumMid -= nMidMin.val;
                }
                else if (oldNode.flag == 1)
                {
                    //如果在最大的k个队列中 此时需要从mid中获取一个最大值
                    Node nMidMax = pqMidMax.Dequeue();
                    nMidMax.flag = 1;
                    pqHigh.Enqueue(nMidMax, nMidMax.val);
                    sumMid -= nMidMax.val;
                }
                else
                {
                    //在中间
                    sumMid -= oldNode.val;
                }
                oldNode.flag = 3;
            }
        }

        public int CalculateMKAverage()
        {
            if (queue.Count < m) return -1;
            return sumMid / (m - k * 2);
        }
    }
}
/**
 * Your MKAverage object will be instantiated and called as such:
 * MKAverage obj = new MKAverage(m, k);
 * obj.AddElement(num);
 * int param_2 = obj.CalculateMKAverage();
 */