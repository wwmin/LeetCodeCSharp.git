namespace Solutions;
/// <summary>
/// 1927. 最大升序子数组和
/// https://leetcode.cn/problems/maximum-ascending-subarray-sum/
/// </summary>
public class MaxAscendingSumSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[6]").ToArray(), 6 };
            yield return new object[] { StringTo<int[]>("[10,20,30,5,10,50]").ToArray(), 65 };
            yield return new object[] { StringTo<int[]>("[10,20,30,40,50]").ToArray(), 150 };
            yield return new object[] { StringTo<int[]>("[12,17,15,13,10,11,12]").ToArray(), 33 };
            yield return new object[] { StringTo<int[]>("[100,10,1]").ToArray(), 100 };
        }
    }

    [Data]
    public int MaxAscendingSum(int[] nums)
    {
        int res = nums[0];
        int curr = nums[0];
        int n = nums.Length - 1;
        for (int i = 1; i < nums.Length; i++)
        {
            var left = nums[i - 1];
            var right = nums[i];
            if (left < right)
            {
                curr += right;
                if (i == n)
                {
                    res = Math.Max(res, curr);
                }
            }
            else
            {
                res = Math.Max(res, curr);
                curr = right;
            }
        }
        return res;
    }
}