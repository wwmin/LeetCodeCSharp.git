namespace Solutions;
/// <summary>
/// 441. 排列硬币
/// </summary>
public class ArrangeCoinsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 5, 2 };
            yield return new object[] { 8, 3 };
        }
    }

    [Data]
    public int ArrangeCoins(int n)
    {
        int ans = 0;
        long sum = 0;
        int i = 1;
        while (true)
        {
            sum += i++;
            if (sum > n)
            {
                break;
            }
            else
            {
                ans++;
            }
        }
        return ans;
    }
}