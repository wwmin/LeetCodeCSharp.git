namespace Solutions;
/// <summary>
/// 217. 存在重复元素
/// </summary>
public class ContainsDuplicateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,1]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), false };
            yield return new object[] { StringTo<int[]>("[1,1,1,3,3,4,3,2,4,2]").ToArray(), true };
        }
    }

    [Data]
    public bool ContainsDuplicate(int[] nums)
    {
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (map.ContainsKey(nums[i])) return true;
            else map[nums[i]] = i;
        }
        return false;
    }
}