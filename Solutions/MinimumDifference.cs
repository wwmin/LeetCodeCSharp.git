﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 5854. 学生分数的最小差值
/// </summary>
public class MinimumDifferenceSolution
{
    //[InlineData(new int[] { 90 }, 1, 0)]
    [InlineData(new int[] { 9, 4, 1, 7 }, 2, 2)]
    public int MinimumDifference(int[] nums, int k)
    {
        int n = nums.Length;
        Array.Sort(nums);
        if (nums.Length == 1) return 0;
        //return nums[nums.Length - 1] - nums[nums.Length-2];
        int min = int.MaxValue;
        int left = 0;
        int right = k - 1;
        while (right < n)
        {
            int s = nums[right] - nums[left];
            if (min > s) min = s;
            left++;
            right++;
        }

        return min;
    }
}
