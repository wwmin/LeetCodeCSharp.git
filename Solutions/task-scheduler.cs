namespace Solutions;
/// <summary>
/// 621. 任务调度器
/// difficulty: Medium
/// https://leetcode.cn/problems/task-scheduler/
/// </summary>
public class LeastInterval_621_Solution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<char[]>("[\'A\',\'A\',\'A\',\'B\',\'B\',\'B\']").ToArray(), 2, 8 };
            yield return new object[] { StringTo<char[]>("[\'A\',\'A\',\'A\',\'B\',\'B\',\'B\']").ToArray(), 0, 6 };
            yield return new object[] { StringTo<char[]>("[\'A\',\'A\',\'A\',\'A\',\'A\',\'A\',\'B\',\'C\',\'D\',\'E\',\'F\',\'G\']").ToArray(), 2, 16 };
        }
    }

    [Data]
    public int LeastInterval(char[] tasks, int n)
    {
        int[] cnts = new int[26];
        foreach (var c in tasks) cnts[c - 'A']++;
        int max = 0, tot = 0;
        for (int i = 0; i < 26; i++) max = Math.Max(max, cnts[i]);
        for (int i = 0; i < 26; i++) tot += max == cnts[i] ? 1 : 0;
        return Math.Max(tasks.Length, (n + 1) * (max - 1) + tot);
    }
}