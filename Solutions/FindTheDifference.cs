namespace Solutions;
/// <summary>
/// 389. 找不同
/// </summary>
public class FindTheDifferenceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcd", "abcde", 'e' };
            yield return new object[] { "", "y", 'y' };
        }
    }

    [Data]
    public char FindTheDifference(string s, string t)
    {
        Dictionary<char, int> map = new Dictionary<char, int>();
        foreach (char c in s)
        {
            if (map.ContainsKey(c)) map[c]++;
            else map.Add(c, 1);
        }
        foreach (var c in t)
        {
            if (!map.ContainsKey(c)) return c;
            map[c]--;
            if (map[c] == -1) return c;
        }
        return default;
    }
}