namespace Solutions;
/// <summary>
/// 1496. 矩阵中的幸运数
/// </summary>
public class LuckyNumbersSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[3,7,8],[9,11,13],[15,16,17]]").ToArray(), StringTo<List<int>>("[15]") };
            yield return new object[] { StringTo<List<int[]>>("[[1,10,4,2],[9,3,8,7],[15,16,17,12]]").ToArray(), StringTo<List<int>>("[12]") };
            yield return new object[] { StringTo<List<int[]>>("[[7,8],[1,2]]").ToArray(), StringTo<List<int>>("[7]") };
        }
    }

    [Data]
    public IList<int> LuckyNumbers(int[][] matrix)
    {
        IList<int> ans = new List<int>();
        int m = matrix.Length;
        int n = matrix[0].Length;
        int[] minRow = new int[m];
        int[] maxCol = new int[n];
        Array.Fill(minRow, int.MaxValue);
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                minRow[i] = Math.Min(minRow[i], matrix[i][j]);
                maxCol[j] = Math.Max(maxCol[j], matrix[i][j]);
            }
        }
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (matrix[i][j] == minRow[i] && matrix[i][j] == maxCol[j])
                {
                    ans.Add(matrix[i][j]);
                }
            }
        }

        return ans;
    }
}