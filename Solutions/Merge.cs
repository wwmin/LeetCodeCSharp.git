﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 56. 合并区间
/// </summary>
public class MergeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,3],[2,6],[8,10],[15,18]]").ToArray(), StringTo<List<int[]>>("[[1,6],[8,10],[15,18]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,4],[4,5]]").ToArray(), StringTo<List<int[]>>("[[1,5]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[1,4],[2,3]]").ToArray(), StringTo<List<int[]>>("[[1,4]]").ToArray() };
        }
    }
    [Data]
    public int[][] Merge(int[][] intervals)
    {
        //先按照第一个数字排序
        Array.Sort(intervals, ((a, b) => a[0] - b[0]));
        List<List<int>> res = new List<List<int>>();
        res.Add(intervals[0].ToList());
        for (int i = 1; i < intervals.Length; i++)
        {
            var resCurrLastNum = res[res.Count - 1][1];
            var sourceCurrFirstNum = intervals[i][0];
            if (resCurrLastNum >= sourceCurrFirstNum)
            {
                if (res[res.Count - 1][1] < intervals[i][1])
                {
                    res[res.Count - 1][1] = intervals[i][1];
                };
            }
            else
            {
                res.Add(intervals[i].ToList());
            }
        }
        return res.Select(p => p.ToArray()).ToArray();
    }
}
