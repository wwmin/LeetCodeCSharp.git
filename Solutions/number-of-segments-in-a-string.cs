namespace Solutions;
/// <summary>
/// 434. 字符串中的单词数
/// </summary>
public class CountSegmentsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "Hello, my name is John", 5 };
            yield return new object[] { "Hello,  a     b", 3 };
        }
    }

    [Data]
    public int CountSegments(string s)
    {
        return s.Split(" ").Where(p => p.Trim() != "").Count();
    }
}