﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 32. 最长有效括号
/// </summary>
public class LongestValidParenthesesSolution
{

    private Dictionary<char, char> brackets = new Dictionary<char, char> { { '(', ')' } };
    /// <summary>
    /// 动态规划解法
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    [InlineData("", 0)]
    [InlineData("(()", 2)]
    [InlineData(")()())", 4)]
    [InlineData(")(())))(())())", 6)]
    public int LongestValidParentheses(string s)
    {
        int res = 0;
        if (s.Length <= 1) return res;
        int n = s.Length;
        //默认值都是0
        int[] dp = new int[n];
        for (int i = 1; i < n; i++)
        {
            if (s[i] == ')')
            {
                //当遇到右括号时
                var pre = i - dp[i - 1] - 1;
                //如果是左括号,则更新匹配长度
                if (pre >= 0 && s[pre] == '(')
                {
                    dp[i] = dp[i - 1] + 2;
                    //处理该配对前面的独立的括号对的情形,类似()(),()(())
                    if (pre > 0)
                    {
                        dp[i] += dp[pre - 1];
                    }
                }
            }
        }

        return dp.Max();
    }

    /// <summary>
    /// 此解超时
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    //[InlineData("", 0)]
    //[InlineData("(()", 2)]
    //[InlineData(")()())", 4)]
    //[InlineData(")(())))(())())", 6)]
    public int LongestValidParentheses2(string s)
    {
        int res = 0;
        if (s.Length <= 1) return res;
        int n = s.Length;
        for (int i = 0; i < n; i++)
        {
            int left = i;
            if (s[left] == ')') continue;
            int right = n - 1;
            while (left < right)
            {
                int l = right - left + 1;
                if (l < res) break;
                if (s[right] == '(') { right--; continue; }
                //判断括号是否有效
                if (MatchParentheses(s[left..(right + 1)]))
                {
                    res = Math.Max(res, l);
                }
                right--;
            }
        }

        return res;
    }

    /// <summary>
    /// 验证字符串是否匹配
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    private bool MatchParentheses(string s)
    {
        Stack<char> res = new Stack<char>();
        for (int i = 0; i < s.Length; i++)
        {
            if (res.Count == 0)
            {
                if (brackets.ContainsKey(s[i])) res.Push(s[i]);
                else return false;
            }
            else
            {
                if (brackets.ContainsKey(s[i])) res.Push(s[i]);
                else
                {
                    var lastChar = res.Peek();
                    var mc = brackets[lastChar];
                    if (mc == s[i]) res.Pop();
                    else return false;
                }
            }
        }
        return res.Count == 0;
    }
}