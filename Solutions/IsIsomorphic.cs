namespace Solutions;
/// <summary>
/// 205. 同构字符串
/// </summary>
public class IsIsomorphicSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "egg", "add", true };
            yield return new object[] { "foo", "bar", false };
            yield return new object[] { "paper", "title", true };
        }
    }

    [Data]
    public bool IsIsomorphic(string s, string t)
    {
        Dictionary<char, char> map = new Dictionary<char, char>();
        int n = s.Length;
        for (int i = 0; i < n; i++)
        {
            char sc = s[i];
            char tc = t[i];
            if (!map.ContainsKey(sc))
            {
                if (map.ContainsValue(tc)) return false;
                else map[sc] = tc;
            }
            else
            {
                if (map[sc] == tc) continue;
                else return false;
            }
        }
        return true;
    }
}