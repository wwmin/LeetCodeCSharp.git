namespace Solutions;
/// <summary>
/// 86. 分隔链表
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class PartitionSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,4,3,2,5,2]"), 3, ToListNodeWithString("[1,2,2,4,3,5]") };
            yield return new object[] { ToListNodeWithString("[2,1]"), 2, ToListNodeWithString("[1,2]") };
            yield return new object[] { ToListNodeWithString("[1]"), 2, ToListNodeWithString("[1]") };
        }
    }
    [Data]
    public ListNode Partition(ListNode head, int x)
    {
        ListNode first = new ListNode(0, null);
        ListNode first_cur = first;
        ListNode second = new ListNode(0, null);
        ListNode second_cur = second;

        while (head != null)
        {
            if (head.val < x)
            {
                first_cur.next = new ListNode(head.val);
                first_cur = first_cur.next;
            }
            else
            {
                second_cur.next = new ListNode(head.val);
                second_cur = second_cur.next;
            }
            head = head.next;
        }
        first_cur.next = second.next;
        return first.next;
    }
}