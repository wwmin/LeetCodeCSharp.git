﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 2169. 得到 0 的操作数
    /// </summary>
    public class CountOperationsSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 2, 3, 3 };
                yield return new object[] { 10, 10, 1 };
            }
        }
        [Data]
        public int CountOperations(int num1, int num2)
        {
            int ans = 0;
            while (true)
            {
                if (num1 == 0 || num2 == 0) break;
                ans++;
                if (num1 >= num2)
                {
                    num1 = num1 - num2;
                }
                else
                {
                    num2 = num2 - num1;
                }
            }
            return ans;
        }
    }
}