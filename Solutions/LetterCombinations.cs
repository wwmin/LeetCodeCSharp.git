﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 17. 电话号码的字母组合
/// </summary>
public class LetterCombinationsSolution
{

    private Dictionary<char, List<string>> phoneWords = new Dictionary<char, List<string>>
        {
            { '2',new List<string>{"a","b","c" }},
            { '3',new List<string>{"d","e","f" }},
            { '4',new List<string>{"g","h","i" }},
            { '5',new List<string>{"j","k","l" }},
            { '6',new List<string>{"m","n","o" }},
            { '7',new List<string>{"p","q","r","s"}},
            { '8',new List<string>{"t","u","v" }},
            { '9',new List<string>{"w","x","y","z" }}
        };

    /// <summary>
    /// 广度算法
    /// </summary>
    /// <param name="digits"></param>
    /// <returns></returns>
    //[InlineData(ignoreOrder: true, "", new string[] { })]
    //[InlineData(ignoreOrder: true, "2", new string[] { "b", "c", "a" })]
    //[InlineData(ignoreOrder: true, "7", new string[] { "p", "q", "r", "s" })]
    //[InlineData(ignoreOrder: true, "23", new string[] { "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" })]
    public IList<string> LetterCombinations(string digits)
    {
        Queue<string> qs = new Queue<string>() { };
        int dl = digits.Length;
        if (dl == 0) return qs.ToArray();
        for (int i = 0; i < dl; i++)
        {
            var dic = phoneWords[digits[i]];
            var dicl = dic.Count();
            if (i == 0)
            {
                foreach (var d in dic)
                {
                    qs.Enqueue(d);
                }
                continue;
            }
            while (qs.Peek().Length <= i)
            {
                var firstQ = qs.Dequeue();
                for (int j = 0; j < dicl; j++)
                {
                    qs.Enqueue(firstQ + dic[j]);
                }
            }
        }
        return qs.ToArray();
    }


    /// <summary>
    /// 深度算法
    /// </summary>
    /// <param name="digits"></param>
    /// <returns></returns>
    //[InlineData(ignoreOrder: true, "", new string[] { })]
    //[InlineData(ignoreOrder: true, "2", new string[] { "b", "c", "a" })]
    //[InlineData(ignoreOrder: true, "7", new string[] { "p", "q", "r", "s" })]
    [InlineData(ignoreOrder: true, "23", new string[] { "ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf" })]
    public IList<string> LetterCombinations2(string digits)
    {
        List<string> combinations = new List<string>();
        int dl = digits.Length;
        if (dl == 0) return combinations.ToArray();

        backtrack(combinations, digits, 0, new StringBuilder());
        return combinations.ToArray();
    }

    private void backtrack(List<string> combinations, string digits, int index, StringBuilder combination)
    {
        if (index == digits.Length)
        {
            combinations.Add(combination.ToString());
        }
        else
        {
            char digit = digits[index];
            List<string> letters = phoneWords[digit];
            int lettersCount = letters.Count;
            for (int i = 0; i < lettersCount; i++)
            {
                combination.Append(letters[i]);
                backtrack(combinations, digits, index + 1, combination);
                combination.Remove(index, 1);
            }
        }
    }
}
