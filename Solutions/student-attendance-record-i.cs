namespace Solutions;
/// <summary>
/// 551. 学生出勤记录 I
/// </summary>
public class CheckRecordSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "PPALLP", true };
            yield return new object[] { "PPALLL", false };
            yield return new object[] { "AAAA", false };
        }
    }

    [Data]
    public bool CheckRecord(string s)
    {
        int countA = 0;
        int countL = 0;
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == 'A')
            {
                countA++;
                countL = 0;
                if (countA > 1) return false;
            }
            else if (s[i] == 'L')
            {
                countL++;
                if (countL > 2)
                {
                    return false;
                }
            }
            else
            {
                countL = 0;
            }
        }

        return true;
    }
}