namespace Solutions;
/// <summary>
/// 87. 扰乱字符串
/// </summary>
public class IsScrambleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "great", "rgeat", true };
            yield return new object[] { "abcde", "caebd", false };
            yield return new object[] { "a", "a", true };
        }
    }

    [Data]
    public bool IsScramble(string s1, string s2)
    {
        if (s1.Length != s2.Length)
        {
            return false;
        }
        int n = s1.Length;
        bool[,,] dp = new bool[n, n, n];//[s1子串起点下标，s2子串起点下标，字串长度（0表示长度1）]
                                        //初始化DP
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                dp[i, j, 0] = s1[i] == s2[j];
            }
        }
        for (int l = 1; l < n; l++)
        {
            for (int i = 0; i < n - l; i++)
            {
                for (int j = 0; j < n - l; j++)
                {
                    for (int w = 1; w < l + 1; w++)
                    {
                        dp[i, j, l] |= dp[i, j, w - 1] && dp[i + w, j + w, l - w];
                        dp[i, j, l] |= dp[i, j + l - w + 1, w - 1] && dp[i + w, j, l - w];
                        if (dp[i, j, l])
                        {
                            break;
                        }
                    }
                }
            }
        }
        return dp[0, 0, n - 1];
    }
}