namespace Solutions;
/// <summary>
/// 326. 3 的幂
/// </summary>
public class IsPowerOfThreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 27, true };
            yield return new object[] { 0, false };
            yield return new object[] { 9, true };
            yield return new object[] { 45, false };
        }
    }

    [Data]
    public bool IsPowerOfThree(int n)
    {
        int m = 0;
        while (m <= 31)
        {
            int res = (int)Math.Pow(3, m++);
            if (res < n) continue;
            else if (res > n) return false;
            return true;
        }
        return false;
    }
}