namespace Solutions;
/// <summary>
/// 561. 数组拆分
/// https://leetcode.cn/problems/array-partition/comments/
/// </summary>
public class ArrayPairSumSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,4,3,2]").ToArray(), 4 };
            yield return new object[] { StringTo<int[]>("[6,2,6,5,1,2]").ToArray(), 9 };
        }
    }

    [Data]
    public int ArrayPairSum(int[] nums)
    {
        Array.Sort(nums);
        int sum = 0;
        for (int i = 0; i < nums.Length; i = i + 2)
        {
            sum += nums[i];
        }
        return sum;
    }
}