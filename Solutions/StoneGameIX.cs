namespace Solutions;
/// <summary>
/// 2156. 石子游戏 IX
/// </summary>
public class StoneGameIXSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,1]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[2]").ToArray(), false };
            yield return new object[] { StringTo<int[]>("[5,1,2,4,3]").ToArray(), false };
        }
    }

    [Data]
    public bool StoneGameIX(int[] stones)
    {
        int cnt0 = 0;
        int cnt1 = 0;
        int cnt2 = 0;
        foreach (var val in stones)
        {
            int type = val % 3;
            if (type == 0)
            {
                cnt0++;
            }
            else if (type == 1)
            {
                cnt1++;
            }
            else
            {
                cnt2++;
            }
        }
        if (cnt0 % 2 == 0)
        {
            return cnt1 >= 1 && cnt2 >= 1;
        }
        return cnt1 - cnt2 > 2 || cnt2 - cnt1 > 2;
    }
}