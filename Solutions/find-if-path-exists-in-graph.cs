namespace Solutions;
/// <summary>
/// 1971. 寻找图中是否存在路径
/// difficulty: Easy
/// https://leetcode.cn/problems/find-if-path-exists-in-graph/
/// </summary>
public class ValidPath_1971_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, StringTo<List<int[]>>("[[0,1],[1,2],[2,0]]").ToArray(), 0, 2, true };
            yield return new object[] { 6, StringTo<List<int[]>>("[[0,1],[0,2],[3,5],[5,4],[4,3]]").ToArray(), 0, 5, false };
        }
    }

    [Data]
    public bool ValidPath(int n, int[][] edges, int source, int destination)
    {
        IList<int>[] adj = new IList<int>[n];
        for (int i = 0; i < n; i++)
        {
            adj[i] = new List<int>();
        }
        foreach (var edge in edges)
        {
            adj[edge[0]].Add(edge[1]);
            adj[edge[1]].Add(edge[0]);
        }
        bool[] visited = new bool[n];
        return DFS(source, destination, adj, visited);
    }

    bool DFS(int source, int destination, IList<int>[] adj, bool[] visited)
    {
        if (source == destination) return true;
        visited[source] = true;
        foreach (var next in adj[source])
        {
            if (!visited[next] && DFS(next, destination, adj, visited))
            {
                return true;
            }
        }
        return false;
    }
}