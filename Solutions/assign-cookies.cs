namespace Solutions;
/// <summary>
/// 455. 分发饼干
/// </summary>
public class FindContentChildrenSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), StringTo<int[]>("[1,1]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), StringTo<int[]>("[1,2,3]").ToArray(), 2 };
        }
    }

    [Data]
    public int FindContentChildren(int[] g, int[] s)
    {
        Array.Sort(g);
        Array.Sort(s);
        int child = 0;
        int cookie = 0;

        while (child < g.Length && cookie < s.Length)
        {
            if (g[child] <= s[cookie])
            {
                child++;
            }
            cookie++;
        }
        return child;
    }
}