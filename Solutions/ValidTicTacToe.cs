namespace Solutions;
/// <summary>
/// 810. 有效的井字游戏
/// </summary>
public class ValidTicTacToeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"   \",\"   \",\"   \"]").ToArray(), true };
            yield return new object[] { StringTo<string[]>("[\"O  \",\"   \",\"   \"]").ToArray(), false };
            yield return new object[] { StringTo<string[]>("[\"XOX\",\" X \",\"   \"]").ToArray(), false };
            yield return new object[] { StringTo<string[]>("[\"XXX\",\"   \",\"OOO\"]").ToArray(), false };
            yield return new object[] { StringTo<string[]>("[\"XOX\",\"O O\",\"XOX\"]").ToArray(), true };
            yield return new object[] { StringTo<string[]>("[\"XXX\",\"OOX\",\"OOX\"]").ToArray(), true };
            yield return new object[] { StringTo<string[]>("[\"OXO\",\"XOX\",\"OXO\"]").ToArray(), false };
            yield return new object[] { StringTo<string[]>("[\"XOX\",\"OXO\",\"XOX\"]").ToArray(), true };
            yield return new object[] { StringTo<string[]>("[\"XXX\",\"XOO\",\"OO \"]").ToArray(), false };
        }
    }
    const char X = 'X';
    const char O = 'O';
    [Data]
    public bool ValidTicTacToe(string[] board)
    {
        //玩家1放 X
        var xn = 0;
        //玩家2放 O
        var on = 0;
        //空格数
        var cn = 0;
        //统计每个棋子及空格个数
        for (var i = 0; i < board.Length; i++)
        {
            for (int j = 0; j < board[i].Length; j++)
            {
                if (board[i][j] == X) xn++;
                else if (board[i][j] == O) on++;
                else cn++;
            }
        }
        if (cn == 9) return true;
        //玩家2 不能大于玩家1
        if (on > xn) return false;
        //玩家1 棋数量不能大于玩家2 其数量2个
        if (xn - on > 1) return false;
        char win_char = default;
        List<int[,]> winCharList = new List<int[,]>();
        //3横 3竖 2斜
        for (int i = 0; i < 3; i++)
        {
            //横
            var row = board[i].ToCharArray();
            if (checkThreeCharEqual(row))
            {
                if (win_char != default && win_char != row[0]) return false;
                win_char = row[0];
                winCharList.Add(new int[,] { { i, 0 }, { i, 1 }, { i, 2 } });
            }
            //竖
            var col = new char[] { board[0][i], board[1][i], board[2][i] };
            if (checkThreeCharEqual(col))
            {
                if (win_char != default && win_char != col[0]) return false;
                win_char = col[0];
                winCharList.Add(new int[,] { { 0, i }, { 1, i }, { 2, i } });
            }
        }
        if (board[1][1] != ' ')
        {
            //左斜
            var left = new char[] { board[0][0], board[1][1], board[2][2] };
            if (checkThreeCharEqual(left))
            {
                if (win_char != default && win_char != left[0]) return false;
                win_char = left[0];
                winCharList.Add(new int[,] { { 0, 0 }, { 1, 1 }, { 2, 2 } });
            }
            //右斜
            var right = new char[] { board[0][2], board[1][1], board[2][0] };
            if (checkThreeCharEqual(right))
            {
                if (win_char != default && win_char != right[0]) return false;
                win_char = right[0];
                winCharList.Add(new int[,] { { 0, 2 }, { 1, 1 }, { 2, 0 } });
            }
        }
        //如果玩家1赢了,则玩家1的棋个数应大于玩家2的棋个数
        if (win_char == X)
        {
            if (on >= xn) return false;
        }
        //如果玩家2赢了,则玩家2的棋个数应等于玩家1的棋个数
        if (win_char == O)
        {
            if (xn != on) return false;
        }
        return true;
    }
    private bool checkThreeCharEqual(char[] chars)
    {
        char c = chars[0];
        for (int i = 1; i < chars.Length; i++)
        {
            if (chars[i] == ' ') return false;
            if (chars[i] != c) return false;
        }
        return true;
    }
}