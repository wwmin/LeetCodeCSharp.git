namespace Solutions;
/// <summary>
/// 678. 有效的括号字符串
/// difficulty: Medium
/// https://leetcode.cn/problems/valid-parenthesis-string/
/// </summary>
public class CheckValidString_678_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "()", true };
            yield return new object[] { "(*)", true };
            yield return new object[] { "(*))", true };
        }
    }

    [Data]
    public bool CheckValidString(string s)
    {
        int min = 0, max = 0;
        foreach (char c in s)
        {
            if (c == '(')
            {
                min++;
                max++;
            }
            else if (c == ')')
            {
                min--;
                max--;
            }
            else
            {
                min--;
                max++;
            }
            if (max < 0) return false;
            min = Math.Max(min, 0);
        }
        return min == 0;
    }
}