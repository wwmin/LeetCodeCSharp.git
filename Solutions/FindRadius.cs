namespace Solutions;
/// <summary>
/// 475. 供暖器
/// </summary>
public class FindRadiusSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), StringTo<int[]>("[2]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), StringTo<int[]>("[1,4]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,5]").ToArray(), StringTo<int[]>("[2]").ToArray(), 3 };
        }
    }

    [Data]
    public int FindRadius(int[] houses, int[] heaters)
    {
        Array.Sort(houses);
        Array.Sort(heaters);
        int l = 0, r = (int)1e9;
        while (l < r)
        {
            int mid = l + r >> 1;
            if (check(houses, heaters, mid)) r = mid;
            else l = mid + 1;
        }
        return r;
    }

    bool check(int[] houses, int[] heaters, int x)
    {
        int n = houses.Length, m = heaters.Length;
        for (int i = 0, j = 0; i < n; i++)
        {
            while (j < m && houses[i] > heaters[j] + x) j++;
            if (j < m && heaters[j] - x <= houses[i] && houses[i] <= heaters[j] + x) continue;
            return false;
        }
        return true;
    }
}