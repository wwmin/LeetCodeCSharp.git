﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 67. 二进制求和
/// </summary>
public class AddBinarySolution
{
    [InlineData("11", "1", "100")]
    [InlineData("1010", "1011", "10101")]
    [InlineData("100", "110010", "110110")]
    public string AddBinary(string a, string b)
    {
        StringBuilder sb = new StringBuilder();
        int n = Math.Max(a.Length, b.Length);
        char remain = default;
        for (int i = 0; i < n; i++)
        {
            var ca = a.Length - i - 1 >= 0 ? a[a.Length - i - 1] : default;
            var cb = b.Length - i - 1 >= 0 ? b[b.Length - i - 1] : default;
            if (remain != default)
            {

                if (ca == '1' && cb == '1')
                {
                    sb.Append('1');
                    remain = '1';
                }
                else if (ca == '0' && cb == '0')
                {
                    sb.Append('1');
                    remain = default;
                }
                else if (ca == default || cb == default)
                {
                    var c = ca == default ? cb : ca;
                    if (c == '0')
                    {
                        sb.Append('1');
                        remain = default;
                    }
                    else
                    {
                        sb.Append('0');
                        remain = '1';
                    }
                }
                else
                {
                    sb.Append('0');
                    remain = '1';
                }
            }
            else
            {
                if (ca == '1' && cb == '1')
                {
                    sb.Append('0');
                    remain = '1';
                }
                else if (ca == '0' && cb == '0')
                {
                    sb.Append('0');
                }
                else if (ca == default || cb == default)
                {
                    sb.Append(ca == default ? cb : ca);
                }
                else
                {
                    sb.Append('1');
                }
            }
        }
        if (remain == '1')
        {
            sb.Append('1');
        }
        char[] res = sb.ToString().ToCharArray();
        Array.Reverse(res);
        return new string(res);
    }
}

