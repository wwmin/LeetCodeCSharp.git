namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 260. 只出现一次的数字 III
/// </summary>
public class SingleNumberSolution
{

    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<int[]>("[1,2,1,3,2,5]").ToArray(), StringTo<int[]>("[3,5]").ToArray() };
            //yield return new object[] { StringTo<int[]>("[-1,0]").ToArray(), StringTo<int[]>("[-1,0]").ToArray() };
            //yield return new object[] { StringTo<int[]>("[0,1]").ToArray(), StringTo<int[]>("[1,0]").ToArray() };
            yield return new object[] { StringTo<int[]>("[0,1,1,2]").ToArray(), StringTo<int[]>("[2,0]").ToArray() };
        }
    }
    [Data]
    public int[] SingleNumber(int[] nums)
    {
        if (nums.Length == 2) return nums;
        Array.Sort(nums);
        List<int> res = new List<int>();
        for (int i = 0; i < nums.Length - 1;)
        {
            var n = nums[i];
            var n_next = nums[i + 1];
            if (n == n_next)
            {
                i = i + 2;
                if (i == nums.Length - 1)
                {
                    res.Add(nums[nums.Length - 1]);
                }
                continue;
            }
            else
            {
                res.Add(n);
                i = i + 1;
                if (i == nums.Length - 1)
                {
                    res.Add(n_next);
                    break;
                }
            }
        }
        return res.ToArray();
    }
}