namespace Solutions;
/// <summary>
/// 145. 二叉树的后序遍历
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class PostorderTraversalSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,null,2,3]"), StringTo<List<int>>("[3,2,1]") };
            yield return new object[] { ToTreeNodeWithString("[]"), StringTo<List<int>>("[]") };
            yield return new object[] { ToTreeNodeWithString("[1]"), StringTo<List<int>>("[1]") };
        }
    }
    [Data]
    public IList<int> PostorderTraversal(TreeNode root)
    {
        IList<int> res = new List<int>();
        postorder(root, res);
        return res;
    }

    private void postorder(TreeNode root, IList<int> res)
    {
        if (root == null) return;
        postorder(root.left, res);
        postorder(root.right, res);
        res.Add(root.val);
    }
}