namespace Solutions;
/// <summary>
/// 112. 路径总和
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class HasPathSumSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[5,4,8,11,null,13,4,7,2,null,null,null,1]"), 22, true };
            yield return new object[] { ToTreeNodeWithString("[1,2,3]"), 5, false };
            yield return new object[] { ToTreeNodeWithString("[]"), 0, false };
        }
    }
    // 深度优先
    //[Data]
    public bool HasPathSum(TreeNode root, int targetSum)
    {
        if (root == null) return false;
        if (root.left == null && root.right == null)
        {
            return root.val == targetSum;
        }

        return HasPathSum(root.left, targetSum - root.val) || HasPathSum(root.right, targetSum - root.val);
    }

    // 广度优先
    [Data]
    public bool HasPathSum1(TreeNode root, int targetSum)
    {
        if (root == null) return false;
        Queue<TreeNode> queue = new Queue<TreeNode>();
        Queue<int> sum_queue = new Queue<int>();
        queue.Enqueue(root);
        sum_queue.Enqueue(root.val);
        while (queue.Count > 0)
        {
            int size = queue.Count;

            for (int i = 0; i < size; i++)
            {
                TreeNode node = queue.Dequeue();
                int sum = sum_queue.Dequeue();
                if (node.left == null && node.right == null)
                {
                    if (sum == targetSum) return true;
                    continue;
                }
                if (node.left != null)
                {
                    queue.Enqueue(node.left);
                    sum_queue.Enqueue(sum + node.left.val);
                }
                if (node.right != null)
                {
                    queue.Enqueue(node.right);
                    sum_queue.Enqueue(sum + node.right.val);
                }
            }
        }
        return false;
    }

}