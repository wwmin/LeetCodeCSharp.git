namespace Solutions;
/// <summary>
/// 168. Excel表列名称
/// </summary>
public class ConvertToTitleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, "A" };
            yield return new object[] { 28, "AB" };
            yield return new object[] { 701, "ZY" };
            yield return new object[] { 2147483647, "FXSHRXW" };
        }
    }

    [Data]
    public string ConvertToTitle(int columnNumber)
    {
        //相当于10进制转26进制
        //1-26 对应 A-Z
        //可以使用数字 1 - 26 表示进制数,最后转化为A-Z
        //采用余数法
        StringBuilder sb = new StringBuilder();
        while (columnNumber > 0)
        {
            //int a = (columnNumber - 1) % 26 + 1;
            //sb.Append((char)(a - 1 + 'A'));
            //columnNumber = (columnNumber - a)/ 26;
            //使用--1
            columnNumber--;
            int a = columnNumber % 26;
            sb.Append((char)(a + 'A'));
            columnNumber /= 26;
        }
        string ans = sb.ToString();
        ans = string.Join("", ans.Reverse());
        return ans;
    }
}