namespace Solutions;
/// <summary>
/// 1447. 跳跃游戏 IV
/// </summary>
public class MinJumpsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[100,-23,-23,404,100,23,23,23,3,404]").ToArray(), 3 };
            yield return new object[] { StringTo<int[]>("[7]").ToArray(), 0 };
            yield return new object[] { StringTo<int[]>("[7,6,9,6,9,6,9,7]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[6,1,9]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[11,22,7,7,7,7,7,7,7,22,13]").ToArray(), 3 };
        }
    }

    [Data]
    public int MinJumps(int[] arr)
    {
        Dictionary<int, IList<int>> idxSameValue = new Dictionary<int, IList<int>>();
        for (int i = 0; i < arr.Length; i++)
        {
            if (!idxSameValue.ContainsKey(arr[i]))
            {
                idxSameValue.Add(arr[i], new List<int>());
            }
            idxSameValue[arr[i]].Add(i);
        }
        ISet<int> visitedIndex = new HashSet<int>();
        Queue<int[]> queue = new Queue<int[]>();
        queue.Enqueue(new int[] { 0, 0 });
        visitedIndex.Add(0);
        while (queue.Count > 0)
        {
            int[] idxStep = queue.Dequeue();
            int idx = idxStep[0], step = idxStep[1];
            if (idx == arr.Length - 1)
            {
                return step;
            }
            int v = arr[idx];
            step++;
            if (idxSameValue.ContainsKey(v))
            {
                foreach (int i in idxSameValue[v])
                {
                    if (visitedIndex.Add(i))
                    {
                        queue.Enqueue(new int[] { i, step });
                    }
                }
                idxSameValue.Remove(v);
            }
            if (idx + 1 < arr.Length && visitedIndex.Add(idx + 1))
            {
                queue.Enqueue(new int[] { idx + 1, step });
            }
            if (idx - 1 >= 0 && visitedIndex.Add(idx - 1))
            {
                queue.Enqueue(new int[] { idx - 1, step });
            }
        }
        return 0;
    }
}