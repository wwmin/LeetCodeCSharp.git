namespace Solutions;
/// <summary>
/// 190. 颠倒二进制位
/// </summary>
public class ReverseBitsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { BinaryStringTo<uint>("00000010100101000001111010011100"), BinaryStringTo<uint>("00111001011110000010100101000000") };
            yield return new object[] { BinaryStringTo<uint>("11111111111111111111111111111101"), BinaryStringTo<uint>("10111111111111111111111111111111") };
        }
    }

    [Data]
    public uint reverseBits(uint n)
    {
        uint result = 0;
        for (int i = 0; i < 32; i++)
        {
            result <<= 1;//结果左移
            result += n % 2;//结果为N最后一位数
            n >>= 1;//N右移
        }
        return result;
    }

    public uint reverseBits1(uint n)
    {
        string s = Convert.ToString(n, toBase: 2).PadLeft(32, '0');
        s = string.Join("", s.Reverse());
        return Convert.ToUInt32(s, fromBase: 2);
    }
}