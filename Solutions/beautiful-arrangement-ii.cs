namespace Solutions;
/// <summary>
/// 667. 优美的排列 II
/// difficulty: Medium
/// https://leetcode.cn/problems/beautiful-arrangement-ii/
/// </summary>
public class ConstructArray_667_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 1, StringTo<int[]>("[1,2,3]").ToArray() };
            yield return new object[] { 3, 2, StringTo<int[]>("[1,3,2]").ToArray() };
        }
    }

    [Data]
    public int[] ConstructArray(int n, int k)
    {
        int[] answer = new int[n];
        int idx = 0;
        for (int i = 1; i < n - k; ++i)
        {
            answer[idx] = i;
            ++idx;
        }
        for (int i = n - k, j = n; i <= j; ++i, --j)
        {
            answer[idx] = i;
            ++idx;
            if (i != j)
            {
                answer[idx] = j;
                ++idx;
            }
        }
        return answer;
    }
}