namespace Solutions;
/// <summary>
/// 409. 最长回文串
/// </summary>
public class LongestPalindromeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abccccdd", 7 };
            yield return new object[] { "a", 1 };
            yield return new object[] { "bb", 2 };
        }
    }

    [Data]
    public int LongestPalindrome(string s)
    {
        Dictionary<char, int> map = new Dictionary<char, int>();
        for (int i = 0; i < s.Length; i++)
        {
            if (map.ContainsKey(s[i])) map[s[i]]++;
            else map.Add(s[i], 1);
        }
        int ans = 0;
        int oddNum = 0;
        var keys = map.Keys.ToArray();
        foreach (var key in keys)
        {
            if (map[key] % 2 != 0)
            {
                oddNum++;
                ans += --map[key];
            }
            else
            {
                ans += map[key];
            }
        }

        if (oddNum > 0)
        {
            ans++;
        }
        return ans;
    }
}