namespace Solutions;
/// <summary>
/// 171. Excel 表列序号
/// </summary>
public class TitleToNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "A", 1 };
            yield return new object[] { "AB", 28 };
            yield return new object[] { "ZY", 701 };
            yield return new object[] { "FXSHRXW", 2147483647 };
        }
    }

    [Data]
    public int TitleToNumber(string columnTitle)
    {
        //相当于26进制转10进制
        /*
         “FXSHRXW” 中的每个字母对应的序号分别是：
            [6,24,19,8,18,24,23][6,24,19,8,18,24,23]，则列名称对应的列序号为：
            23 x 26^0 + 24 x 26^1 + 18 x 26^2 + 8 x 26^3 + 19 x 26^4 + 24 x 26^5 + 6 x 26^6 = 2147483647

            模拟求解即可
        */
        int n = columnTitle.Length;
        int ans = 0;
        for (int i = n - 1; i >= 0; i--)
        {
            var a = columnTitle[i] - 'A' + 1;
            ans += (int)Math.Pow(26, n - i - 1) * a;
        }
        return ans;
    }
}