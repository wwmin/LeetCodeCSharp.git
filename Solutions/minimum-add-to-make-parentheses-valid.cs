namespace Solutions;
/// <summary>
/// 957. 使括号有效的最少添加
/// https://leetcode.cn/problems/minimum-add-to-make-parentheses-valid/
/// </summary>
public class MinAddToMakeValidSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "())", 1 };
            yield return new object[] { "(((", 3 };
        }
    }

    [Data]
    public int MinAddToMakeValid(string s)
    {
        int res = 0;
        Stack<char> stack = new Stack<char>();
        for (int i = 0; i < s.Length; i++)
        {

            if (s[i] == '(')
            {
                //如果是左括号
                stack.Push(s[i]);
            }
            else
            {
                //如果是右括号
                if (stack.Count == 0)
                {
                    //栈已经为空
                    res++;
                }
                else
                {
                    //栈不为空
                    stack.Pop();
                }
            }
        }
        //栈为空时的匹配个数,+ 栈中剩余的左括号个数
        return res + stack.Count;
    }
}