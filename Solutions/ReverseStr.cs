﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 541. 反转字符串 II
/// </summary>
public class ReverseStrSolution
{
    [InlineData("abcdefg", 2, "bacdfeg")]
    [InlineData("abcd", 2, "bacd")]
    [InlineData("a", 2, "a")]
    public string ReverseStr(string s, int k)
    {
        string res = "";

        int startK = 0;
        int endK = 0;
        int end2k = 0;

        string temp = "";
        string temp2k = "";
        for (int i = 1; i <= s.Length; i++)
        {
            if (i - startK <= k)
            {
                endK++;
                end2k++;
                temp += s[i - 1];
                continue;
            }
            if (i - startK > k && i - startK < 2 * k)
            {
                end2k++;
                temp2k += s[i - 1];
                continue;
            }
            if (i - startK == 2 * k)
            {
                temp2k += s[i - 1];
                startK = i;
                endK = i;
                end2k = i;

                var rs = string.Join("", ReverseFn(temp));
                res += rs + temp2k;
                temp = "";
                temp2k = "";
            }
        }
        //处理最后没有到2k的情况
        if (end2k > endK)
        {
            res += string.Join("", ReverseFn(temp)) + temp2k;
        }
        else
        {
            res += string.Join("", ReverseFn(temp));
        }
        return res;
    }

    /// <summary>
    /// 字符串反转
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    private char[] ReverseFn(string s)
    {
        int left = 0;
        int right = s.Length - 1;
        char[] charArr = s.ToCharArray();
        while (left < right)
        {
            char temp = s[left];
            charArr[left] = charArr[right];
            charArr[right] = temp;
            left++;
            right--;
        }
        return charArr;
    }
}
