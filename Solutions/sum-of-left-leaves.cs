namespace Solutions;
/// <summary>
/// 404. 左叶子之和
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class SumOfLeftLeavesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[3,9,20,null,null,15,7]"), 24 };
            yield return new object[] { ToTreeNodeWithString("[0,-4,-3,null,-1,8,null,null,3,null,-9,-2,null,4]"), 2 };
        }
    }
    [Data]
    public int SumOfLeftLeaves(TreeNode root)
    {
        int sum = 0;
        SumLeftLeaves(root, false, ref sum);
        return sum;
    }

    private void SumLeftLeaves(TreeNode root, bool isLeft, ref int sum)
    {
        //空节点直接返回
        if (root == null) return;
        //是叶子节点,且是左节点,sum+=val
        if (root.left == null && root.right == null && isLeft) sum += root.val;
        //继续遍历左节点,将isLeft标识为true
        SumLeftLeaves(root.left, true, ref sum);
        //继续遍历左节点,将isLeft标识为true
        SumLeftLeaves(root.right, false, ref sum);
    }
}