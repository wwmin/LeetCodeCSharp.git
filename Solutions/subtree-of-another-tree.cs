namespace Solutions;
/// <summary>
/// 572. 另一棵树的子树
/// https://leetcode.cn/problems/subtree-of-another-tree/comments/
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class IsSubtreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[3,4,5,1,2]"), ToTreeNodeWithString("[4,1,2]"), true };
            yield return new object[] { ToTreeNodeWithString("[3,4,5,1,2,null,null,null,null,0]"), ToTreeNodeWithString("[4,1,2]"), false };
        }
    }
    [Data]
    public bool IsSubtree(TreeNode root, TreeNode subRoot)
    {
        if (root == null) return false;
        return Check(root, subRoot) || IsSubtree(root.left, subRoot) || IsSubtree(root.right, subRoot);
    }

    private bool Check(TreeNode root, TreeNode subRoot)
    {
        if (root == null && subRoot == null) return true;
        if (root == null || subRoot == null || root.val != subRoot.val) return false;
        return Check(root.left, subRoot.left) && Check(root.right, subRoot.right);
    }
}