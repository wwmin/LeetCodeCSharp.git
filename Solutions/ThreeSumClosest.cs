﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 16. 最接近的三数之和
/// </summary>
public class ThreeSumClosestSolution
{
    [InlineData(new int[] { -1, 2, 1, -4 }, 1, 2)]
    [InlineData(new int[] { 1, 1, 1, 1 }, 4, 3)]
    [InlineData(new int[] { 0, 2, 1, -3 }, 1, 0)]
    [InlineData(new int[] { 1, 1, -1, -1, 3 }, -1, -1)]
    [InlineData(new int[] { -3, -2, -5, 3, -4 }, -1, -2)]
    [InlineData(new int[] { 1, 2, 4, 8, 16, 32, 64, 128 }, 82, 82)]
    [InlineData(new int[] { 0, 5, -1, -2, 4, -1, 0, -3, 4, -5 }, 1, 1)]
    [InlineData(new int[] { -100, -98, -2, -1 }, -101, -101)]
    public int ThreeSumClosest(int[] nums, int target)
    {
        if (nums.Length == 3) return nums.Sum();
        int minTarget = nums[0] + nums[1] + nums[2];
        int nl = nums.Length;
        //List<int> numList = nums.ToList();
        //numList.Sort();
        Array.Sort(nums);
        for (int i = 0; i < nl - 2; i++)
        {
            int left = i + 1;
            int right = nl - 1;
            //if (left >= right) return minTarget;
            //int temp = nums[i] + nums[left] + nums[right];
            //if (i == 0) minTarget = temp;
            //if (Math.Abs(target - temp) < Math.Abs(target - minTarget))
            //{
            //    minTarget = temp;
            //}
            //if (minTarget == target) return minTarget;
            //if (i > 0 && nums[i] == nums[i + 1]) continue;
            while (left < right)
            {
                int t = nums[i] + nums[left] + nums[right];

                if (t > target) right--;
                else if (t < target) left++;
                else return target;
                if (Math.Abs(target - t) < Math.Abs(target - minTarget))
                {
                    minTarget = t;
                }
            }
        }
        return minTarget;
    }
}
