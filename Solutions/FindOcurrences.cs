namespace Solutions;
/// <summary>
/// 1156. Bigram 分词
/// </summary>
public class FindOcurrencesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "alice is a good girl she is a good student", "a", "good", StringTo<string[]>("[\"girl\",\"student\"]").ToArray() };
            yield return new object[] { "we will we will rock you", "we", "will", StringTo<string[]>("[\"we\",\"rock\"]").ToArray() };
        }
    }

    [Data]
    public string[] FindOcurrences(string text, string first, string second)
    {
        var words = text.Split(' ');
        List<string> ans = new List<string>();
        for (int i = 0; i < words.Length - 2; i++)
        {
            if (words[i] == first && words[i + 1] == second) ans.Add(words[i + 2]);
        }
        return ans.ToArray();
    }
}