namespace Solutions;
/// <summary>
/// 697. 数组的度
/// https://leetcode.cn/problems/degree-of-an-array/
/// </summary>
public class FindShortestSubArraySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,2,2,3,1]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[1,2,2,3,1,4,2]").ToArray(), 6 };
        }
    }

    [Data]
    public int FindShortestSubArray(int[] nums)
    {
        Dictionary<int, KeyValue> dic = new Dictionary<int, KeyValue>();
        int max = 1;
        for (int i = 0; i < nums.Length; i++)
        {
            if (dic.ContainsKey(nums[i]))
            {
                dic[nums[i]].count++;
                dic[nums[i]].endIndex = i;

                if (dic[nums[i]].count > max)
                {
                    max = dic[nums[i]].count;
                }
            }
            else
            {
                dic.Add(nums[i], new KeyValue()
                {
                    key = nums[i],
                    count = 1,
                    startIndex = i,
                    endIndex = i
                });
            }
        }
        var keys = dic.Keys.ToArray();
        int minLen = nums.Length - 1;
        for (int i = 0; i < keys.Length; i++)
        {
            var key = keys[i];
            if (dic[key].count == max)
            {
                int len = dic[key].endIndex - dic[key].startIndex;
                if (len < minLen) minLen = len;
            }
        }
        return minLen + 1;
    }

    class KeyValue
    {
        public int key { get; set; }
        public int count { get; set; }
        public int startIndex { get; set; }
        public int endIndex { get; set; }
        public int len { get; set; }
    }
}