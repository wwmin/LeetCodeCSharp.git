namespace Solutions;
/// <summary>
/// 438. 找到字符串中所有字母异位词
/// </summary>
public class FindAnagramsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "cbaebabacd", "abc", StringTo<List<int>>("[0,6]") };
            yield return new object[] { "abab", "ab", StringTo<List<int>>("[0,1,2]") };
        }
    }
    [Data]
    public IList<int> FindAnagrams(string s, string p)
    {
        int sLen = s.Length;
        int pLen = p.Length;
        if (sLen < pLen)
        {
            return new List<int>();
        }
        IList<int> ans = new List<int>();
        int[] sCount = new int[26];
        int[] pCount = new int[26];
        for (int i = 0; i < pLen; i++)
        {
            ++sCount[s[i] - 'a'];
            ++pCount[p[i] - 'a'];
        }
        if (Enumerable.SequenceEqual(sCount, pCount))
        {
            ans.Add(0);
        }
        for (int i = 0; i < sLen - pLen; i++)
        {
            --sCount[s[i] - 'a'];
            ++sCount[s[i + pLen] - 'a'];
            if (Enumerable.SequenceEqual(sCount, pCount)) ans.Add(i + 1);
        }
        return ans;
    }
}