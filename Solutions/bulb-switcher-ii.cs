using Microsoft.Playwright;

using wwm.LeetCodeHelper.Servers;

namespace Solutions;
/// <summary>
/// 672. 灯泡开关 Ⅱ
/// https://leetcode.cn/problems/bulb-switcher-ii/
/// </summary>
public class FlipLightsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, 1, 2 };
            yield return new object[] { 2, 1, 3 };
            yield return new object[] { 3, 1, 4 };
        }
    }

    [Data]
    public int FlipLights(int n, int presses)
    {
        //不按开关
        if (presses == 0)
        {
            return 1;
        }
        //特殊情况处理
        if (n == 1)
        {
            return 2;
        }
        else if (n == 2)
        {
            //特殊情况
            return presses == 1 ? 3 : 4;
        }
        else
        {
            //n >= 3
            return presses == 1 ? 4 : presses == 2 ? 7 : 8;
        }
    }
}