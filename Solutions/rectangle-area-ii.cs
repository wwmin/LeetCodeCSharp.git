namespace Solutions;
/// <summary>
/// 880. 矩形面积 II
/// https://leetcode.cn/problems/rectangle-area-ii/
/// </summary>
public class RectangleAreaSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,0,2,2],[1,0,2,3],[1,0,3,1]]").ToArray(), 6 };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,1000000000,1000000000]]").ToArray(), 49 };
        }
    }

    [Data]
    public int RectangleArea(int[][] rectangles)
    {
        return default;
    }
}