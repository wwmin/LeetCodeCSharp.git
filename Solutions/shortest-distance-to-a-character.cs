namespace Solutions;
/// <summary>
/// 841. 字符的最短距离
/// https://leetcode.cn/problems/shortest-distance-to-a-character/
/// </summary>
public class ShortestToCharSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "loveleetcode", 'e', StringTo<int[]>("[3,2,1,0,1,0,0,1,2,2,1,0]").ToArray() };
            yield return new object[] { "aaab", 'b', StringTo<int[]>("[3,2,1,0]").ToArray() };
        }
    }

    [Data]
    public int[] ShortestToChar(string s, char c)
    {
        int[] res = new int[s.Length];
        //从左遍历一遍
        int leftIndex = -1;
        int[] leftArray = new int[s.Length];
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] != c)
            {
                leftArray[i] = leftIndex == -1 ? s.Length : i - leftIndex;
            }
            else
            {
                leftIndex = i;
                leftArray[i] = 0;
            }
        }
        //从右遍历一遍
        int rightIndex = -1;
        int[] rightArray = new int[s.Length];
        for (int i = s.Length - 1; i >= 0; i--)
        {
            if (s[i] != c)
            {
                rightArray[i] = rightIndex == -1 ? s.Length : rightIndex - i;
            }
            else
            {
                rightIndex = i;
                rightArray[i] = 0;
            }
        }
        //将左右数组相同坐标取最小
        for (int i = 0; i < s.Length; i++)
        {
            res[i] = Math.Min(leftArray[i], rightArray[i]);
        }
        return res;
    }
}