namespace Solutions;
/// <summary>
/// 334. 递增的三元子序列
/// </summary>
public class IncreasingTripletSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[5,4,3,2,1]").ToArray(), false };
            yield return new object[] { StringTo<int[]>("[2,1,5,0,4,6]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[1,2,1,2,1,2,1,2]").ToArray(), false };
        }
    }

    [Data]
    public bool IncreasingTriplet(int[] nums)
    {
        int a = int.MaxValue;
        int b = int.MaxValue;
        foreach (var num in nums)
        {
            if (num <= a) a = num;
            else if (num <= b) b = num;
            else return true;
        }
        return false;
    }

    //[Data]
    //此法超时
    public bool IncreasingTriplet1(int[] nums)
    {
        int n = nums.Length;
        if (n <= 2) return false;
        for (int i = 0; i < n - 2; i++)
        {
            int first = nums[i];
            for (int j = i + 1; j < n - 1; j++)
            {
                int second = nums[j];
                if (second <= first) continue;
                for (int k = j + 1; k < n; k++)
                {
                    int third = nums[k];
                    if (third <= second) continue;
                    return true;
                }
            }
        }
        return false;
    }
}