namespace Solutions;
/// <summary>
/// 234. 回文链表
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class IsPalindrome_234Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2,2,1]"), true };
            yield return new object[] { ToListNodeWithString("[1,2]"), false };
        }
    }
    [Data]
    public bool IsPalindrome_234(ListNode head)
    {
        ListNode cur = head;
        int n = 0;
        //翻转NodeList
        ListNode foot = new ListNode();
        while (cur != null)
        {
            //记录node的总长度,以便在后续只需要与翻转后的NodeList对比一半的值就可以
            n++;
            //构造新的NodeList,且next指向原foot
            ListNode node = new ListNode(cur.val, foot);
            //将foot指针移动到新的NodeList
            foot = node;
            cur = cur.next;
        }
        int m = n / 2;
        while (m >= 0)
        {
            if (head.val != foot.val) return false;
            head = head.next;
            foot = foot.next;
            m--;
        }
        return true;
    }
}