﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 43. 字符串相乘
/// </summary>
public class MultiplySolution
{
    [InlineData("2", "3", "6")]
    [InlineData("123", "456", "56088")]
    public string Multiply(string num1, string num2)
    {
        if (num1 == "0" || num2 == "0") return "0";
        string ans = "0";
        int m = num1.Length;
        int n = num2.Length;
        for (int i = n - 1; i >= 0; i--)
        {
            StringBuilder curr = new StringBuilder();
            int add = 0;
            for (int j = n - 1; j > i; j--)
            {
                curr.Append(0);
            }
            int y = num2[i] - '0';
            for (int j = m - 1; j >= 0; j--)
            {
                int x = num1[j] - '0';
                int product = x * y + add;
                curr.Append(product % 10);
                add = product / 10;
            }
            if (add != 0)
            {
                curr.Append(add % 10);
            }
            ans = addString(ans, string.Join("", curr.ToString().Reverse()));
        }
        return ans;
    }

    private string addString(string num1, string num2)
    {
        int i = num1.Length - 1;
        int j = num2.Length - 1;
        int add = 0;
        StringBuilder ans = new StringBuilder();
        while (i >= 0 || j >= 0 || add != 0)
        {
            int x = i >= 0 ? num1[i] - '0' : 0;
            int y = j >= 0 ? num2[j] - '0' : 0;
            int result = x + y + add;
            ans.Append(result % 10);
            add = result / 10;
            i--;
            j--;
        }
        return string.Join("", ans.ToString().Reverse());
    }
}
