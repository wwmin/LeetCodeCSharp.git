namespace Solutions;
/// <summary>
/// 125. 验证回文串
/// </summary>
public class IsPalindrome_125Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "A man, a plan, a canal: Panama", true };
            yield return new object[] { "race a car", false };
            yield return new object[] { ".,", true };
        }
    }
    private Dictionary<char, bool> validCharMap = new Dictionary<char, bool>()
    {
        { '0',true },
        { '1',true },
        { '2',true },
        { '3',true },
        { '4',true },
        { '5',true },
        { '6',true },
        { '7',true },
        { '8',true },
        { '9',true },
        {'a',true },
        {'b',true },
        {'c',true },
        {'d',true },
        {'e',true },
        {'f',true },
        {'g',true },
        {'h',true },
        {'i',true },
        {'j',true },
        {'k',true },
        {'l',true },
        {'m',true },
        {'n',true },
        {'o',true },
        {'p',true },
        {'q',true },
        {'r',true },
        {'s',true },
        {'t',true },
        {'u',true },
        {'v',true },
        {'w',true },
        {'x',true },
        {'y',true },
        {'z',true },
        {'A',true },
        {'B',true },
        {'C',true },
        {'D',true },
        {'E',true },
        {'F',true },
        {'G',true },
        {'H',true },
        {'I',true },
        {'J',true },
        {'K',true },
        {'L',true },
        {'M',true },
        {'N',true },
        {'O',true },
        {'P',true },
        {'Q',true },
        {'R',true },
        {'S',true },
        {'T',true },
        {'U',true },
        {'V',true },
        {'W',true },
        {'X',true },
        {'Y',true },
        {'Z',true }
    };
    [Data]
    public bool IsPalindrome_125(string s)
    {
        int n = s.Length;
        int left = 0;
        int right = n - 1;
        while (left < right)
        {
            while (!char.IsLetterOrDigit(s[left]))
            {
                if (left < right) left++;
                else break;
            }
            while (!char.IsLetterOrDigit(s[right]))
            {
                if (right > left) right--;
                else break;
            }
            if (left == right)
            {
                return true;
            }
            char a = s[left];
            char b = s[right];

            if (a == b)
            {
                left++;
                right--;
            };
            if (a != b)
            {
                string sa = a.ToString();
                string sb = b.ToString();
                if (sa.Equals(sb, StringComparison.OrdinalIgnoreCase))
                {
                    left++;
                    right--;
                }
                else return false;
            }
        }
        return true;
    }

    public bool IsPalindrome1_125(string s)
    {
        int n = s.Length;
        int left = 0;
        int right = n - 1;
        while (left < right)
        {
            while (!validCharMap.ContainsKey(s[left]))
            {
                if (left < right) left++;
                else break;
            }
            while (!validCharMap.ContainsKey(s[right]))
            {
                if (right > left) right--;
                else break;
            }
            if (left == right)
            {
                return true;
            }
            char a = s[left];
            char b = s[right];
            if (a == b)
            {
                left++;
                right--;
            };
            if (a != b)
            {
                string sa = a.ToString();
                string sb = b.ToString();
                if (sa.Equals(sb, StringComparison.OrdinalIgnoreCase))
                {
                    left++;
                    right--;
                }
                else return false;
            }
        }
        return true;
    }
}