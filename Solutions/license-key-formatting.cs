namespace Solutions;
/// <summary>
/// 482. 密钥格式化
/// </summary>
public class LicenseKeyFormattingSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "5F3Z-2e-9-w", 4, "5F3Z-2E9W" };
            yield return new object[] { "2-5g-3-J", 2, "2-5G-3J" };
            yield return new object[] { "2", 2, "2" };
            yield return new object[] { "a-a-a-a-", 1, "A-A-A-A" };
        }
    }

    [Data]
    public string LicenseKeyFormatting(string s, int k)
    {
        StringBuilder sb = new StringBuilder();
        List<char> cs = new List<char>();
        for (int i = 0; i < s.Length; i++)
        {
            if (s[i] == '-') continue;
            cs.Add(s[i]);
        }
        int left = cs.Count % k;
        for (int i = 0; i < left; i++)
        {
            sb.Append(char.IsLetter(cs[i]) ? cs[i].ToString().ToUpper() : cs[i]);
        }
        if (left > 0 && k > left && left < cs.Count) sb.Append('-');
        for (int i = left; i < cs.Count;)
        {
            for (int j = 0; j < k; j++)
            {

                sb.Append(char.IsLetter(cs[i]) ? cs[i].ToString().ToUpper() : cs[i]);
                i++;
            }
            if (i < cs.Count) sb.Append('-');
        }
        return sb.ToString();
    }
}