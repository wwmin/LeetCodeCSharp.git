﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    public class MaxScoreIndicesSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[0,0,1,0]").ToArray(), StringTo<int[]>("[2,4]").ToArray() };
                yield return new object[] { StringTo<int[]>("[0,0,0]").ToArray(), StringTo<int[]>("[3]").ToArray() };
                yield return new object[] { StringTo<int[]>("[1,1]").ToArray(), StringTo<int[]>("[0]").ToArray() };
            }
        }
        [Data]
        public IList<int> MaxScoreIndices(int[] nums)
        {
            IList<int> ans = new List<int>();
            int n = nums.Length;
            int[] groupScores = new int[n + 1];
            int allOneNum = 0;
            for (int i = 0; i < n; i++)
            {
                if (nums[i] == 1) allOneNum++;
            }
            //第一组就是nums
            int maxScore = Math.Max(allOneNum, 0);
            groupScores[0] = allOneNum;
            int leftOnes = 0;
            int leftZeros = 0;
            for (int i = 0; i < n; i++)
            {
                int currInt = nums[i];
                if (currInt == 1)
                {
                    leftOnes++;
                }
                else
                {
                    leftZeros++;
                }
                groupScores[i + 1] = leftZeros + allOneNum - leftOnes;
                maxScore = Math.Max(maxScore, groupScores[i + 1]);
            }
            for (int i = 0; i < n + 1; i++)
            {
                if (groupScores[i] == maxScore)
                {
                    ans.Add(i);
                }
            }
            return ans.ToArray();
        }
    }
}