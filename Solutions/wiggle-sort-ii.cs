namespace Solutions;
/// <summary>
/// 324. 摆动排序 II
/// difficulty: Medium
/// https://leetcode.cn/problems/wiggle-sort-ii/
/// </summary>
public class WiggleSort_324_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,5,1,1,6,4]").ToArray(), StringTo<int[]>("[1,6,1,5,1,4]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,3,2,2,3,1]").ToArray(), StringTo<int[]>("[2,3,1,3,1,2]").ToArray() };
        }
    }

    [Data]
    public void WiggleSort(int[] nums)
    {
        int n = nums.Length;
        int[] arr = new int[n];
        Array.Copy(nums, arr, n);
        Array.Sort(arr);
        int x = (n + 1) / 2;
        for (int i = 0, j = x - 1, k = n - 1; i < n; i += 2, j--, k--)
        {
            nums[i] = arr[j];
            if (i + 1 < n)
            {
                nums[i + 1] = arr[k];
            }
        }
    }
}