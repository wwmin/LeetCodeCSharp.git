﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5986. 设置时间的最少代价
    /// </summary>
    public class MinCostSetTimeSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 1, 2, 1, 600, 6 };
                yield return new object[] { 0, 1, 2, 76, 6 };
                yield return new object[] { 1, 1, 1, 1, 1 };
                yield return new object[] { 0, 821, 389, 33, 1599 };
                yield return new object[] { 5, 3269, 3270, 650, 26156 };
                yield return new object[] { 1, 9403, 9402, 6008, 65817 };
                yield return new object[] { 9, 100000, 1, 6039, 4 };
                yield return new object[] { 7, 220, 479, 6000, 2576 };
            }
        }
        [Data]
        public int MinCostSetTime(int startAt, int moveCost, int pushCost, int targetSeconds)
        {
            //move和push次数相同,必要比较moveCost和pushCost
            //出现相同数字 不需要move
            //1分钟可以使用60秒代替
            //只有两种情况
            //使用60秒代替一分钟,不使用60秒代替一分钟
            int min = targetSeconds / 60;
            int sec = targetSeconds % 60;

            string num1 = (min == 0 ? "" : min.ToString()) + ((sec < 10 && min > 0) ? "0" + sec.ToString() : sec.ToString());
            string num2 = sec + 60 > 99 ? num1 : (min == 0 ? sec.ToString() : ((min - 1 > 0 ? (min - 1).ToString() : "") + (sec + 60).ToString()));
            string t = targetSeconds.ToString();
            if (min == 100 && t.Length == 4)
            {

                if (sec + 60 < 100)
                {
                    num1 = (min - 1).ToString() + (sec + 60).ToString();
                    num2 = num1;
                }
            }
            int cost1 = costAll(num1, startAt, moveCost, pushCost);
            int cost2 = costAll(num2, startAt, moveCost, pushCost);
            return Math.Min(cost1, cost2);
        }

        private int costAll(string num_string, int startAt, int moveCost, int pushCost)
        {
            int cost = 0;
            for (int i = 0; i < num_string.Length; i++)
            {
                if (i == 0)
                {
                    if (startAt == Convert.ToInt32(num_string[i].ToString())) cost += pushCost;
                    else cost += moveCost + pushCost;
                }
                else
                {
                    if (num_string[i] == num_string[i - 1])
                    {
                        cost += pushCost;
                    }
                    else
                    {
                        cost += moveCost + pushCost;
                    }
                }
            }
            return cost;
        }
    }
}