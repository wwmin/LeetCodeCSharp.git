namespace Solutions;
/// <summary>
/// 345. 反转字符串中的元音字母
/// </summary>
public class ReverseVowelsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "hello", "holle" };
            yield return new object[] { "leetcode", "leotcede" };
        }
    }
    Dictionary<char, bool> vowelsMap = new Dictionary<char, bool>()
    {
        { 'a',true },{'e',true },{'i',true },{'o',true },{'u',true },
        { 'A',true },{'E',true },{'I',true },{'O',true },{'U',true }
    };

    [Data]
    public string ReverseVowels(string s)
    {
        int n = s.Length;
        if (n <= 1) return s;
        char[] ss = s.ToCharArray();
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            while (left < n - 1 && !vowelsMap.ContainsKey(s[left]))
            {
                left++;
            }
            while (right >= 0 && !vowelsMap.ContainsKey(s[right]))
            {
                right--;
            }
            if (left >= right) break;
            char temp = ss[left];
            ss[left] = ss[right];
            ss[right] = temp;
            left++;
            right--;
        }
        return string.Join("", ss);
    }
}