namespace Solutions;
/// <summary>
/// 1764. 通过连接另一个数组的子数组得到一个数组
/// difficulty: Medium
/// https://leetcode.cn/problems/form-array-by-concatenating-subarrays-of-another-array/
/// </summary>
public class CanChoose_1764_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,-1,-1],[3,-2,0]]").ToArray(), StringTo<int[]>("[1,-1,0,1,-1,-1,3,-2,0]").ToArray(), true };
            yield return new object[] { StringTo<List<int[]>>("[[10,-2],[1,2,3,4]]").ToArray(), StringTo<int[]>("[1,2,3,4,10,-2]").ToArray(), false };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,3],[3,4]]").ToArray(), StringTo<int[]>("[7,7,1,2,3,4,7,7]").ToArray(), false };
        }
    }

    [Data]
    public bool CanChoose(int[][] groups, int[] nums)
    {
        int i = 0;
        for (int k = 0; k < nums.Length && i < groups.Length;)
        {
            if (Check(groups[i], nums, k))
            {
                k += groups[i].Length;
                i++;
            }
            else
            {
                k++;
            }
        }
        return i == groups.Length;
    }

    public bool Check(int[] g, int[] nums, int k)
    {
        if (k + g.Length > nums.Length)
        {
            return false;
        }
        for (int j = 0; j < g.Length; j++)
        {
            if (g[j] != nums[k + j])
            {
                return false;
            }
        }
        return true;
    }
}