namespace Solutions;
/// <summary>
/// 1260. 一年中的第几天
/// </summary>
public class DayOfYearSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "2019-01-09", 9 };
            yield return new object[] { "2019-02-10", 41 };
            yield return new object[] { "2003-03-01", 60 };
            yield return new object[] { "2004-03-01", 61 };
        }
    }
    private readonly int[] _monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    [Data]
    public int DayOfYear(string date)
    {
        //DateOnly d = DateOnly.Parse(date);
        DateTime d = DateTime.Parse(date);
        int sum = 0;
        for (int i = 0; i < d.Month - 1; i++)
        {
            sum += _monthDays[i];
        }
        if (d.Month > 2)
        {
            //判断是否为闰年,如果是闰年则2月加1
            if (d.Year % 400 == 0 || (d.Year % 4 == 0 && d.Year % 100 != 0))
            {
                sum += 1;
            }
        }
        return sum + d.Day;
    }
}