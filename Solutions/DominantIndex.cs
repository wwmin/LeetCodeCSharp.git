namespace Solutions;
/// <summary>
/// 748. 至少是其他数字两倍的最大数
/// </summary>
public class DominantIndexSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,6,1,0]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), -1 };
            yield return new object[] { StringTo<int[]>("[1]").ToArray(), 0 };
        }
    }

    [Data]
    public int DominantIndex(int[] nums)
    {
        int n = nums.Length;
        int first_large = 0;
        int first_large_i = -1;
        int second_large = 0;
        for (int i = 0; i < n; i++)
        {
            if (nums[i] > first_large)
            {
                second_large = first_large;
                first_large = nums[i];
                first_large_i = i;
            }
            else if (nums[i] > second_large)
            {
                second_large = nums[i];
            }
        }
        return first_large >= second_large * 2 ? first_large_i : -1;
    }
}