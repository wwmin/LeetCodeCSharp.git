namespace Solutions;
/// <summary>
/// 136. 只出现一次的数字
/// </summary>
public class SingleNumber_136Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,2,1]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[4,1,2,1,2]").ToArray(), 4 };
        }
    }

    [Data]
    public int SingleNumber_136(int[] nums)
    {
        Dictionary<int, int> map = new Dictionary<int, int>();
        for (int i = 0; i < nums.Length; i++)
        {
            if (map.ContainsKey(nums[i])) map[nums[i]]++;
            else map[nums[i]] = 1;
        }
        int[] keys = map.Keys.ToArray();
        for (int i = 0; i < keys.Length; i++)
        {
            int key = keys[i];
            if (map[key] == 1) return key;
        }
        return -1;
    }
}