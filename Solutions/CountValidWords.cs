namespace Solutions;
/// <summary>
/// 2173. 句子中的有效单词数
/// </summary>
public class CountValidWordsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "cat and  dog", 3 };
            yield return new object[] { "!this  1-s b8d!", 0 };
            yield return new object[] { "alice and  bob are playing stone-game10", 5 };
            yield return new object[] { "he bought 2 pencils, 3 erasers, and 1  pencil-sharpener.", 6 };
        }
    }

    [Data]
    public int CountValidWords(string sentence)
    {
        string[] words = sentence.Split(' ');
        int ans = 0;
        foreach (var word in words)
        {
            if (word.Trim().Length == 0) continue;
            if (isValid(word)) ans++;
        }
        return ans;
    }

    public bool isValid(string word)
    {
        int n = word.Length;
        bool hasHyphens = false;
        for (int i = 0; i < n; i++)
        {
            if (char.IsDigit(word[i]))
            {
                return false;
            }
            else if (word[i] == '-')
            {
                if (hasHyphens == true || i == 0 || i == n - 1 || !char.IsLetter(word[i - 1]) || !char.IsLetter(word[i + 1]))
                {
                    return false;
                }
                hasHyphens = true;
            }
            else if (word[i] == '!' || word[i] == '.' || word[i] == ',')
            {
                if (i != n - 1)
                {
                    return false;
                }
            }
        }
        return true;
    }
}