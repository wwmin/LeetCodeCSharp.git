namespace Solutions;
/// <summary>
/// 1833. 找到最高海拔
/// https://leetcode.cn/problems/find-the-highest-altitude/
/// </summary>
public class LargestAltitudeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[-5,1,5,0,-7]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[-4,-3,-2,-1,4,3,2]").ToArray(), 0 };
        }
    }

    [Data]
    public int LargestAltitude(int[] gain)
    {
        int max = 0;
        int sum = 0;
        for (int i = 0; i < gain.Length; i++)
        {
            sum += gain[i];
            max = Math.Max(max, sum);
        }
        return max;
    }
}