namespace Solutions;
/// <summary>
/// 1331. 黄金矿工
/// </summary>
public class GetMaximumGoldSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[0,6,0],[5,8,7],[0,9,0]]").ToArray(), 24 };
            yield return new object[] { StringTo<List<int[]>>("[[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]").ToArray(), 28 };
        }
    }

    [Data]
    public int GetMaximumGold(int[][] grid)
    {
        //边界条件的判断
        if (grid == null || grid.Length == 0) return 0;
        //保存最大路径值
        int ans = 0;
        //两个for循环,遍历每一个位置,让他们当作起点
        //查找最大路径值
        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[0].Length; j++)
            {
                //函数dfs是以坐标(i,j)为起点,查找最大路径值
                ans = Math.Max(ans, dfs(grid, i, j));
            }
        }
        //返回最大路径值
        return ans;
    }

    private int dfs(int[][] grid, int i, int j)
    {
        //边界条件的判断,i,j都不能越界，同时当前坐标的位置如果是0，表示有障碍物
        //或者遍历过了
        if (i < 0 || i >= grid.Length || j < 0 || j >= grid[0].Length || grid[i][j] == 0) return 0;
        //先把当前坐标的值保存下来,最后再还原
        int temp = grid[i][j];
        //当前坐标已经访问过了,要把他标记为0,防止再次访问
        grid[i][j] = 0;
        //然后沿着当前坐标的上下左右4个方向查找
        int up = dfs(grid, i - 1, j);//上
        int down = dfs(grid, i + 1, j);//下
        int left = dfs(grid, i, j - 1);//左
        int right = dfs(grid, i, j + 1);//右
        //取4个方向的最大值
        int max = Math.Max(up, Math.Max(down, Math.Max(left, right)));
        //然后再把当前位置的值还原
        grid[i][j] = temp;
        //返回最大路径值
        return grid[i][j] + max;
    }
}