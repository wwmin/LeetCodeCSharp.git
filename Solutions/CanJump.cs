﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 55. 跳跃游戏
/// </summary>
public class CanJumpSolution
{
    [InlineData(new int[] { 2, 0, 0 }, true)]
    //[InlineData(new int[] { 2, 3, 1, 1, 4 }, true)]
    //[InlineData(new int[] { 3, 2, 1, 0, 4 }, false)]
    public bool CanJump(int[] nums)
    {
        int n = nums.Length;
        if (n == 1) return true;
        //目前跳到的最远距离
        int max_far = 0;
        //跳跃总步数
        int step = 0;
        //上次跳跃可达范围右边界
        int end = 0;
        for (int i = 0; i < n; i++)
        {
            max_far = Math.Max(max_far, i + nums[i]);
            if (i == end)
            {
                end = max_far;
                step++;
                if (end >= n - 1) return true;
            }
        }
        return false;
    }
}
