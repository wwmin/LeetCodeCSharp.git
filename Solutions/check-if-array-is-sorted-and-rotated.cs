namespace Solutions;
/// <summary>
/// 1878. 检查数组是否经排序和轮转得到
/// https://leetcode.cn/problems/check-if-array-is-sorted-and-rotated/
/// </summary>
public class CheckSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,4,5,1,2]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[2,1,3,4]").ToArray(), false };
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), true };
        }
    }

    [Data]
    public bool Check(int[] nums)
    {
        int cnt = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            if (nums[i] > nums[(i + 1) % nums.Length]) cnt++;
        }
        return cnt <= 1;
    }
}