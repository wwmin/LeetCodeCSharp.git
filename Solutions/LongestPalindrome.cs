﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 5. 最长回文子串
/// </summary>
public class LongestPalindromeSolution
{
    public void Test()
    {
        var s = "ccd";
        var res = LongestPalindrome(s);
        Console.WriteLine(res);
    }
    public string LongestPalindrome(string s)
    {
        Span<char> ss = s.ToArray().AsSpan();
        int si = 0;
        int ei = ss.Length - 1;
        Span<char> maxS = new Span<char>();
        while (si <= ei)
        {
            var i = si;
            var j = ei;
            while (i <= ei)
            {
                if (ei - i <= maxS.Length) break;
                var sp = ss[i..(ei + 1)];
                var b = isPalindrome(sp);
                if (b && sp.Length > maxS.Length) maxS = sp;
                i++;
            }
            j--;
            while (j >= si)
            {
                if (j - si <= maxS.Length) break;
                var sp = ss[si..(j + 1)];
                var b = isPalindrome(sp);
                if (b && sp.Length > maxS.Length) maxS = sp;
                j--;
            }
            si++;
            ei--;
        }
        return maxS.ToString();
    }

    private bool isPalindrome(Span<char> ss)
    {
        if (ss.Length == 1) return true;
        if (ss.Length == 2 && ss[0] == ss[1]) return true;
        int si = 0;
        int ei = ss.Length - 1;
        while (si <= ei)
        {
            if (ss[si] != ss[ei]) return false;
            si++;
            ei--;
        }
        return true;
    }
}
