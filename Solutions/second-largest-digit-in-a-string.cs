namespace Solutions;
/// <summary>
/// 1796. 字符串中第二大的数字
/// difficulty: Easy
/// https://leetcode.cn/problems/second-largest-digit-in-a-string/
/// </summary>
public class SecondHighest_1796_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "dfa12321afd", 2 };
            yield return new object[] { "abc1111", -1 };
        }
    }

    [Data]
    public int SecondHighest(string s)
    {
        var max = -1;
        var second = -1;
        for (int i = 0; i < s.Length; i++)
        {
            if (Char.IsDigit(s[i]))
            {
                var num = s[i] - '0';
                if (num > max)
                {
                    second = max;
                    max = num;
                }
                else if (num > second && num < max)
                {
                    second = num;
                }
            }
        }
        return second;
    }
}