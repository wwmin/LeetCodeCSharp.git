namespace Solutions;
/// <summary>
/// 382. 链表随机节点
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class Solution_382Solution
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"Solution_382Solution\",\"getRandom\",\"getRandom\",\"getRandom\",\"getRandom\",\"getRandom\"]", "[[[1,2,3]],[],[],[],[],[]]", "[null,1,3,2,2,3]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }
    private int n = 0;
    private ListNode node = null;
    public Solution_382Solution(ListNode head)
    {
        node = head;
        while (head != null)
        {
            n++;
            head = head.next;
        }
    }

    public int GetRandom()
    {
        Random random = new Random();
        int r = random.Next(0, n);
        ListNode cur = node;
        for (int i = 0; i < r; i++)
        {
            cur = cur.next;
        }
        return cur.val;
    }
}

/**
 * Your Solution_382Solution object will be instantiated and called as such:
 * Solution_382Solution obj = new Solution_382Solution(head);
 * int param_1 = obj.GetRandom();
 */