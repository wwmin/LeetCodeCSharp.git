﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5980. 将字符串拆分为若干长度为 k 的组
    /// </summary>
    public class DivideStringSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { "abcdefghi", 3, 'x', StringTo<string[]>("[\"abc\",\"def\",\"ghi\"]") };
                yield return new object[] { "abcdefghij", 3, 'x', StringTo<string[]>("[\"abc\",\"def\",\"ghi\",\"jxx\"]") };
            }
        }
        [Data]
        public string[] DivideString(string s, int k, char fill)
        {
            List<string> list = new List<string>();
            int n = s.Length;
            for (int i = 0; i < n;)
            {
                StringBuilder sb = new StringBuilder();
                for (int j = 0; j < k && i < n; j++, i++)
                {
                    sb.Append(s[i]);
                }
                list.Add(sb.ToString());
            }
            string lastS = list[list.Count - 1];
            int lastN = lastS.Length;
            if (lastN < k)
            {
                for (int i = lastN; i < k; i++)
                {
                    lastS += fill;
                }
            }
            list[list.Count - 1] = lastS;
            return list.ToArray();
        }
    }
}