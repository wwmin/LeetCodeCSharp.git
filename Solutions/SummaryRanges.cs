namespace Solutions;
/// <summary>
/// 228. 汇总区间
/// </summary>
public class SummaryRangesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[-1]").ToArray(), StringTo<List<string>>("[\"-1\"]") };
            yield return new object[] { StringTo<int[]>("[-1,0]").ToArray(), StringTo<List<string>>("[\"-1->0\"]") };
            yield return new object[] { StringTo<int[]>("[-1,1]").ToArray(), StringTo<List<string>>("[\"-1\",\"1\"]") };
            yield return new object[] { StringTo<int[]>("[0,1,2,4,5,7]").ToArray(), StringTo<List<string>>("[\"0->2\",\"4->5\",\"7\"]") };
            yield return new object[] { StringTo<int[]>("[0,2,3,4,6,8,9]").ToArray(), StringTo<List<string>>("[\"0\",\"2->4\",\"6\",\"8->9\"]") };
        }
    }

    [Data]
    public IList<string> SummaryRanges(int[] nums)
    {
        IList<string> result = new List<string>();
        int n = nums.Length;
        if (n == 1)
        {
            result.Add(nums[0].ToString());
            return result;
        }
        for (int i = 0; i < n - 1;)
        {
            int start = nums[i];
            int end = nums[i];
            bool remainLastOne = false;
            while (i < n - 1)
            {
                if (nums[i] == nums[i + 1] - 1)
                {
                    end = nums[i + 1];
                    i++;
                }
                else
                {
                    i++;
                    if (i == n - 1)
                    {
                        remainLastOne = true;
                    }
                    break;
                };
            }

            string s = "";
            if (start == end) s = start + "";
            else s = start + "->" + end;
            result.Add(s);
            if (remainLastOne)
            {
                result.Add(nums[i].ToString());
            }
        }
        return result;
    }
}