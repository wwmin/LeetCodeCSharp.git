﻿namespace LeetCodeCSharp.Solutions
{
    public class MinimumCostSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[1,2,3]"), 5 };
                yield return new object[] { StringTo<int[]>("[6,5,7,9,2,2]"), 23 };
                yield return new object[] { StringTo<int[]>("[5,5]"), 10 };
            }
        }
        [Data]
        public int MinimumCost(int[] cost)
        {
            Array.Sort(cost);
            int n = cost.Length;
            int ans = 0;
            for (int i = n - 1; i >= 0; i--)
            {
                if (i - 1 >= 0)
                {
                    ans += cost[i] + cost[i - 1]; i = i - 2;
                }
                else
                {
                    ans += cost[i];
                }
            }
            return ans;
        }
    }
}