namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 495. 提莫攻击
/// </summary>
public class FindPoisonedDurationSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,4]").ToArray(), 2, 4 };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), 2, 3 };
        }
    }
    [Data]
    public int FindPoisonedDuration(int[] timeSeries, int duration)
    {
        int n = timeSeries.Length;
        if (n == 1) return duration;
        int ans = 0;
        for (int i = 0; i < n - 1; i++)
        {
            var cur = timeSeries[i];
            var next = timeSeries[i + 1];
            if (cur + duration - 1 >= next)
            {
                ans += next - cur;
            }
            else
            {
                ans += duration;
            }
        }
        //处理最后一个数字
        var last_num = timeSeries[n - 2];
        var end_num = timeSeries[n - 1];
        if (last_num - duration - 1 > end_num) ans += last_num - duration - 1 - end_num;
        else ans += duration;
        return ans;
    }
}