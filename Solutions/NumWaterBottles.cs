namespace Solutions;
/// <summary>
/// 1642. 换酒问题
/// </summary>
public class NumWaterBottlesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 9, 3, 13 };
            yield return new object[] { 15, 4, 19 };
            yield return new object[] { 5, 5, 6 };
            yield return new object[] { 2, 3, 2 };
        }
    }

    [Data]
    public int NumWaterBottles(int numBottles, int numExchange)
    {
        int ans = numBottles;
        while (numBottles >= numExchange)
        {
            int hasBottles = numBottles / numExchange;
            int leftBottles = numBottles % numExchange;
            ans += hasBottles;
            numBottles = hasBottles + leftBottles;
        }
        return ans;
    }
}