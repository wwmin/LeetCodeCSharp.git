﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 61. 旋转链表
/// </summary>
public class RotateRightSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2]"), 2, ToListNodeWithString("[1,2]") };
            yield return new object[] { ToListNodeWithString("[1]"), 1, ToListNodeWithString("[1]") };
            yield return new object[] { ToListNodeWithString("[1]"), 0, ToListNodeWithString("[1]") };
            yield return new object[] { ToListNodeWithString("[0,1,2]"), 4, ToListNodeWithString("[2,0,1]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4,5]"), 2, ToListNodeWithString("[4,5,1,2,3]") };
        }
    }
    [Data]
    public ListNode RotateRight(ListNode head, int k)
    {
        if (k == 0) return head;
        int l = 0;
        var copy = head;
        while (copy != null)
        {
            l++;
            copy = copy.next;
        }
        if (l <= 1) return head;
        var kk = k % l;
        if (kk == 0) return head;
        ListNode res = new ListNode(0);
        ListNode r = res;

        for (int i = 0; i < l - kk; i++)
        {
            r.next = new ListNode(head.val);
            r = r.next;
            head = head.next;
        }
        copy = head;
        while (copy.next != null)
        {
            copy = copy.next;
        }
        copy.next = res.next;
        return head;
    }
}
