﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 6002. 设计位集
    /// </summary>
    public class Bitset
    {
        bool[] bitset;
        int count = 0;
        bool flag;
        public Bitset(int size)
        {
            bitset = new bool[size];
            flag = false;
        }

        public void Fix(int idx)
        {
            if (flag)
            {
                if (bitset[idx] == true)
                {
                    bitset[idx] = false;
                    count++;
                }
            }
            else
            {
                if (bitset[idx] == false)
                {
                    bitset[idx] = true;
                    count++;
                }
            }

        }

        public void Unfix(int idx)
        {
            if (flag)
            {
                if (bitset[idx] == false)
                {
                    bitset[idx] = true;
                    count--;
                }
            }
            else
            {
                if (bitset[idx] == true)
                {
                    bitset[idx] = false;
                    count--;
                }
            }

        }

        public void Flip()
        {
            //懒标记
            flag = !flag;
            count = bitset.Length - count;
        }

        public bool All()
        {
            return count == bitset.Length;
        }

        public bool One()
        {
            return count >= 1;
        }

        public int Count()
        {
            return count;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (flag)
            {
                for (int i = 0; i < bitset.Length; i++)
                {
                    sb.Append(bitset[i] == true ? "0" : "1");
                }
            }
            else
            {
                for (int i = 0; i < bitset.Length; i++)
                {
                    sb.Append(bitset[i] == true ? "1" : "0");
                }
            }

            return sb.ToString();
        }
    }

    //超时
    public class Bitset1
    {
        int[] arr;
        int count = 0;
        public Bitset1(int size)
        {
            arr = new int[size];
        }

        public void Fix(int idx)
        {
            if (arr[idx] == 0)
            {
                count++;
                arr[idx] = 1;
            }
        }

        public void Unfix(int idx)
        {
            if (arr[idx] == 1)
            {
                arr[idx] = 0;
                count--;
            }
        }

        public void Flip()
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = 1 - arr[i];
            }
            count = arr.Length - count;
        }

        public bool All()
        {
            return count == arr.Length;
        }

        public bool One()
        {
            return count >= 1;
        }

        public int Count()
        {
            return count;
        }

        public override string ToString()
        {
            return string.Join("", arr);
        }
    }
}