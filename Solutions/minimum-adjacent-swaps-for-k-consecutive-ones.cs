namespace Solutions;
/// <summary>
/// 1703. 得到连续 K 个 1 的最少相邻交换次数
/// difficulty: Hard
/// https://leetcode.cn/problems/minimum-adjacent-swaps-for-k-consecutive-ones/
/// </summary>
public class MinMoves_1703_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[1,0,0,1,0,1]").ToArray(), 2, 1};
            yield return new object[]{StringTo<int[]>("[1,0,0,0,0,0,1,1]").ToArray(), 3, 5};
            yield return new object[]{StringTo<int[]>("[1,1,0,1]").ToArray(), 2, 0};
        }
    }

    [Data]
    public int MinMoves(int[] nums, int k)
    {
        IList<int> g = new List<int>();
        IList<int> preSum = new List<int>();
        preSum.Add(0);
        for (int i = 0; i < nums.Length; i++) {
            if (nums[i] == 1) {
                g.Add(i - g.Count);
                preSum.Add(preSum[preSum.Count - 1] + g[g.Count - 1]);
            }
        }
        int m = g.Count, res = int.MaxValue;
        for (int i = 0; i <= m - k; i++) {
            int mid = i + k / 2;
            int r = g[mid];
            res = Math.Min(res, (1 - k % 2) * r + (preSum[i + k] - preSum[mid + 1]) - (preSum[mid] - preSum[i]));
        }
        return res;
    }
}