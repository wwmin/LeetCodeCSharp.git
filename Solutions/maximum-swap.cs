namespace Solutions;
/// <summary>
/// 670. 最大交换
/// https://leetcode.cn/problems/maximum-swap/
/// </summary>
public class MaximumSwapSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2736, 7236 };
            yield return new object[] { 9973, 9973 };
            yield return new object[] { 1993, 9913 };
            yield return new object[] { 10909091, 90909011 };
        }
    }

    [Data]
    public int MaximumSwap(int num)
    {
        var ns = num.ToString().ToCharArray();
        //原数字数组
        var na = ns.ToArray();
        //排列后数字数组
        Array.Sort(ns);
        Array.Reverse(ns);
        int n = ns.Length;
        int left = -1;
        for (int i = 0; i < n; i++)
        {
            if (left == -1)
            {
                if (na[i] != ns[i])
                {
                    left = i;
                }
            }
            else
            {
                //从后向前找
                for (int j = n - 1; j >= 0; j--)
                {
                    if (ns[j] != na[j] && ns[left] == na[j])
                    {
                        //转换
                        var temp = na[left];
                        na[left] = na[j];
                        na[j] = temp;
                        break;
                    }
                }
                break;
            }
        }
        return Convert.ToInt32(string.Join("", na));
    }
}