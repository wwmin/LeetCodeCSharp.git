namespace Solutions;
/// <summary>
/// 303. 区域和检索 - 数组不可变
/// difficulty: Easy
/// https://leetcode.cn/problems/range-sum-query-immutable/
/// </summary>
public class NumArray
{
    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"NumArray\",\"sumRange\",\"sumRange\",\"sumRange\"]", "[[[-2,0,3,-5,2,-1]],[0,2],[2,5],[0,5]]", "[null,1,-1,-3]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }
    /// <summary>
    /// 数组前缀和
    /// </summary>
    private int[] SumAll { get; set; }
    public NumArray(int[] nums)
    {
        if (nums.Length == 0) return;
        SumAll = new int[nums.Length];
        SumAll[0] = nums[0];
        for (int i = 1; i < nums.Length; i++)
        {
            SumAll[i] = SumAll[i - 1] + nums[i];
        }
    }

    public int SumRange(int left, int right)
    {
        if (SumAll.Length == 0) return 0;
        if (left == 0)
        {
            return SumAll[right];
        }

        return SumAll[right] - SumAll[left - 1];
    }
}

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.SumRange(left,right);
 */