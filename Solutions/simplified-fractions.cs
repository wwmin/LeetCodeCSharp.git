namespace Solutions;
/// <summary>
/// 1543. 最简分数
/// </summary>
public class SimplifiedFractionsSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 2, StringTo<List<string>>("[\"1/2\"]") };
            yield return new object[] { 3, StringTo<List<string>>("[\"1/2\",\"1/3\",\"2/3\"]") };
            yield return new object[] { 4, StringTo<List<string>>("[\"1/2\",\"1/3\",\"1/4\",\"2/3\",\"3/4\"]") };
            yield return new object[] { 1, StringTo<List<string>>("[]") };
        }
    }

    [Data]
    public IList<string> SimplifiedFractions(int n)
    {
        IList<string> ans = new List<string>();
        for (int i = 2; i <= n; i++)
        {
            for (int j = 1; j < i; j++)
            {
                if (GCD(i, j) == 1)
                {
                    ans.Add($"{j}/{i}");
                }
            }
        }

        return ans;
    }
    /// <summary>
    /// 求最大公约数,如果公约数为1则这两个数互质
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private int GCD(int a, int b)
    {
        return b != 0 ? GCD(b, a % b) : a;
    }
}