using wwm.LeetCodeHelper.Servers;

namespace Solutions;
/// <summary>
/// 819. 使序列递增的最小交换次数
/// https://leetcode.cn/problems/minimum-swaps-to-make-sequences-increasing/
/// </summary>
public class MinSwapSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,3,5,4]").ToArray(), StringTo<int[]>("[1,2,3,7]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[0,3,5,8,9]").ToArray(), StringTo<int[]>("[2,1,4,6,9]").ToArray(), 1 };
        }
    }

    [Data]
    public int MinSwap(int[] nums1, int[] nums2)
    {
        //动态规划
        int n = nums1.Length;
        int a = 0, b = 1;
        for (int i = 1; i < n; i++)
        {
            int at = a, bt = b;
            a = b = n;
            if (nums1[i] > nums1[i - 1] && nums2[i] > nums2[i - 1])
            {
                a = Math.Min(a, at);
                b = Math.Min(b, bt + 1);
            }
            if (nums1[i] > nums2[i - 1] && nums2[i] > nums1[i - 1])
            {
                a = Math.Min(a, bt);
                b = Math.Min(b, at + 1);
            }
        }
        return Math.Min(a, b);
    }
}