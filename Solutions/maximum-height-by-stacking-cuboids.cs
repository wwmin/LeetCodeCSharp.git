namespace Solutions;
/// <summary>
/// 1691. 堆叠长方体的最大高度
/// difficulty: Hard
/// https://leetcode.cn/problems/maximum-height-by-stacking-cuboids/
/// </summary>
public class MaxHeight_1691_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[50,45,20],[95,37,53],[45,23,12]]").ToArray(), 190 };
            yield return new object[] { StringTo<List<int[]>>("[[38,25,45],[76,35,3]]").ToArray(), 76 };
            yield return new object[] { StringTo<List<int[]>>("[[7,11,17],[7,17,11],[11,7,17],[11,17,7],[17,7,11],[17,11,7]]").ToArray(), 102 };
        }
    }

    [Data]
    public int MaxHeight(int[][] cuboids)
    {
        int n = cuboids.Length;
        foreach (int[] v in cuboids)
        {
            Array.Sort(v);
        }
        Array.Sort(cuboids, (a, b) => (a[0] + a[1] + a[2]) - (b[0] + b[1] + b[2]));
        int ans = 0;
        int[] dp = new int[n];
        for (int i = 0; i < n; i++)
        {
            dp[i] = cuboids[i][2];
            for (int j = 0; j < i; j++)
            {
                if (cuboids[i][0] >= cuboids[j][0] &&
                    cuboids[i][1] >= cuboids[j][1] &&
                    cuboids[i][2] >= cuboids[j][2])
                {
                    dp[i] = Math.Max(dp[i], dp[j] + cuboids[i][2]);
                }
            }
            ans = Math.Max(ans, dp[i]);
        }
        return ans;
    }
}