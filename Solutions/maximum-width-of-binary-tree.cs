namespace Solutions;
/// <summary>
/// 662. 二叉树最大宽度
/// https://leetcode.cn/problems/maximum-width-of-binary-tree/
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class WidthOfBinaryTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,3,2,5,3,null,9]"), 4 };
            yield return new object[] { ToTreeNodeWithString("[1,3,2,5,null,null,9,6,null,7]"), 7 };
            yield return new object[] { ToTreeNodeWithString("[1,3,2,5]"), 2 };
        }
    }
    [Data]
    public int WidthOfBinaryTree(TreeNode root)
    {
        int res = 1;
        List<Tuple<TreeNode, int>> arr = new List<Tuple<TreeNode, int>>();
        arr.Add(new Tuple<TreeNode, int>(root, 1));
        while (arr.Count > 0)
        {
            List<Tuple<TreeNode, int>> tmp = new List<Tuple<TreeNode, int>>();
            foreach (Tuple<TreeNode, int> pair in arr)
            {
                TreeNode node = pair.Item1;
                int index = pair.Item2;
                if (node.left != null)
                {
                    tmp.Add(new Tuple<TreeNode, int>(node.left, index * 2));
                }
                if (node.right != null)
                {
                    tmp.Add(new Tuple<TreeNode, int>(node.right, index * 2 + 1));
                }
            }
            res = Math.Max(res, arr[arr.Count - 1].Item2 - arr[0].Item2 + 1);
            arr = tmp;
        }
        return res;
    }
}