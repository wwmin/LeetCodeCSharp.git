namespace Solutions;
/// <summary>
/// 780. 最多能完成排序的块
/// https://leetcode.cn/problems/max-chunks-to-make-sorted/
/// </summary>
public class MaxChunksToSortedSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[4,3,2,1,0]").ToArray(), 1 };
            yield return new object[] { StringTo<int[]>("[1,0,2,3,4]").ToArray(), 4 };
        }
    }

    [Data]
    public int MaxChunksToSorted(int[] arr)
    {
        int max = 0;
        int res = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            max = Math.Max(max, arr[i]);
            if (max == i)
            {
                res++;
            }
        }
        return res;
    }
}