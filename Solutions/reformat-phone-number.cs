namespace Solutions;
/// <summary>
/// 1812. 重新格式化电话号码
/// https://leetcode.cn/problems/reformat-phone-number/
/// </summary>
public class ReformatNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1-23-45 6", "123-456" };
            yield return new object[] { "123 4-567", "123-45-67" };
            yield return new object[] { "123 4-5678", "123-456-78" };
            yield return new object[] { "12", "12" };
            yield return new object[] { "9964-", "99-64" };
            yield return new object[] { "--17-5 229 35-39475 ", "175-229-353-94-75" };
        }
    }

    [Data]
    public string ReformatNumber(string number)
    {
        List<char> chars = new List<char>();
        for (int i = 0; i < number.Length; i++)
        {
            if (char.IsDigit(number[i])) chars.Add(number[i]);
        }
        int n = chars.Count;
        if (n == 2) return new string(chars.ToArray());
        if (n == 3) return new string(chars.ToArray());
        if (n == 4) return chars[n - 4].ToString() + chars[n - 3].ToString() + '-' + chars[n - 2].ToString() + chars[n - 1].ToString();
        var m = n % 3;
        StringBuilder res = new StringBuilder();
        if (m == 0)
        {
            return formatString(chars, n - m).ToString();
        }
        if (m == 1)
        {
            return formatString(chars, n - 4).Append('-').Append(chars[n - 4]).Append(chars[n - 3]).Append('-').Append(chars[n - 2]).Append(chars[n - 1]).ToString();
        }
        if (m == 2)
        {
            return formatString(chars, n - 2).Append('-').Append(chars[n - 2]).Append(chars[n - 1]).ToString();
        }

        return default;
    }

    private StringBuilder formatString(List<char> cs, int endIndex)
    {
        StringBuilder sb = new StringBuilder();
        int j = 0;
        for (int i = 0; i < cs.Count && i < endIndex; i++)
        {
            if (j == 3)
            {
                sb.Append('-');
                sb.Append(cs[i]);
                j = 1;
            }
            else
            {
                sb.Append(cs[i]);
                j++;
            }
        }
        return sb;
    }
}