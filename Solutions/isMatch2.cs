﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 44. 通配符匹配
/// </summary>
public class isMatch2Solution
{
    [InlineData("aa", "a", false)]
    [InlineData("aa", "*", true)]
    public bool IsMatch(string s, string p)
    {
        int m = s.Length;
        int n = p.Length;
        bool[,] dp = new bool[m + 1, n + 1];
        dp[0, 0] = true;
        for (int i = 1; i <= n; i++)
        {
            if (p[i - 1] == '*')
            {
                dp[0, i] = true;
            }
            else break;
        }
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (p[j - 1] == '*')
                {
                    dp[i, j] = dp[i, j - 1] || dp[i - 1, j];
                }
                else if (p[j - 1] == '?' || s[i - 1] == p[j - 1])
                {
                    dp[i, j] = dp[i - 1, j - 1];
                }
            }
        }
        return dp[m, n];
    }
}