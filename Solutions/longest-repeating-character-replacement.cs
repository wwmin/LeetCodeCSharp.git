namespace Solutions;
/// <summary>
/// 424. 替换后的最长重复字符
/// difficulty: Medium
/// https://leetcode.cn/problems/longest-repeating-character-replacement/
/// </summary>
public class CharacterReplacement_424_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "ABAB", 2, 4 };
            yield return new object[] { "AABABBA", 1, 4 };
        }
    }

    [Data]
    public int CharacterReplacement(string s, int k)
    {
        int ans = 0;
        int n = s.Length;
        if (n < 2) return n;
        int[] count = new int[26];
        int maxCount = 0;
        int left = 0;
        int right = 0;
        while (right < n)
        {
            maxCount = Math.Max(maxCount, ++count[s[right] - 'A']);
            right++;
            if (right - left > maxCount + k)
            {
                count[s[left] - 'A']--;
                left++;
            }
            ans = Math.Max(ans, right - left);
        }
        return ans;
    }
}