﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 65. 有效数字
/// </summary>
public class IsNumberSolution
{

    private Dictionary<char, int> numDic = new Dictionary<char, int>
        {
            { '0',0 },
            { '1',1 },
            { '2',2 },
            { '3',3 },
            { '4',4 },
            { '5',5 },
            { '6',6 },
            { '7',7 },
            { '8',8 },
            { '9',9 }
        };

    [InlineData("4e+", false)]
    [InlineData("0", true)]
    [InlineData("0089", true)]
    [InlineData(".1", true)]
    [InlineData("-0.1", true)]
    [InlineData("+3.14", true)]
    [InlineData("4.", true)]
    [InlineData("-.9", true)]
    [InlineData("2e10", true)]
    [InlineData("-90E3", true)]
    [InlineData("3e+7", true)]
    [InlineData("+6e-1", true)]
    [InlineData("53.5e93", true)]
    [InlineData("-123.456e789", true)]
    [InlineData("e", false)]
    [InlineData(".", false)]
    [InlineData("abc", false)]
    [InlineData("e3", false)]
    [InlineData("99e2.5", false)]
    [InlineData("--6", false)]
    [InlineData("-+3", false)]
    [InlineData("95a54e53", false)]
    public bool IsNumber(string s)
    {
        s = s.ToLower();
        var arr = s.Split("e");
        if (arr.Length == 1)
        {
            if (arr[0].Contains('.')) return isDecimal(s);
            else return isInterger(s);
        }
        else if (arr.Length == 2)
        {
            if (arr[0].Contains('.'))
            {
                return isDecimal(arr[0]) && isInterger(arr[1]);
            }
            else
            {
                return isInterger(arr[0]) && isInterger(arr[1]);
            }
        }
        return false;
    }
    //整数
    private bool isInterger(string s)
    {
        if (s.Length == 0) return false;
        var nchar = numDic.Keys;
        if (s[0] == '+' || s[0] == '-')
        {
            s = s.Remove(0, 1);
        }
        return s.Length > 0 && s.All(p => nchar.Contains(p));
    }
    //小数
    private bool isDecimal(string s)
    {
        if (s.Length == 0) return false;
        var nchar = numDic.Keys;
        if (s[0] == '+' || s[0] == '-')
        {
            s = s.Remove(0, 1);
        }
        if (s.Length == 0) return false;
        var ns = s.Split(".");
        if (ns.Length == 0) return false;
        if (ns.Length > 2) return false;
        if (ns.Length == 1) return s.All(p => nchar.Contains(p));
        if (ns.Length == 2)
        {
            if (ns[0].Length + ns[1].Length == 0) return false;
            return ns[0].All(p => nchar.Contains(p)) && ns[1].All(p => nchar.Contains(p));
        }
        return false;
    }
}