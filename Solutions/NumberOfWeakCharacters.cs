﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 5864. 游戏中弱角色的数量
/// </summary>
public class NumberOfWeakCharactersSolution
{
    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<List<int[]>>("[[21,56],[11,6],[38,77],[79,60],[98,90],[22,96],[63,97],[82,48],[60,39],[79,14],[37,49],[40,60],[53,62],[82,61],[46,39],[67,2],[93,20],[47,55],[98,86],[60,52],[10,44],[28,32],[52,78],[100,15],[60,48],[94,83],[5,38],[96,27],[88,95],[100,53],[95,26],[67,8],[45,8],[26,33],[3,22],[92,69],[53,57],[38,65],[36,63],[70,1],[20,69],[2,17],[70,29],[11,33],[19,14],[83,63],[96,32],[28,95],[90,45],[10,27],[25,80],[56,5],[78,67],[37,4],[4,13],[45,25],[27,15],[81,68],[7,1],[56,57],[6,25],[61,68],[57,49],[28,42],[54,48],[96,37],[85,58],[29,88],[61,76],[45,60],[27,93],[15,77],[43,31],[10,71],[20,63],[11,14],[47,37],[13,4],[3,24],[57,39],[83,9]]").ToArray(), 75 };
            //yield return new object[] { StringTo<List<int[]>>("[[7,7],[1,2],[9,7],[7,3],[3,10],[9,8],[8,10],[4,3],[1,5],[1,5]]").ToArray(), 6 };
            //yield return new object[] { StringTo<List<int[]>>("[[5,5],[6,3],[3,6]]").ToArray(), 0 };
            //yield return new object[] { StringTo<List<int[]>>("[[2,2],[3,3]]").ToArray(), 1 };
            //yield return new object[] { StringTo<List<int[]>>("[[1,5],[10,4],[4,3]]").ToArray(), 1 };
            //yield return new object[] { StringTo<List<int[]>>("[[1,1],[2,1],[2,2],[1,2]]").ToArray(), 1 };
            yield return new object[] { StringTo<List<int[]>>("[[7,9],[10,7],[6,9],[10,4],[7,5],[7,10]]").ToArray(), 2 };
        }
    }
    [Data]
    //总体思想: 两个维度都先降序处理,循环第一个维度,找到第一个维度的值比前面低的值的时候,再进行比较第二个维度, 第二个维度始终维护一个前面的最大值,如果当前第二维度值小于这个最大值,那么此就是绝对弱玩家
    public int NumberOfWeakCharacters(int[][] properties)
    {
        //子数组 0位置由大到小排序,1位置由大到小排序
        Array.Sort(properties, (a, b) =>
        {
            var c = b[0] - a[0];
            if (c != 0) return c;
            return b[1] - a[1];
        });
        int n = properties.Length;
        int ans = 0;
        int bhi = 0;
        int last = 0;
        for (int i = 0; i < n; i++)
        {
            int a = properties[i][0];
            int b = properties[i][1];
            //当子数组0位置 ,比较该数组 后面值小于前面值时
            if (i > 0 && a < properties[i - 1][0])
            {
                //保存前面数组子数组1位置,最大值
                bhi = Math.Max(bhi, properties[last][1]);
                //last保存上次的最大值的位置
                last = i;
            }
            if (b < bhi) ans++;
        }
        return ans;
    }
    //超时
    //[Data]
    public int NumberOfWeakCharacters2(int[][] properties)
    {
        int res = 0;
        Array.Sort(properties, (a, b) =>
        {
            var c = b[0] - a[0];
            if (c != 0) return c;
            return b[1] - a[1];
        });
        int n = properties.Length;
        for (int i = 0; i < n - 1; i++)
        {
            var ci = properties[i][0];
            var cj = properties[i][1];
            if (ci == 0)
            {
                i++;
                continue;
            }
            int right = i + 1;
            while (right < n)
            {
                if (right - 1 > i && properties[right][0] == ci) { right++; continue; }
                if (properties[right][1] > 0 && properties[right][0] < ci && properties[right][1] < cj)
                {
                    res++;
                    properties[right][1] = 0;
                }
                right++;
            }
        }
        return res;
    }
}
