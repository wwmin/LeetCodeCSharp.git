namespace Solutions;
/// <summary>
/// 800. 字母大小写全排列
/// https://leetcode.cn/problems/letter-case-permutation/
/// </summary>
public class LetterCasePermutationSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "a1b2", StringTo<List<string>>("[\"a1b2\",\"a1B2\",\"A1b2\",\"A1B2\"]") };
            yield return new object[] { "3z4", StringTo<List<string>>("[\"3z4\",\"3Z4\"]") };
        }
    }

    [Data]
    public IList<string> LetterCasePermutation(string s)
    {
        return default;
    }
}