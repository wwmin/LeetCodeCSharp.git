namespace Solutions;
/// <summary>
/// 1775. 通过最少操作次数使数组的和相等
/// difficulty: Medium
/// https://leetcode.cn/problems/equal-sum-arrays-with-minimum-number-of-operations/
/// </summary>
public class MinOperations_1775_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[1,2,3,4,5,6]").ToArray(), StringTo<int[]>("[1,1,2,2,2,2]").ToArray(), 3};
            yield return new object[]{StringTo<int[]>("[1,1,1,1,1,1,1]").ToArray(), StringTo<int[]>("[6]").ToArray(), -1};
            yield return new object[]{StringTo<int[]>("[6,6]").ToArray(), StringTo<int[]>("[1]").ToArray(), 3};
        }
    }

    [Data]
    public int MinOperations(int[] nums1, int[] nums2)
    {
        if(nums1.Length * 6 < nums2.Length || nums2.Length * 6 < nums1.Length)
            return -1;
         int[] cnt1 = new int[7];
        int[] cnt2 = new int[7];
        int diff = 0;
        foreach (int i in nums1) {
            ++cnt1[i];
            diff += i;
        }
        foreach (int i in nums2) {
            ++cnt2[i];
            diff -= i;
        }
        if (diff == 0) {
            return 0;
        }
        if (diff > 0) {
            return Help(cnt2, cnt1, diff);
        }
        return Help(cnt1, cnt2, -diff);
    }
    private int Help(int[] h1, int[] h2, int diff) {
        int[] h = new int[7];
        for (int i = 1; i < 7; ++i) {
            h[6 - i] += h1[i];
            h[i - 1] += h2[i];
        }
        int res = 0;
        for (int i = 5; i > 0 && diff > 0; --i) {
            int t = Math.Min((diff + i - 1) / i, h[i]);
            res += t;
            diff -= t * i;
        }
        return res;
    }
}