namespace Solutions;
/// <summary>
/// 131. 分割回文串
/// difficulty: Medium
/// https://leetcode.cn/problems/palindrome-partitioning/
/// </summary>
public class Partition_131_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "aab", StringTo<List<List<string>>>("[[\"a\",\"a\",\"b\"],[\"aa\",\"b\"]]").ToArray() };
            yield return new object[] { "a", StringTo<List<List<string>>>("[[\"a\"]]").ToArray() };
        }
    }
    IList<IList<string>> res = new List<IList<string>>();
    [Data]
    public IList<IList<string>> Partition(string s)
    {
        res.Clear();
        dfs(s, 0, new List<string>());
        return res;
    }

    void dfs(string s, int start, List<string> path)
    {
        if (start == s.Length)
        {
            res.Add(new List<string>(path));
            return;
        }
        for (int i = start; i < s.Length; i++)
        {
            if (IsPalindrome(s[start..(i + 1)]))
            {
                path.Add(s[start..(i + 1)]);
                dfs(s, i + 1, path);
                path.RemoveAt(path.Count - 1);
            }
        }
    }

    bool IsPalindrome(string s)
    {
        int i = 0;
        int j = s.Length - 1;
        while (i < j)
        {
            if (s[i] != s[j]) return false;
            i++;
            j--;
        }
        return true;
    }
}