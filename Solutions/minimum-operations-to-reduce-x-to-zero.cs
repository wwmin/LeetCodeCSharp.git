namespace Solutions;
/// <summary>
/// 1658. 将 x 减到 0 的最小操作数
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-operations-to-reduce-x-to-zero/
/// </summary>
public class MinOperations_1658_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,1,4,2,3]").ToArray(), 5, 2 };
            yield return new object[] { StringTo<int[]>("[5,6,7,8,9]").ToArray(), 4, -1 };
            yield return new object[] { StringTo<int[]>("[3,2,20,1,1,3]").ToArray(), 10, 5 };
        }
    }

    [Data]
    public int MinOperations(int[] nums, int x)
    {
        //转换问题:求解最长子数组和为sum(nums)-x
        //双指针
        int target = nums.Sum() - x;
        int sum = 0;
        int left = 0;
        int right = 0;
        int max = -1;
        while (right < nums.Length)
        {
            sum += nums[right];
            while (sum > target && left <= right)
            {
                sum -= nums[left];
                left++;
            }
            if (sum == target)
            {
                max = Math.Max(max, right - left + 1);
            }
            right++;
        }
        if (max == -1)
        {
            return -1;
        }
        return nums.Length - max;
    }
}