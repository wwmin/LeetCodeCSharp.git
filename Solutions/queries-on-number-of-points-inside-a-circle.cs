namespace Solutions;
/// <summary>
/// 1828. 统计一个圆中点的数目
/// difficulty: Medium
/// https://leetcode.cn/problems/queries-on-number-of-points-inside-a-circle/
/// </summary>
public class CountPoints_1828_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<List<int[]>>("[[1,3],[3,3],[5,3],[2,2]]").ToArray(), StringTo<List<int[]>>("[[2,3,1],[4,3,1],[1,1,2]]").ToArray(), StringTo<int[]>("[3,2,2]").ToArray()};
            yield return new object[]{StringTo<List<int[]>>("[[1,1],[2,2],[3,3],[4,4],[5,5]]").ToArray(), StringTo<List<int[]>>("[[1,2,2],[2,2,2],[4,3,2],[4,3,3]]").ToArray(), StringTo<int[]>("[2,3,2,4]").ToArray()};
        }
    }

    [Data]
    public int[] CountPoints(int[][] points, int[][] queries)
    {
        int[] result = new int[queries.Length];
        for (int i = 0; i < queries.Length; i++)
        {
            int[] query = queries[i];
            int count = 0;
            for (int j = 0; j < points.Length; j++)
            {
                int[] point = points[j];
                if (Math.Pow(point[0] - query[0], 2) + Math.Pow(point[1] - query[1], 2) <= Math.Pow(query[2], 2))
                {
                    count++;
                }
            }
            result[i] = count;
        }
        return result;
    }
}