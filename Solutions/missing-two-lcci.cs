namespace Solutions;
/// <summary>
/// 1000044. 消失的两个数字
/// https://leetcode.cn/problems/missing-two-lcci/solution/zhua-wa-mou-si-by-muse-77-pqe5/
/// </summary>
public class MissingTwoSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1]").ToArray(), StringTo<int[]>("[2,3]").ToArray() };
            yield return new object[] { StringTo<int[]>("[2,3]").ToArray(), StringTo<int[]>("[1,4]").ToArray() };
        }
    }

    [Data]
    public int[] MissingTwo(int[] nums)
    {
        int totalLength = nums.Length + 2;
        int totalSum = ((totalLength + 1) * totalLength) >> 1;
        foreach (var num in nums)
        {
            totalSum -= num;
        }
        int missingHalf = totalSum >> 1;
        int missingHalfSum = ((missingHalf + 1) * missingHalf) >> 1;
        foreach (var num in nums)
        {
            if (num <= missingHalf)
                missingHalfSum -= num;
        }

        return new int[] { missingHalfSum, totalSum - missingHalfSum };
    }
}