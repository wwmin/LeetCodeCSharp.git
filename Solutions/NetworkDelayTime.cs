﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 743. 网络延迟时间
/// </summary>
public class NetworkDelayTimeSolution
{
    public class NetworkDelayTimeDataAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { new int[][] { new int[] { 2, 1, 1 }, new int[] { 2, 3, 1 }, new int[] { 3, 4, 1 } }, 4, 2, 2 };
            yield return new object[] { new int[][] { new int[] { 1, 2, 1 } }, 2, 1, 1 };
        }
    }

    [NetworkDelayTimeData]
    public int NetworkDelayTime(int[][] times, int n, int k)
    {
        const int INF = int.MaxValue / 2;
        int[,] g = new int[n, n];
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                g[i, j] = INF;
            }
        }
        foreach (int[] t in times)
        {
            int x = t[0] - 1, y = t[1] - 1;
            g[x, y] = t[2];
        }
        int[] dist = new int[n];
        Array.Fill(dist, INF);
        dist[k - 1] = 0;
        bool[] used = new bool[n];
        for (int i = 0; i < n; ++i)
        {
            int x = -1;
            for (int y = 0; y < n; ++y)
            {
                if (!used[y] && (x == -1 || dist[y] < dist[x]))
                {
                    x = y;
                }
            }
            used[x] = true;
            for (int y = 0; y < n; ++y)
            {
                dist[y] = Math.Min(dist[y], dist[x] + g[x, y]);
            }
        }
        int ans = dist.Max();
        return ans == INF ? -1 : ans;
    }
}
