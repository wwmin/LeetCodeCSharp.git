﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    public class FindFinalValueSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[5,3,6,1,12]").ToArray(), 3, 24 };
                yield return new object[] { StringTo<int[]>("[2,7,9]").ToArray(), 4, 4 };
            }
        }
        [Data]
        public int FindFinalValue(int[] nums, int original)
        {
            Array.Sort(nums);
            int n = nums.Length;
            int max = nums[n - 1];
            while (original <= max)
            {
                bool value = findValue(nums, original);
                if (value == false) return original;
                original = original * 2;
            }
            return original;
        }

        private bool findValue(int[] nums, int orignal)
        {
            int left = 0;
            int right = nums.Length - 1;
            while (left < right)
            {
                int mid = (left + right) / 2;
                if (nums[mid] == orignal) return true;
                if (nums[mid] < orignal) left = mid + 1;
                else right = mid;
            }
            return nums[left] == orignal;
        }
    }
}