namespace Solutions;
/// <summary>
/// 557. 反转字符串中的单词 III
/// https://leetcode.cn/problems/reverse-words-in-a-string-iii/solution/
/// </summary>
public class ReverseWordsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "Let's take LeetCode contest", "s'teL ekat edoCteeL tsetnoc" };
            yield return new object[] { "God Ding", "doG gniD" };
        }
    }

    [Data]
    public string ReverseWords(string s)
    {
        var ss = s.ToCharArray();
        int i, j;
        i = j = 0;
        for (int k = 0; k < ss.Length; k++)
        {
            if (ss[k] == ' ' || k + 1 == ss.Length)
            {
                if (ss[k] != ' ') j++;
                //反转单词
                while (j > i)
                {
                    var tem = ss[i];
                    ss[i] = ss[j];
                    ss[j] = tem;
                    i++;
                    j--;
                }
                //i从下一个单词开始
                i = k + 1;
            }
            else
            {
                j = k;
            }
        }
        return new string(ss);
    }
}