namespace Solutions;
/// <summary>
/// 2287. 重排字符形成目标字符串
/// difficulty: Easy
/// https://leetcode.cn/problems/rearrange-characters-to-make-target-string/
/// </summary>
public class RearrangeCharacters_2287_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcefgf", "abcd", 0 };
            yield return new object[] { "abc", "abcd", 0 };
            yield return new object[] { "ilovecodingonleetcode", "code", 2 };
            yield return new object[] { "abcba", "abc", 1 };
            yield return new object[] { "aabbcc", "abc", 2 };
            yield return new object[] { "abbaccaddaeea", "aaaaa", 1 };
            yield return new object[] { "lrnvlcqukanpdnluowenfxquitzryponxsikhciohyostvmkapkfpglzikitwiraqgchxnpryhwpuwpozacjhmwhjvslprqlnxrk", "woijih", 2 };
        }
    }

    [Data]
    public int RearrangeCharacters(string s, string target)
    {
        int count = int.MaxValue;
        int[] targetArr = new int[26];
        foreach (var c in target) targetArr[c - 'a']++;
        int[] sArr = new int[26];
        foreach (var c in s) sArr[c - 'a']++;
        for (int i = 0; i < targetArr.Length; i++)
        {
            if (targetArr[i] == 0) continue;
            if (sArr[i] == 0) return 0;
            count = Math.Min(count, sArr[i] / targetArr[i]);
        }
        return count;
    }
}