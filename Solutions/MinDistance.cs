﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 72. 编辑距离
/// </summary>
public class MinDistanceSolution
{
    [InlineData("horse", "ros", 3)]
    [InlineData("intention", "execution", 5)]
    public int MinDistance(string word1, string word2)
    {
        //动态规划 状态转移方程 dp[i][j] = min(dp[i-1][j-1],dp[i-1][j],dp[i][j-1])+1
        //dp[i-1][j-1]表示替换操作 dp[i-1][j]表示删除操作 dp[i][j-1]表示插入操作
        //第一行 第一列 单独考虑, 设为空字符串
        int m = word1.Length;
        int n = word2.Length;
        int[,] dp = new int[m + 1, n + 1];
        dp[0, 0] = 0;
        //边界条件
        //第一行
        for (int j = 1; j <= n; j++)
        {
            dp[0, j] = dp[0, j - 1] + 1;
        }
        //第一列
        for (int i = 1; i <= m; i++)
        {
            dp[i, 0] = dp[i - 1, 0] + 1;
        }
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                //如果当前字符相同,则当前步骤等于上一个步骤
                if (word1[i - 1] == word2[j - 1]) dp[i, j] = dp[i - 1, j - 1];
                //否则使用上一个或左或右中最小的那个步骤然后加1
                else dp[i, j] = Math.Min(Math.Min(dp[i - 1, j], dp[i, j - 1]), dp[i - 1, j - 1]) + 1;
            }
        }
        return dp[m, n];
    }
}
