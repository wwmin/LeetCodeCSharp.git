namespace Solutions;
/// <summary>
/// 100186. 零矩阵
/// https://leetcode.cn/problems/zero-matrix-lcci/
/// </summary>
public class SetZeroesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,1,1],[1,0,1],[1,1,1]]").ToArray(), StringTo<List<int[]>>("[[1,0,1],[0,0,0],[1,0,1]]").ToArray() };
            yield return new object[] { StringTo<List<int[]>>("[[0,1,2,0],[3,4,5,2],[1,3,1,5]]").ToArray(), StringTo<List<int[]>>("[[0,0,0,0],[0,4,5,0],[0,3,1,0]]").ToArray() };
        }
    }

    [Data]
    public void SetZeroes(int[][] matrix)
    {
        Dictionary<int, bool> i_dic = new Dictionary<int, bool>();
        Dictionary<int, bool> j_dic = new Dictionary<int, bool>();
        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[i].Length; j++)
            {
                if (matrix[i][j] == 0)
                {
                    if (!i_dic.ContainsKey(i)) i_dic.Add(i, true);
                    if (!j_dic.ContainsKey(j)) j_dic.Add(j, true);
                }
            }
        }
        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[i].Length; j++)
            {
                if (i_dic.ContainsKey(i) || j_dic.ContainsKey(j)) matrix[i][j] = 0;
            }
        }
    }
}