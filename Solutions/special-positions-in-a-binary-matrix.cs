namespace Solutions;
/// <summary>
/// 1704. 二进制矩阵中的特殊位置
/// https://leetcode.cn/problems/special-positions-in-a-binary-matrix/
/// </summary>
public class NumSpecialSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<List<int[]>>("[[1,0,0], [0,0,1], [1,0,0]]").ToArray(), 1};
            yield return new object[]{StringTo<List<int[]>>("[[1,0,0], [0,1,0], [0,0,1]]").ToArray(), 3};
            yield return new object[]{StringTo<List<int[]>>("[[0,0,0,1], [1,0,0,0], [0,1,1,0], [0,0,0,0]]").ToArray(), 2};
            yield return new object[]{StringTo<List<int[]>>("[[0,0,0,0,0], [1,0,0,0,0], [0,1,0,0,0], [0,0,1,0,0], [0,0,0,1,1]]").ToArray(), 3};
        }
    }

    [Data]
    public int NumSpecial(int[][] mat)
    {
        return default;
    }
}