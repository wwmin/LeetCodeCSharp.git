namespace Solutions;
/// <summary>
/// 520. 检测大写字母
/// </summary>
public class DetectCapitalUseSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "USA", true };
            yield return new object[] { "FlaG", false };
            yield return new object[] { "Flag", true };
            yield return new object[] { "USa", false };
        }
    }
    [Data]
    public bool DetectCapitalUse(string word)
    {
        int n = word.Length;
        if (n == 1) return true;
        var firstCode = word[0];
        bool isFirstLower = false;
        if (firstCode >= 'a' && firstCode <= 'z')
        {
            isFirstLower = true;
        }
        if (isFirstLower)
        {
            for (int i = 1; i < n; i++)
            {
                if (word[i] >= 'a' && word[i] <= 'z') continue;
                else return false;
            }
        }
        else
        {
            bool hasLower = false;
            bool hasUpper = false;
            for (int i = 1; i < n; i++)
            {
                if (word[i] >= 'A' && word[i] <= 'Z')
                {
                    hasUpper = true;
                    if (hasLower) return false;
                    else continue;
                }
                else
                {
                    if (hasUpper) return false;
                    hasLower = true;
                };
            }
        }
        return true;
    }
}