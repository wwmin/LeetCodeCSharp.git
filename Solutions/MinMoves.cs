﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5194. 得到目标值的最少行动次数
    /// </summary>
    public class MinMovesSolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { 5, 0, 4 };
                yield return new object[] { 19, 2, 7 };
                yield return new object[] { 10, 4, 4 };
            }
        }

        [Data]
        public int MinMoves(int target, int maxDoubles)
        {
            //倒序模拟
            int ans = 0;

            while (target > 1)
            {
                if (maxDoubles > 0)
                {
                    if (target % 2 == 1)
                    {
                        ans++;
                        target--;
                    }
                    target = target / 2;
                    if (target < 1)
                    {
                        ans += target - 1;
                        break;
                    }
                    else
                    {
                        ans++;
                        maxDoubles--;
                    }
                }
                else
                {
                    ans += target - 1;
                    break;
                }

            }
            return ans;
        }
    }
}