﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 76. 最小覆盖子串
/// </summary>
public class MinWindowSolution
{
    Dictionary<char, int> cnt = new Dictionary<char, int>();
    Dictionary<char, int> ori = new Dictionary<char, int>();
    [InlineData("ADOBECODEBANC", "ABC", "BANC")]
    [InlineData("a", "a", "a")]
    //[InlineData("a", "aa", "")]
    public string MinWindow(string s, string t)
    {
        int tLen = t.Length;
        cnt = new Dictionary<char, int>();
        ori = new Dictionary<char, int>();
        for (int j = 0; j < tLen; j++)
        {
            if (ori.ContainsKey(t[j])) ori[t[j]]++;
            else ori.Add(t[j], 0);
        }

        int l = 0, r = -1;
        int len = int.MaxValue, ansL = -1, ansR = -1;
        int sLen = s.Length;
        while (r < sLen)
        {
            r++;
            if (r < sLen && ori.ContainsKey(s[r]))
            {
                if (cnt.ContainsKey(s[r]))
                {
                    cnt[s[r]]++;
                }
                else
                {
                    cnt.Add(s[r], 0);
                }
            }
            while (aContainB() && l <= r)
            {
                if (r - l + 1 < len)
                {
                    len = r - l + 1;
                    ansL = l;
                    ansR = l + len;
                }
                if (ori.ContainsKey(s[l]))
                {
                    if (cnt.ContainsKey(s[l]))
                    {
                        cnt[s[l]]--;
                    }
                    else
                    {
                        cnt.Add(s[l], 0);
                    }
                }
                l++;
            }
        }
        return ansL == -1 ? "" : s[ansL..ansR];
    }

    private bool aContainB()
    {
        var iter = ori.GetEnumerator();
        while (iter.MoveNext())
        {
            var entry = iter.Current;
            var key = entry.Key;
            var val = entry.Value;
            if (!cnt.ContainsKey(key) || cnt[key] < val)
            {
                return false;
            }
        }
        return true;
    }
}
