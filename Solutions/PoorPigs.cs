namespace Solutions;
/// <summary>
/// 458. 可怜的小猪
/// </summary>
public class PoorPigsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1000, 15, 60, 5 };
            yield return new object[] { 4, 15, 15, 2 };
            yield return new object[] { 4, 15, 30, 2 };
        }
    }// 不会做
    [Data]
    public int PoorPigs(int buckets, int minutesToDie, int minutesToTest)
    {
        if (buckets == 1)
        {
            return 0;
        }
        int[,] combinations = new int[buckets + 1, buckets + 1];
        combinations[0, 0] = 1;
        int iterations = minutesToTest / minutesToDie;
        int[,] f = new int[buckets, iterations + 1];
        for (int i = 0; i < buckets; i++)
        {
            f[i, 0] = 1;
        }
        for (int j = 0; j <= iterations; j++)
        {
            f[0, j] = 1;
        }
        for (int i = 1; i < buckets; i++)
        {
            combinations[i, 0] = 1;
            combinations[i, i] = 1;
            for (int j = 1; j < i; j++)
            {
                combinations[i, j] = combinations[i - 1, j - 1] + combinations[i - 1, j];
            }
            for (int j = 1; j <= iterations; j++)
            {
                for (int k = 0; k <= i; k++)
                {
                    f[i, j] += f[k, j - 1] * combinations[i, i - k];
                }
            }
            if (f[i, iterations] >= buckets)
            {
                return i;
            }
        }
        return 0;
    }
}