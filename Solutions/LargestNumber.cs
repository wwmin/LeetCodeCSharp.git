namespace Solutions;
/// <summary>
/// 179. 最大数
/// </summary>
public class LargestNumberSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[10,2]").ToArray(), "210" };
            yield return new object[] { StringTo<int[]>("[3,30,34,5,9]").ToArray(), "9534330" };
            yield return new object[] { StringTo<int[]>("[1]").ToArray(), "1" };
            yield return new object[] { StringTo<int[]>("[10]").ToArray(), "10" };
        }
    }

    [Data]
    public string LargestNumber(int[] nums)
    {
        string[] ss = new string[nums.Length];
        int i = 0;
        foreach (var num in nums)
        {
            ss[i++] = num.ToString();
        }
        Array.Sort(ss, (a, b) =>
        {
            string sa = a + b;
            string sb = b + a;
            //字符串比较
            return sb.CompareTo(sa);
        });
        StringBuilder sb = new StringBuilder();
        foreach (var s in ss)
        {
            sb.Append(s);
        }
        //去掉所有前导0的字符
        int len = sb.Length;
        int k = 0;
        while (k < len - 1 && sb[k] == '0')
        {
            k++;
        }
        return sb.ToString(k, len - k);
    }
}