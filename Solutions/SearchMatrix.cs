﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 74. 搜索二维矩阵
/// </summary>
public class SearchMatrixSolution
{
    class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,3,5,7],[10,11,16,20],[23,30,34,60]]").ToArray(), 3, true };
            yield return new object[] { StringTo<List<int[]>>("[[1,3,5,7],[10,11,16,20],[23,30,34,60]]").ToArray(), 13, false };
        }
    }

    [Data]
    public bool SearchMatrix(int[][] matrix, int target)
    {
        //先列再行二分查找
        int m = matrix.Length;
        //int n = matrix[0].Length;
        int[] firstCol = new int[m];
        for (int i = 0; i < m; i++)
        {
            firstCol[i] = matrix[i][0];
        }
        (int colRes, int left) = binarySerarch(firstCol, target);
        if (colRes >= 0) return true;
        (int res, _) = binarySerarch(matrix[left], target);
        return res > -1;
    }

    private (int res, int left) binarySerarch(int[] arr, int target)
    {
        int left = 0;
        int right = arr.Length - 1;
        while (left <= right)
        {
            int mid = (left + right) / 2;
            if (arr[mid] == target) return (mid, left);
            if (arr[mid] > target) right = mid - 1;
            else left = mid + 1;
        }
        return (-1, left > 0 ? left - 1 : 0);
    }
}
