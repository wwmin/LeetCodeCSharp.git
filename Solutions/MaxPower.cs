namespace Solutions;
/// <summary>
/// 1542. 连续字符
/// </summary>
public class MaxPowerSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "leetcode", 2 };
            yield return new object[] { "eedeee", 3 };
            yield return new object[] { "abbcccddddeeeeedcba", 5 };
            yield return new object[] { "triplepillooooow", 5 };
            yield return new object[] { "hooraaaaaaaaaaay", 11 };
            yield return new object[] { "tourist", 1 };
        }
    }
    [Data]
    public int MaxPower(string s)
    {
        int ans = 0;
        char lastChar = default;
        int curr_len = 0;
        for (int i = 0; i < s.Length; i++)
        {

            if (s[i] != lastChar)
            {
                lastChar = s[i];
                ans = Math.Max(ans, curr_len);
                curr_len = 0;
                //剪枝之后运行更慢了, 😓
                //if (s.Length - i <= ans) break;
            }
            curr_len++;
        }
        ans = Math.Max(ans, curr_len);
        return ans;
    }
}