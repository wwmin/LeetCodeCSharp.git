namespace Solutions;
/// <summary>
/// 1805. 字符串中不同整数的数目
/// difficulty: Easy
/// https://leetcode.cn/problems/number-of-different-integers-in-a-string/
/// </summary>
public class NumDifferentIntegers_1805_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "a123bc34d8ef34", 3 };
            yield return new object[] { "leet1234code234", 2 };
            yield return new object[] { "a1b01c001", 1 };
            yield return new object[] { "0000000aa0000a000", 1 };
        }
    }

    [Data]
    public int NumDifferentIntegers(string word)
    {
        var set = new HashSet<string>();
        for (int i = 0; i < word.Length; i++)
        {
            if (char.IsDigit(word[i]))
            {
                int j = i + 1;
                while (j < word.Length && char.IsDigit(word[j])) j++;
                var numStr = removePreZero(word[i..j]);
                set.Add(numStr);
                i = j - 1;
            }
            else
            {
                continue;
            }
        }
        return set.Count;
    }

    string removePreZero(string s)
    {
        int i = 0;
        while (i < s.Length && s[i] == '0') i++;
        return s[i..];
    }
}