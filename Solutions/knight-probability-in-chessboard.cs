namespace Solutions;
/// <summary>
/// 688. 骑士在棋盘上的概率
/// </summary>
public class KnightProbabilitySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{3, 2, 0, 0, 0.0625};
            yield return new object[]{1, 0, 0, 0, 1.00000};
        }
    }

    static int[][] dirs = { new int[] { -2, -1 }, new int[] { -2, 1 }, new int[] { 2, -1 }, new int[] { 2, 1 }, new int[] { -1, -2 }, new int[] { -1, 2 }, new int[] { 1, -2 }, new int[] { 1, 2 } };

    [Data]
    public double KnightProbability(int n, int k, int row, int column)
    {
        double[,,] dp = new double[k + 1, n, n];
        for (int step = 0; step <= k; step++)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (step == 0)
                    {
                        dp[step, i, j] = 1;
                    }
                    else
                    {
                        foreach (int[] dir in dirs)
                        {
                            int ni = i + dir[0], nj = j + dir[1];
                            if (ni >= 0 && ni < n && nj >= 0 && nj < n)
                            {
                                dp[step, i, j] += dp[step - 1, ni, nj] / 8;
                            }
                        }
                    }
                }
            }
        }
        return dp[k, row, column];
    }
}