namespace Solutions;
/// <summary>
/// 2042. 检查句子中的数字是否递增
/// difficulty: Easy
/// https://leetcode.cn/problems/check-if-numbers-are-ascending-in-a-sentence/
/// </summary>
public class AreNumbersAscending_2042_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1 box has 3 blue 4 red 6 green and 12 yellow marbles", true };
            yield return new object[] { "hello world 5 x 5", false };
            yield return new object[] { "sunset is at 7 51 pm overnight lows will be in the low 50 and 60 s", false };
            yield return new object[] { "4 5 11 26", true };
        }
    }

    [Data]
    public bool AreNumbersAscending(string s)
    {
        var arr = s.Split(' ');
        var last = 0;
        foreach (var item in arr)
        {
            if (int.TryParse(item, out var num))
            {
                if (num <= last) return false;
                last = num;
            }
        }
        return true;
    }
}