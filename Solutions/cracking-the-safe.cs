namespace Solutions;
/// <summary>
/// 753. 破解保险箱
/// difficulty: Hard
/// https://leetcode.cn/problems/cracking-the-safe/
/// </summary>
public class CrackSafe_753_Solution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{1, 2, "01"};
            yield return new object[]{2, 2, "00110"};
        }
    }

 
    ISet<int> seen = new HashSet<int>();
    StringBuilder ans = new StringBuilder();
    int highest;
    int k;

   [Data]
    public string CrackSafe(int n, int k) {
        ans.Clear();
        seen.Clear();
        highest = (int) Math.Pow(10, n - 1);
        this.k = k;
        DFS(0);
        for (int i = 1; i < n; i++) {
            ans.Append('0');
        }
        return ans.ToString();
    }

    public void DFS(int node) {
        for (int x = 0; x < k; ++x) {
            int nei = node * 10 + x;
            if (!seen.Contains(nei)) {
                seen.Add(nei);
                DFS(nei % highest);
                ans.Append(x);
            }
        }
    }
}