namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 1547. 旅行终点站
/// </summary>
public class DestCitySolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<List<string>>>("[[\"London\",\"New York\"],[\"New York\",\"Lima\"],[\"Lima\",\"Sao Paulo\"]]").ToArray(), "Sao Paulo" };
            yield return new object[] { StringTo<List<List<string>>>("[[\"B\",\"C\"],[\"D\",\"B\"],[\"C\",\"A\"]]").ToArray(), "A" };
            yield return new object[] { StringTo<List<List<string>>>("[[\"A\",\"Z\"]]").ToArray(), "Z" };
        }
    }
    [Data]
    public string DestCity(IList<IList<string>> paths)
    {
        ISet<string> cityA = new HashSet<string>();
        foreach (IList<string> path in paths)
        {
            cityA.Add(path[0]);
        }
        foreach (IList<string> path in paths)
        {
            if (!cityA.Contains(path[1])) return path[1];
        }
        return "";
    }
}
