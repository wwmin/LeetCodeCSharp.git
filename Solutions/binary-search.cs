namespace Solutions;
/// <summary>
/// 792. 二分查找
/// https://leetcode.cn/problems/binary-search/
/// </summary>
public class SearchSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,5]").ToArray(), 5, 1 };
            yield return new object[]{StringTo<int[]>("[-1,0,3,5,9,12]").ToArray(), 9, 4};
            yield return new object[]{StringTo<int[]>("[-1,0,3,5,9,12]").ToArray(), 2, -1};
        }
    }

    [Data]
    public int Search(int[] nums, int target)
    {
        //if (nums.Length == 1)
        //{
        //    if (nums[0] == target) return 0;
        //    return -1;
        //}
        int left = 0;
        int right = nums.Length-1;
        while (left<=right)
        {
            int mid = left+(right-left)/2;
            if (nums[mid] == target) return mid;
            if (nums[mid] > target) right = mid-1;
            else left = mid+1;
        }
        return -1 ;
    }
}