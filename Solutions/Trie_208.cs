namespace Solutions;
/// <summary>
/// 208. 实现 Trie (前缀树)
/// </summary>
public class Trie
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"Trie\",\"insert\",\"search\",\"search\",\"startsWith\",\"insert\",\"search\"]", "[[],[\"apple\"],[\"apple\"],[\"app\"],[\"app\"],[\"app\"],[\"app\"]]", "[null,null,true,false,true,null,true]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();

    }

    private Trie[] children;
    private bool isEnd;
    public Trie()
    {
        children = new Trie[26];
        isEnd = false;
    }

    public void Insert(string word)
    {
        Trie node = this;
        for (int i = 0; i < word.Length; i++)
        {
            char ch = word[i];
            int index = ch - 'a';
            if (node.children[index] == null)
            {
                node.children[index] = new Trie();
            }
            node = node.children[index];
        }
        node.isEnd = true;
    }

    public bool Search(string word)
    {
        Trie node = searchPrefix(word);
        return node != null && node.isEnd;
    }

    public bool StartsWith(string prefix)
    {
        return searchPrefix(prefix) != null;
    }

    private Trie searchPrefix(string prefix)
    {
        Trie node = this;
        for (int i = 0; i < prefix.Length; i++)
        {
            char ch = prefix[i];
            int index = ch - 'a';
            if (node.children[index] == null)
            {
                return null;
            }
            node = node.children[index];
        }
        return node;
    }
}

/**
 * Your Trie object will be instantiated and called as such:
 * Trie obj = new Trie();
 * obj.Insert(word);
 * bool param_2 = obj.Search(word);
 * bool param_3 = obj.StartsWith(prefix);
 */