namespace Solutions;
/// <summary>
/// 1570. 商品折扣后的最终价格
/// https://leetcode.cn/problems/final-prices-with-a-special-discount-in-a-shop/
/// </summary>
public class FinalPricesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[8,4,6,2,3]").ToArray(), StringTo<int[]>("[4,2,4,2,3]").ToArray() };
            yield return new object[] { StringTo<int[]>("[1,2,3,4,5]").ToArray(), StringTo<int[]>("[1,2,3,4,5]").ToArray() };
            yield return new object[] { StringTo<int[]>("[10,1,1,6]").ToArray(), StringTo<int[]>("[9,0,1,6]").ToArray() };
        }
    }

    [Data]
    public int[] FinalPrices(int[] prices)
    {
        int n = prices.Length;
        int[] ans = new int[n];
        //单调栈
        Stack<int> stack = new Stack<int>();
        for (int i = n - 1; i >= 0; i--)
        {
            //从后向前入栈，如果当前栈顶层大于当前值，则继续出栈，直到栈为空或找到栈顶值小于当前值
            while (stack.Count > 0 && stack.Peek() > prices[i])
            {
                stack.Pop();
            }
            //栈已空，说明当前值后面没有比此时小的。如果栈非空，说明栈顶值就是比当前值小的第一个值
            ans[i] = stack.Count == 0 ? prices[i] : prices[i] - stack.Peek();
            //将当前值加入栈中，继续与前面的值比较
            stack.Push(prices[i]);
        }
        return ans;
    }

    public int[] FinalPrices1(int[] prices)
    {
        int n = prices.Length;
        int[] ans = new int[n];
        //模拟
        for (int i = 0; i < n; i++)
        {
            ans[i] = prices[i];
            for (int j = i + 1; j < n; j++)
            {
                if (ans[i] >= prices[j])
                {
                    ans[i] = ans[i] - prices[j];
                    break;
                }
            }
        }
        return ans;
    }
}