namespace Solutions;
/// <summary>
/// 1785. 构成特定和需要添加的最少元素
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-elements-to-add-to-form-a-given-sum/
/// </summary>
public class MinElements_1785_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,-1,1]").ToArray(), 3, -4, 2 };
            yield return new object[] { StringTo<int[]>("[1,-10,9,1]").ToArray(), 100, 0, 1 };
        }
    }

    [Data]
    public int MinElements(int[] nums, int limit, int goal)
    {
        long sum = 0;
        foreach (int x in nums)
        {
            sum += x;
        }
        long diff = Math.Abs(sum - goal);
        return (int)((diff + limit - 1) / limit);
    }
}