namespace Solutions;
/// <summary>
/// 992. K 个不同整数的子数组
/// difficulty: Hard
/// https://leetcode.cn/problems/subarrays-with-k-different-integers/
/// </summary>
public class SubarraysWithKDistinct_992_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,1,2,3]").ToArray(), 2, 7 };
            yield return new object[] { StringTo<int[]>("[1,2,1,3,4]").ToArray(), 3, 3 };
        }
    }

    [Data]
    public int SubarraysWithKDistinct(int[] nums, int k)
    {
        int ans = calcKDistinct(nums, k) - calcKDistinct(nums, k - 1);
        return ans;
    }

    public int calcKDistinct(int[] nums, int k)
    {
        var map = new Dictionary<int, int>();
        int ans = 0;
        int left = 0;
        int right = 0;
        int count = 0;
        while (right < nums.Length)
        {
            if (!map.ContainsKey(nums[right]))
            {
                count++;
                map.Add(nums[right], 1);
            }
            else
            {
                map[nums[right]]++;
            }
            right++;
            while (count > k)
            {
                map[nums[left]]--;
                if (map[nums[left]] == 0)
                {
                    count--;
                    map.Remove(nums[left]);
                }
                left++;
            }
            ans += right - left;
        }
        return ans;
    }
}