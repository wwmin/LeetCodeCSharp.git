namespace Solutions;
/// <summary>
/// 232. 用栈实现队列
/// </summary>
public class MyQueue
{


    public class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "[\"MyQueue\",\"push\",\"push\",\"peek\",\"pop\",\"empty\"]", "[[],[1],[2],[],[],[]]", "[null,null,null,1,1,false]" };

        }
    }
    public static void TestInvoke()
    {
        var data_enumerable = new Data().GetData();
        //write your custom test code.
    }

    Stack<int> stack { get; set; }
    public MyQueue()
    {
        stack = new Stack<int>();
    }

    public void Push(int x)
    {
        Stack<int> _temp = new Stack<int>();
        while (stack.Count > 0)
        {
            _temp.Push(stack.Pop());
        }
        stack.Push(x);
        while (_temp.Count > 0)
        {
            stack.Push(_temp.Pop());
        }
    }

    public int Pop()
    {
        return stack.Pop();
    }

    public int Peek()
    {
        return stack.Peek();
    }

    public bool Empty()
    {
        return stack.Count == 0;
    }
}

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.Push(x);
 * int param_2 = obj.Pop();
 * int param_3 = obj.Peek();
 * bool param_4 = obj.Empty();
 */