namespace Solutions;
/// <summary>
/// 1687. 从仓库到码头运输箱子
/// difficulty: Hard
/// https://leetcode.cn/problems/delivering-boxes-from-storage-to-ports/
/// </summary>
public class BoxDelivering_1687_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,1],[2,1],[1,1]]").ToArray(), 2, 3, 3, 4 };
            yield return new object[] { StringTo<List<int[]>>("[[1,2],[3,3],[3,1],[3,1],[2,4]]").ToArray(), 3, 3, 6, 6 };
            yield return new object[] { StringTo<List<int[]>>("[[1,4],[1,2],[2,1],[2,1],[3,2],[3,4]]").ToArray(), 3, 6, 7, 6 };
            yield return new object[] { StringTo<List<int[]>>("[[2,4],[2,5],[3,1],[3,2],[3,7],[3,1],[4,4],[1,3],[5,2]]").ToArray(), 5, 5, 7, 14 };
        }
    }

    [Data]
    public int BoxDelivering(int[][] boxes, int portsCount, int maxBoxes, int maxWeight)
    {
        int n = boxes.Length;

        // 注意不能用sum,会出现数值溢出
        //var sumWeight = boxes.Sum(x => x[1]);

        if (n <= maxBoxes) // 处理能够一次装完的特殊情况
        {
            var sumWeight = 0;

            int prevPorts = -1;

            int res = 1;

            bool flag = true;

            foreach (var item in boxes)
            {
                sumWeight += item[1];
                if (sumWeight > maxWeight)
                {
                    flag = false;
                    break;
                }
                if (item[0] != prevPorts)
                {
                    res++;
                    prevPorts = item[0];
                }
            }
            if (flag)
            {
                return res;
            }
        }

        var dp = new int[n + 1];

        for (int i = n - 1; i >= 0; i--)
        {
            int min = 2 + dp[i + 1];

            int weight = boxes[i][1];

            int boxCount = 1;

            List<int> targetPorts = new List<int>() { boxes[i][0] };

            for (int j = i + 1; j < n; j++)
            {
                weight += boxes[j][1];
                boxCount++;

                if (weight > maxWeight) break;
                if (boxCount > maxBoxes) break;

                if (boxes[j][0] != targetPorts[targetPorts.Count - 1])
                {
                    targetPorts.Add(boxes[j][0]);
                }

                min = Math.Min(min, targetPorts.Count + 1 + dp[j + 1]);

            }

            dp[i] = min;
        }

        return dp[0];
    }
}