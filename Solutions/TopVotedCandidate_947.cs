namespace Solutions;
/// <summary>
/// 947. 在线选举
/// </summary>
public class TopVotedCandidate
{
	private int[] voteList;
	private int[] candicates = new int[5000];
	private int first_top_count = 0;
	private int first_top_person = 0;

	public TopVotedCandidate(int[] persons, int[] times)
	{
		voteList = new int[times[times.Length - 1]];
		for (int i = 0; i < times[0]; i++)
		{
			voteList[i] = -1;
		}
		for (int i = 0; i < times.Length - 1; i++)
		{
			var winner = checkWhoWin(persons[i]);
			//当前时间节点到下一个时间节点前都是winner赢了
			for (int j = times[i]; j < times[i + 1]; j++)
			{
				voteList[j] = winner;
			}
		}
		{
			//最后一个投票人
			var winner = checkWhoWin(persons[times.Length - 1]);
			voteList[times[times.Length - 1] - 1] = winner;
		}
	}

	public int Q(int t)
	{
		if (t >= voteList.Count()) return first_top_person;
		return voteList[t];
	}

	//判断此时谁赢了
	private int checkWhoWin(int vote_person)
	{
		candicates[vote_person]++;
		if (candicates[vote_person] >= first_top_count)
		{
			first_top_count = candicates[vote_person];
			first_top_person = vote_person;
		}
		return first_top_person;
	}
}

/**
 * Your TopVotedCandidate object will be instantiated and called as such:
 * TopVotedCandidate obj = new TopVotedCandidate(persons, times);
 * int param_1 = obj.Q(t);
 */