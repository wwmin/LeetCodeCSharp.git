namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 237. 删除链表中的节点
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int x) { val = x; }
 * }
 */
public class DeleteNodeSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[4,5,1,9]"), 5, ToListNodeWithString("[4,1,9]") };
            yield return new object[] { ToListNodeWithString("[4,5,1,9]"), 1, ToListNodeWithString("[4,5,9]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4]"), 3, ToListNodeWithString("[1,2,4]") };
            yield return new object[] { ToListNodeWithString("[0,1]"), 0, ToListNodeWithString("[1]") };
            yield return new object[] { ToListNodeWithString("[-3,5,-99]"), -3, ToListNodeWithString("[5,-99]") };
        }
    }
    [Data]
    public void DeleteNode(ListNode node)
    {
        node.val = node.next.val;
        node.next = node.next.next;
    }
}