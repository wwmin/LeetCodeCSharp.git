﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 539. 最小时间差
/// </summary>
public class FindMinDifferenceSolution 
    {
        /// <summary>
        /// 12小时分钟数
        /// </summary>
        private static int twelveHourMinutes = 12 * 60;
        /// <summary>
        /// 24小时分钟数
        /// </summary>
        private static int twentyFourHourMinutes = 24 * 60;

        [InlineData(new string[] { "00:00", "00:00", "23:59" }, 0)]
        [InlineData(new string[] { "12:12", "12:13", "00:12", "00:13" }, 1)]
        public int FindMinDifference(IList<string> timePoints)
        {
            var ts = timePoints.Select(p => ParseTime(p));
            var ss = ts.Select(p => SumMinutes(p)).ToList();
            ss.Sort();
            var l = ss.Count;
            ss.Add(ss[0]);
            var res = new List<int>();
            for (int i = 0; i < l; i++)
            {
                var r = ss[i] >= ss[i + 1] ? ss[i] - ss[i + 1] : ss[i + 1] - ss[i];
                if (r == 0) return 0;
                if (r > twelveHourMinutes) r = twentyFourHourMinutes > r ? twentyFourHourMinutes - r : r - twentyFourHourMinutes;
                res.Add(r);
            }
            return res.Min();
        }

        /// <summary>
        /// 格式化时间
        /// </summary>
        /// <param name="timeString">"23:59"</param>
        /// <returns></returns>
        private int[] ParseTime(string timeString) => timeString.Split(":").Select(p => int.Parse(p)).ToArray();

        /// <summary>
        /// 将小时分钟转成总分钟数
        /// </summary>
        /// <param name="hhmm"></param>
        /// <returns></returns>
        private int SumMinutes(int[] hhmm) => hhmm[0] * 60 + hhmm[1];
    }

