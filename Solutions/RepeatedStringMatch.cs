namespace Solutions;
/// <summary>
/// 686. 重复叠加字符串匹配
/// </summary>
public class RepeatedStringMatchSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcd", "cdabcdab", 3 };
            yield return new object[] { "a", "aa", 2 };
            yield return new object[] { "a", "a", 1 };
            yield return new object[] { "aa", "a", 1 };
            yield return new object[] { "abc", "wxyz", -1 };
            yield return new object[] { "abc", "cabcabca", 4 };
            yield return new object[] { "aaaaaaaaaaaaaaaaaaaaaab", "ba", 2 };
            yield return new object[] { "abcd", "bcdab", 2 };
        }
    }

    [Data]
    public int RepeatedStringMatch(string a, string b)
    {
        int m = a.Length;
        int n = b.Length;
        int firstIndex = b.IndexOf(a);
        int lastIndex = b.LastIndexOf(a);
        if (n < m)
        {
            var ai1 = a.IndexOf(b);
            var ai2 = (a + a).IndexOf(b);
            if (ai1 == -1 && ai2 == -1) return -1;
            if (ai1 == -1)
            {
                if (ai2 != -1) return 2;
                return -1;
            }
            else
            {
                return 1;
            }
        }
        int k = (lastIndex + m - firstIndex) / m;
        if (k == 0) return -1;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < k; i++)
        {
            sb.Append(a);
        }

        string subString = sb.ToString();
        int ido = b.IndexOf(subString);
        if (ido == -1)
        {

            var ai2 = (a + a).IndexOf(b);
            if (ai2 == -1) return -1;
            else
            {
                return 2;
            }
        }
        string startString = b[..ido];
        string endString = b[(startString.Length + subString.Length)..];
        if (startString.Length == 0 && endString.Length == 0) return k;
        if (startString.Length == 0 && a.StartsWith(endString)) return k + 1;
        if (endString.Length == 0 && a.EndsWith(startString)) return k + 1;
        if (a.StartsWith(endString) && a.EndsWith(startString)) return k + 2;
        return -1;
    }
}