namespace Solutions;
/// <summary>
/// 1040. 最大二叉树 II
/// https://leetcode.cn/problems/maximum-binary-tree-ii/
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class InsertIntoMaxTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[4,1,3,null,null,2]"), 5, ToTreeNodeWithString("[5,4,null,1,3,null,null,2]") };
            yield return new object[] { ToTreeNodeWithString("[5,2,4,null,1]"), 3, ToTreeNodeWithString("[5,2,4,null,1,null,3]") };
            yield return new object[] { ToTreeNodeWithString("[5,2,3,null,1]"), 4, ToTreeNodeWithString("[5,2,4,null,1,3]") };
        }
    }
    [Data]
    public TreeNode InsertIntoMaxTree(TreeNode root, int val)
    {
        TreeNode parent = null;
        TreeNode cur = root;
        while (cur != null)
        {
            if (val > cur.val)
            {
                if (parent == null)
                {
                    return new TreeNode(val, root, null);
                }
                TreeNode node = new TreeNode(val, cur, null);
                parent.right = node;
                return root;
            }
            else
            {
                parent = cur;
                cur = cur.right;
            }
        }
        parent.right = new TreeNode(val);
        return root;
    }
}