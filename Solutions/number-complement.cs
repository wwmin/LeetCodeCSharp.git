namespace Solutions;
/// <summary>
/// 476. 数字的补数
/// </summary>
public class FindComplementSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 5, 2 };
            yield return new object[] { 1, 0 };
        }
    }

    [Data]
    public int FindComplement(int num)
    {
        int highbit = 0;
        //2^31次方 
        for (int i = 1; i <= 30; ++i)
        {
            if (num >= 1 << i)
            {
                //找到最高位的1
                highbit = i;
            }
            else
            {
                break;
            }
        }
        //求最高位的数字
        int mask = highbit == 30 ? 0x7fffffff : (1 << (highbit + 1)) - 1;
        //与最高位数字取反
        return num ^ mask;
    }
}