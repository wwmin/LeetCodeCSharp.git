namespace Solutions;
/// <summary>
/// 290. 单词规律
/// </summary>
public class WordPatternSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abba", "dog cat cat dog", true };
            yield return new object[] { "abba", "dog cat cat fish", false };
            yield return new object[] { "aaaa", "dog cat cat dog", false };
            yield return new object[] { "abba", "dog dog dog dog", false };
        }
    }

    [Data]
    public bool WordPattern(string pattern, string s)
    {
        string[] slist = s.Split(" ");
        if (pattern.Length != slist.Length) return false;
        Dictionary<char, string> dic = new Dictionary<char, string>();
        for (int i = 0; i < pattern.Length; i++)
        {
            if (dic.ContainsKey(pattern[i]))
            {
                if (dic[pattern[i]] != slist[i]) return false;
            }
            else
            {
                if (dic.ContainsValue(slist[i])) return false;
                dic.Add(pattern[i], slist[i]);
            }
        }
        return true;
    }
}