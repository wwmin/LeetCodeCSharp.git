namespace Solutions;
/// <summary>
/// 121. 买卖股票的最佳时机
/// </summary>
public class MaxProfitSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[7,1,5,3,6,4]").ToArray(), 5 };
            yield return new object[] { StringTo<int[]>("[7,6,4,3,1]").ToArray(), 0 };
        }
    }

    [Data]
    public int MaxProfit(int[] prices)
    {
        int n = prices.Length;
        if (n == 1) return 0;
        int minPrice = int.MaxValue;
        int maxProfit = 0;
        for (int i = 0; i < n; i++)
        {
            if (prices[i] < minPrice)
            {
                minPrice = prices[i];
            }
            else
            {
                int profit = prices[i] - minPrice;
                if (profit > maxProfit)
                {
                    maxProfit = profit;
                }
            }
        }
        return maxProfit;
    }
}