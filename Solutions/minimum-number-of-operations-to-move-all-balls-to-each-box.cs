namespace Solutions;
/// <summary>
/// 1895. 移动所有球到每个盒子所需的最小操作数
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-number-of-operations-to-move-all-balls-to-each-box/
/// </summary>
public class MinOperations_1895_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "110", StringTo<int[]>("[1,1,3]").ToArray() };
            yield return new object[] { "001011", StringTo<int[]>("[11,8,5,4,3,4]").ToArray() };
        }
    }

    [Data]
    public int[] MinOperations(string boxes)
    {
        int[] ans = new int[boxes.Length];
        for (int i = 0; i < boxes.Length; i++)
        {
            int count = 0;
            for (int j = 0; j < boxes.Length; j++)
            {
                count += boxes[j] == '1' ? Math.Abs(j - i) : 0;
            }
            ans[i] = count;
        }
        return ans;
    }
}