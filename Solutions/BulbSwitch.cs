namespace Solutions;
/// <summary>
/// 319. 灯泡开关
/// </summary>
public class BulbSwitchSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 1 };
            yield return new object[] { 0, 0 };
            yield return new object[] { 1, 1 };
        }
    }
    [Data]
    public int BulbSwitch(int n)
    {
        return (int)Math.Sqrt(n + 0.5);
    }
}