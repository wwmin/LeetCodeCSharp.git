namespace Solutions;
/// <summary>
/// 1894. 交替合并字符串
/// https://leetcode.cn/problems/merge-strings-alternately/
/// </summary>
public class MergeAlternatelySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abc", "pqr", "apbqcr" };
            yield return new object[] { "ab", "pqrs", "apbqrs" };
            yield return new object[] { "abcd", "pq", "apbqcd" };
        }
    }

    [Data]
    public string MergeAlternately(string word1, string word2)
    {
        StringBuilder sb = new StringBuilder(word1.Length + word2.Length);
        int n = Math.Min(word1.Length, word2.Length);
        for (int i = 0; i < n; i++)
        {
            sb.Append(word1[i]);
            sb.Append(word2[i]);
        }
        if (n < word1.Length) sb.Append(word1[n..]);
        else sb.Append(word2[n..]);
        return sb.ToString();
    }
}