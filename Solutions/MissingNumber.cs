namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 268. 丢失的数字
/// </summary>
public class MissingNumberSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[3,0,1]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[0,1]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[9,6,4,2,3,5,7,0,1]").ToArray(), 8 };
            yield return new object[] { StringTo<int[]>("[0]").ToArray(), 1 };
        }
    }
    [Data]
    public int MissingNumber(int[] nums)
    {
        int n = nums.Length;
        int[] arr = new int[n + 1];
        for (int i = 0; i < n; i++)
        {
            int curr = nums[i];
            arr[curr] = 1;
        }
        for (int i = 0; i <= n; i++)
        {
            if (arr[i] == 0) return i;
        }
        return default;
    }
}