using System.Collections;

namespace Solutions;
/// <summary>
/// 630. 课程表 III
/// </summary>
public class ScheduleCourseSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[100,200],[200,1300],[1000,1250],[2000,3200]]").ToArray(), 3 };
            yield return new object[] { StringTo<List<int[]>>("[[1,2]]").ToArray(), 1 };
            yield return new object[] { StringTo<List<int[]>>("[[3,2],[4,3]]").ToArray(), 0 };
        }
    }

    [Data]
    public int ScheduleCourse(int[][] courses)
    {
        var t = courses.OrderBy(a => a[1]);
        var list = new SortedList();
        int total = 0, i = 0;
        foreach (var cour in t)
        {
            int c0 = cour[0], c1 = cour[1];
            var sum = total + c0;
            if (sum <= c1)
            {
                total = sum;
                list.Add((c0 << 14) + i++, c0);
            }
            else
            {
                var cnt = list.Count;
                if (cnt == 0) continue;
                var max = (int)list.GetByIndex(--cnt);
                if (max > c0)
                {
                    list.RemoveAt(cnt);
                    total = sum - max;
                    list.Add((c0 << 14) + i++, c0);
                }
            }
        }
        return list.Count;
    }
}