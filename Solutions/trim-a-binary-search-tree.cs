namespace Solutions;
/// <summary>
/// 669. 修剪二叉搜索树
/// https://leetcode.cn/problems/trim-a-binary-search-tree/
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class TrimBSTSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,0,2]"), 1, 2, ToTreeNodeWithString("[1,null,2]") };
            yield return new object[] { ToTreeNodeWithString("[3,0,4,null,2,null,null,1]"), 1, 3, ToTreeNodeWithString("[3,2,null,1]") };
        }
    }
    [Data]
    public TreeNode TrimBST(TreeNode root, int low, int high)
    {
        if (root == null) return null;
        if (root.val < low) return TrimBST(root.right, low, high);
        if (root.val > high) return TrimBST(root.left, low, high);
        root.left = TrimBST(root.left, low, high);
        root.right = TrimBST(root.right, low, high);
        return root;
    }
}