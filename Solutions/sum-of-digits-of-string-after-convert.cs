namespace Solutions;
/// <summary>
/// 1945. 字符串转化后的各位数字之和
/// difficulty: Easy
/// https://leetcode.cn/problems/sum-of-digits-of-string-after-convert/
/// </summary>
public class GetLucky_1945_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "iiii", 1, 36 };
            yield return new object[] { "leetcode", 2, 6 };
        }
    }

    [Data]
    public int GetLucky(string s, int k)
    {
        var sb = new StringBuilder();
        foreach (var c in s)
        {
            sb.Append(c - 'a' + 1);
        }
        for (int i = 0; i < k; i++)
        {
            var sum = GetSum(sb.ToString());
            if (sum < 10 || i == k - 1)
            {
                return sum;
            }
            sb.Clear();
            sb.Append(sum);
        }
        return default;
    }

    int GetSum(string s)
    {
        var sum = 0;
        foreach (var c in s)
        {
            sum += c - '0';
        }

        return sum;
    }
}