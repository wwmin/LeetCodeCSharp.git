namespace Solutions;
/// <summary>
/// 242. 有效的字母异位词
/// </summary>
public class IsAnagramSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "anagram", "nagaram", true };
            yield return new object[] { "rat", "car", false };
        }
    }

    [Data]
    public bool IsAnagram(string s, string t)
    {
        if (s.Length != t.Length) return false;
        int[] charArr = new int[26];
        for (int i = 0; i < s.Length; i++)
        {
            charArr[s[i] - 'a']++;
            charArr[t[i] - 'a']--;
        }
        for (int i = 0; i < charArr.Length; i++)
        {
            if (charArr[i] > 0) return false;
        }
        return true;
    }
}