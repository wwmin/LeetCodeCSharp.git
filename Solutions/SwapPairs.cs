﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 24. 两两交换链表中的节点
/// </summary>
public class SwapPairsSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { ToListNodeWithString("[]"), ToListNodeWithString("[]") };
            //yield return new object[] { ToListNodeWithString("[1]"), ToListNodeWithString("[1]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4]"), ToListNodeWithString("[2,1,4,3]") };
        }
    }

    [Data]
    public ListNode SwapPairs(ListNode head)
    {
        if (head == null || head.next == null) return head;

        ListNode lastNode = head.next;
        head.next = SwapPairs(lastNode.next);
        lastNode.next = head;
        return lastNode;
    }
}
