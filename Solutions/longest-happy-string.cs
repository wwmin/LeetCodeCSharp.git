namespace Solutions;
/// <summary>
/// 1304. 最长快乐字符串
/// </summary>
public class LongestDiverseStringSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, 1, 7, "ccaccbcc" };
            yield return new object[] { 2, 2, 1, "aabbc" };
            yield return new object[] { 7, 1, 0, "aabaa" };
        }
    }

    [Data]
    public string LongestDiverseString(int a, int b, int c)
    {

        StringBuilder res = new StringBuilder();
        Pair[] arr = { new Pair(a, 'a'), new Pair(b, 'b'), new Pair(c, 'c') };

        while (true)
        {
            Array.Sort(arr, (p1, p2) => p2.Freq - p1.Freq);
            bool hasNext = false;
            foreach (Pair pair in arr)
            {
                if (pair.Freq <= 0)
                {
                    break;
                }
                int m = res.Length;
                if (m >= 2 && res[m - 2] == pair.Ch && res[m - 1] == pair.Ch)
                {
                    continue;
                }
                hasNext = true;
                res.Append(pair.Ch);
                pair.Freq--;
                break;
            }
            if (!hasNext)
            {
                break;
            }
        }

        return res.ToString();
    }

    class Pair
    {
        public int Freq { get; set; }
        public char Ch { get; set; }

        public Pair(int Freq, char Ch)
        {
            this.Freq = Freq;
            this.Ch = Ch;
        }
    }
}