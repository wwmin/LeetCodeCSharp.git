namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 500. 键盘行
/// </summary>
public class FindWordsSolution
{

    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<string[]>("[\"Hello\",\"Alaska\",\"Dad\",\"Peace\"]").ToArray(), StringTo<string[]>("[\"Alaska\",\"Dad\"]").ToArray() };
            yield return new object[] { StringTo<string[]>("[\"omk\"]").ToArray(), StringTo<string[]>("[]").ToArray() };
            yield return new object[] { StringTo<string[]>("[\"adsdf\",\"sfd\"]").ToArray(), StringTo<string[]>("[\"adsdf\",\"sfd\"]").ToArray() };
        }
    }
    private string first_words = "qwertyuiop";
    private string second_words = "asdfghjkl";
    private string third_words = "zxcvbnm";
    [Data]
    public string[] FindWords(string[] words)
    {
        List<int> indexs = new List<int>();
        for (int i = 0; i < words.Length; i++)
        {
            var ws = words[i];
            if (ws.Length == 1) { indexs.Add(i); continue; }
            var fw = ws[0];
            var other_ws = ws[1..];
            var has_words = "";
            if (first_words.Contains(fw, StringComparison.OrdinalIgnoreCase))
            {
                has_words = first_words;
            }
            else if (second_words.Contains(fw, StringComparison.OrdinalIgnoreCase))
            {
                has_words = second_words;
            }
            else
            {
                has_words = third_words;
            }
            if (has_words.Contains(fw, StringComparison.OrdinalIgnoreCase))
            {
                bool allHas = true;
                for (int j = 0; j < other_ws.Length; j++)
                {
                    var w = other_ws[j];
                    if (!has_words.Contains(w, StringComparison.OrdinalIgnoreCase))
                    {
                        allHas = false;
                        break;
                    }
                }
                if (allHas)
                {
                    indexs.Add(i);
                }
            }
        }
        return indexs.Select(p => words[p]).ToArray();
    }
}