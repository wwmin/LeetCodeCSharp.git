namespace Solutions;
/// <summary>
/// 101. 对称二叉树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class IsSymmetricSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,2,2,3,4,4,3]"), true };
            yield return new object[] { ToTreeNodeWithString("[1,2,2,null,3,null,3]"), false };
        }
    }
    [Data]
    public bool IsSymmetric(TreeNode root)
    {
        Queue<TreeNode> queue = new Queue<TreeNode>();
        queue.Enqueue(root);
        while (queue.Count > 0)
        {
            int size = queue.Count;
            int?[] arr = new int?[size];
            for (int i = 0; i < size; i++)
            {
                TreeNode node = queue.Dequeue();
                arr[i] = node?.val;
                if (node != null)
                {
                    queue.Enqueue(node.left);
                    queue.Enqueue(node.right);
                }
            }
            if (!isSymetricArray(arr)) return false;
        }
        return true;
    }

    private bool isSymetricArray(int?[] arr)
    {
        int n = arr.Length;
        int mid = n / 2;
        if (n <= 1) return true;
        for (int i = 0; i < mid; i++)
        {
            if (arr[i] != arr[n - i - 1]) return false;
        }
        return true;
    }
}