namespace Solutions;
/// <summary>
/// 392. 判断子序列
/// </summary>
public class IsSubsequenceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "", "", true };
            yield return new object[] { "", "ahbgdc", true };
            yield return new object[] { "abc", "ahbgdc", true };
            yield return new object[] { "axc", "ahbgdc", false };
            yield return new object[] { "aaaaaa", "bbaaaa", false };
        }
    }

    [Data]
    public bool IsSubsequence(string s, string t)
    {
        if (s.Length > t.Length) return false;
        int i = 0;
        int j = 0;
        while (i < s.Length && j < t.Length)
        {
            if (s[i] == t[j])
            {
                i++;
                j++;
            }
            else
            {
                j++;
            }
        }
        return i == s.Length;
    }
}