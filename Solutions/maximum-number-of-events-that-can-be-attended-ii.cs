namespace Solutions;
/// <summary>
/// 1751. 最多可以参加的会议数目 II
/// difficulty: Hard
/// https://leetcode.cn/problems/maximum-number-of-events-that-can-be-attended-ii/
/// </summary>
public class MaxValue_1751_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,2,4],[3,4,3],[2,3,1]]").ToArray(), 2, 7 };
            yield return new object[] { StringTo<List<int[]>>("[[1,2,4],[3,4,3],[2,3,10]]").ToArray(), 2, 10 };
            yield return new object[] { StringTo<List<int[]>>("[[1,1,1],[2,2,2],[3,3,3],[4,4,4]]").ToArray(), 3, 9 };
        }
    }

    [Data]
    public int MaxValue(int[][] events, int k)
    {
        int n = events.Length;
        Array.Sort(events, (a, b) => a[1] - b[1]);
        //f[i][j] 代表考虑前i个事件，选择不超过j的最大价值
        int[,] f = new int[n + 1, k + 1];
        for (int i = 1; i <= n; i++)
        {
            int[] ev = events[i - 1];
            int s = ev[0];
            int e = ev[1];
            int v = ev[2];
            //找到第i件事件之前，与第i件事件不冲突的事件
            //对于当前事件而言，冲突与否，与j无关
            int last = 0;
            for (int p = i - 1; p >= 1; p--)
            {
                int[] prev = events[p - 1];
                if (prev[1] < s)
                {
                    last = p;
                    break;
                }
            }
            for (int j = 1; j <= k; j++)
            {
                f[i, j] = Math.Max(f[i - 1, j], f[last, j - 1] + v);
            }
        }
        return f[n, k];
    }
}