﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 11. 盛最多水的容器
/// </summary>
public class MaxAreaSolution
{
    [InlineData(new int[] { 1, 1 }, 1)]
    [InlineData(new int[] { 1, 2, 1 }, 2)]
    [InlineData(new int[] { 4, 3, 2, 1, 4 }, 16)]
    [InlineData(new int[] { 1, 8, 6, 2, 5, 4, 8, 3, 7 }, 49)]
    public int MaxArea(int[] height)
    {
        int maxArea = 0;
        //int hl = height.Length;
        //for (int i = 0; i < hl; i++)
        //{
        //    var ci = height[i];
        //    for (int j = hl - 1; j > i; j--)
        //    {
        //        var cj = height[j];
        //        var minN = ci >= cj ? cj : ci;
        //        var area = minN * (j - i);
        //        if (maxArea < area) maxArea = area;
        //    }
        //}

        int left = 0;
        int right = height.Length - 1;
        while (left < right)
        {
            var li = height[left];
            var ri = height[right];
            var area = (right - left) * (li > ri ? ri : li);
            if (maxArea < area) maxArea = area;
            if (li > ri) right--;
            else left++;
        }
        return maxArea;
    }
}
