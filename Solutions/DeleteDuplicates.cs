namespace Solutions;
/// <summary>
/// 82. 删除排序链表中的重复元素 II
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class DeleteDuplicatesSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2,3,3,4,4,5]"), ToListNodeWithString("[1,2,5]") };
            yield return new object[] { ToListNodeWithString("[1,1,1,2,3]"), ToListNodeWithString("[2,3]") };
        }
    }
    [Data]
    public ListNode DeleteDuplicates(ListNode head)
    {
        if (head == null)
        {
            return head;
        }
        ListNode ans = new ListNode(0, head);
        ListNode cur = ans;
        while (cur.next != null && cur.next.next != null)
        {
            if (cur.next.val == cur.next.next.val)
            {
                int x = cur.next.val;
                while (cur.next != null && cur.next.val == x)
                {
                    cur.next = cur.next.next;
                }
            }
            else
            {
                cur = cur.next;
            }
        }
        return ans.next;
    }
}