﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 19. 删除链表的倒数第 N 个结点
/// </summary>
public class RemoveNthFromEndSolution
{
    private class Data : DataAttribute
    {
        private ListNode GenerateNodeWithString(string s)
        {
            ListNode res = new ListNode();
            var list = JsonSerializer.Deserialize<List<int>>(s);
            ListNode currentNode = res;
            for (int i = 0; i < list.Count(); i++)
            {
                if (i == 0) res.val = list[i];
                else
                {
                    currentNode.next = new ListNode(list[i]);
                    currentNode = currentNode.next;
                }
            }
            return res;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { GenerateNodeWithString("[1,2,3,4,5]"), 2, GenerateNodeWithString("[1,2,3,5]") };
            //yield return new object[] { GenerateNodeWithString("[1]"), 1, GenerateNodeWithString("[]") };
        }
    }
    /// <summary>
    /// 逐项替代法
    /// </summary>
    /// <param name="head"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    [Data]
    public ListNode RemoveNthFromEnd(ListNode head, int n)
    {
        ListNode nthNode = head;
        ListNode prevNode = head;
        int index = 1;
        while (nthNode.next != null)
        {
            if (index > n)
            {
                prevNode = prevNode.next;
            }
            else
            {
                index++;
            }
            nthNode = nthNode.next;
        }
        if (index <= n) return head.next;
        prevNode.next = prevNode.next.next;
        return head;
    }


    /// <summary>
    /// 快慢指针法
    /// </summary>
    /// <param name="head"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    [Data]
    public ListNode RemoveNthFromEnd2(ListNode head, int n)
    {
        ListNode dumy = new ListNode(0, head);
        ListNode first = head;
        ListNode second = dumy;
        for (int i = 0; i < n; i++)
        {
            first = first.next;
        }
        while (first != null)
        {
            first = first.next;
            second = second.next;
        }
        second.next = second.next.next;
        ListNode ans = dumy.next;
        return ans;
    }

    /// <summary>
    /// 转化为数组发
    /// </summary>
    /// <param name="head"></param>
    /// <param name="n"></param>
    /// <returns></returns>
    [Data]
    public ListNode RemoveNthFromEnd3(ListNode head, int n)
    {
        ListNode res = new ListNode();
        List<int> lis = new List<int>();
        lis.Add(head.val);
        ListNode currentNode = res;
        while (head.next != null)
        {
            lis.Add(head.next.val);
            head.next = head.next.next;
        }
        lis.RemoveAt(lis.Count() - n);

        if (lis.Count() == 0) res = null;
        else
        {
            for (int i = 0; i < lis.Count(); i++)
            {
                if (i == 0) res.val = lis[i];
                else { currentNode.next = new ListNode(lis[i]); currentNode = currentNode.next; }
            }
        }
        return res;
    }



    public class ListNode
    {
        public int val;
        public ListNode next;
        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
        public override string ToString()
        {
            List<int> s = new List<int>();
            GetVal(this, s);
            return string.Join("-", s);
        }

        private void GetVal(ListNode ln, List<int> s)
        {
            if (ln != null) s.Add(ln.val);
            if (ln != null && ln.next != null) GetVal(ln.next, s);
        }
    }
}
