namespace Solutions;
/// <summary>
/// 235. 二叉搜索树的最近公共祖先
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int x) { val = x; }
 * }
 */

public class LowestCommonAncestorSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[6,2,8,0,4,7,9,null,null,3,5]"), ToTreeNodeWithString("2"), ToTreeNodeWithString("8"), ToTreeNodeWithString("6") };
            yield return new object[] { ToTreeNodeWithString("[6,2,8,0,4,7,9,null,null,3,5]"), ToTreeNodeWithString("2"), ToTreeNodeWithString("4"), ToTreeNodeWithString("2") };
        }
    }
    [Data]
    public TreeNode LowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q)
    {
        if (root.val < p.val && root.val < q.val) return LowestCommonAncestor(root.right, p, q);
        if (root.val > p.val && root.val > q.val) return LowestCommonAncestor(root.left, p, q);
        return root;
    }
}