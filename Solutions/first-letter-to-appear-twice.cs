namespace Solutions;
/// <summary>
/// 2351. 第一个出现两次的字母
/// difficulty: Easy
/// https://leetcode.cn/problems/first-letter-to-appear-twice/
/// </summary>
public class RepeatedCharacter_2351_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{"abccbaacz", 'c'};
            yield return new object[]{"abcdd", 'd'};
        }
    }

    [Data]
    public char RepeatedCharacter(string s)
    {
        HashSet<char> hs=new HashSet<char>();
        for (int i = 0; i < s.Length; i++)
        {
            if(hs.Contains(s[i]))return s[i];
            hs.Add(s[i]);
        }
        return default;
    }
}