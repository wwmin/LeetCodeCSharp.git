﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 1928. 规定时间内到达终点的最小花费
/// </summary>
public class MinCostSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 30, StringTo<List<int[]>>("[[0,1,10],[1,2,10],[2,5,10],[0,3,1],[3,4,10],[4,5,15]]").ToArray(), StringTo<int[]>("[5,1,2,20,20,3]"), 11 };
        }
    }
    [Data]
    public int MinCost(int maxTime, int[][] edges, int[] passingFees)
    {
        int n = passingFees.Length;
        const int INF = int.MaxValue / 2;
        int[,] f = new int[maxTime + 1, n];
        for (int i = 0; i < maxTime + 1; i++)
        {
            for (int j = 0; j < n; j++)
            {
                f[i, j] = INF;
            }
        }
        f[0, 0] = passingFees[0];
        for (int t = 0; t < maxTime + 1; t++)
        {
            foreach (var edge in edges)
            {
                int i = edge[0];
                int j = edge[1];
                int cost = edge[2];
                if (cost <= t)
                {
                    f[t, i] = Math.Min(f[t, i], f[t - cost, j] + passingFees[i]);
                    f[t, j] = Math.Min(f[t, j], f[t - cost, i] + passingFees[j]);
                }
            }
        }
        int ans = INF;
        for (int t = 1; t <= maxTime; t++)
        {
            ans = Math.Min(ans, f[t, n - 1]);
        }
        return ans == INF ? -1 : ans;
    }
}
