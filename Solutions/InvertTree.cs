namespace Solutions;
/// <summary>
/// 226. 翻转二叉树
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class InvertTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[4,2,7,1,3,6,9]"), ToTreeNodeWithString("[4,7,2,9,6,3,1]") };
            yield return new object[] { ToTreeNodeWithString("[2,1,3]"), ToTreeNodeWithString("[2,3,1]") };
            yield return new object[] { ToTreeNodeWithString("[]"), ToTreeNodeWithString("[]") };
        }
    }
    [Data]
    public TreeNode InvertTree(TreeNode root)
    {
        if (root == null) return null;
        TreeNode temp = root.right;
        //中序遍历
        root.right = InvertTree(root.left);
        root.left = InvertTree(temp);
        return root;
    }

}