﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 10. 正则表达式匹配
/// </summary>
public class IsMatchSolution
{
    /// <summary>
    /// 动态规划,状态转移
    /// </summary>
    /// <param name="s"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    [InlineData("aa", "a", false)]
    [InlineData("aa", "a*", true)]
    [InlineData("ab", ".*", true)]
    [InlineData("aab", "c*a*b", true)]
    [InlineData("mississippi", "mis*is*p*.", false)]
    public bool IsMatch(string s, string p)
    {
        int m = s.Length;
        int n = p.Length;
        bool[,] f = new bool[m + 1, n + 1];
        f[0, 0] = true;
        for (int i = 0; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (p[j - 1] == '*')
                {
                    f[i, j] = f[i, j - 2];
                    if (matches(s, p, i, j - 1))
                    {
                        f[i, j] = f[i, j] || f[i - 1, j];
                    }
                }
                else
                {
                    if (matches(s, p, i, j))
                    {
                        f[i, j] = f[i - 1, j - 1];
                    }
                }
            }
        }
        return f[m, n];
    }

    public bool matches(string s, string p, int i, int j)
    {
        if (i == 0) return false;
        if (p[j - 1] == '.') return true;
        return s[i - 1] == p[j - 1];
    }
}