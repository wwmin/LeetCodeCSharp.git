﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 59. 螺旋矩阵 II
/// </summary>
public class GenerateMatrixSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, StringTo<List<int[]>>("[[1,2,3],[8,9,4],[7,6,5]]").ToArray() };
        }
    }
    [Data]
    public int[][] GenerateMatrix(int n)
    {
        int[][] matrix = new int[n][];
        for (int i = 0; i < n; i++)
        {
            matrix[i] = new int[n];
        }
        int allN = n * n;
        int k = 1;
        int left = 0;
        int right = n - 1;
        int top = 0;
        int bottom = n - 1;
        while (k <= allN)
        {
            for (int i = left; i <= right; i++)
            {
                matrix[top][i] = k++;
            }
            top++;
            for (int i = top; i <= bottom; i++)
            {
                matrix[i][right] = k++;
            }
            right--;
            for (int i = right; i >= left; i--)
            {
                matrix[bottom][i] = k++;
            }
            bottom--;
            for (int i = bottom; i >= top; i--)
            {
                matrix[i][left] = k++;
            }
            left++;
        }
        return matrix;
    }
}