namespace Solutions;
/// <summary>
/// 287. 寻找重复数
/// https://leetcode.cn/problems/find-the-duplicate-number/
/// </summary>
public class FindDuplicateSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[1,3,4,2,2]").ToArray(), 2};
            yield return new object[]{StringTo<int[]>("[3,1,3,4,2]").ToArray(), 3};
        }
    }
    [Data]
    public int FindDuplicate(int[] nums)
    {
        //快慢指针,2n 次方 超时
        int slow = 0;
        int fast = 0;
        do{
            slow = nums[slow];
            fast = nums[nums[fast]];
        }while (slow != fast);
        slow = 0;
        while(slow!=fast){
            slow = nums[slow];
            fast = nums[fast];
        }
        return slow;
    }

    public int FindDuplicate2(int[] nums)
    {
        //快慢指针,2n 次方 超时
        int n=nums.Length-1;
        for (int left = 0; left < n; left++)
        {
            var leftValue = nums[left];
            for(int right = left+1;right<=n;right++){
                var rightValue = nums[right];
                if(leftValue == rightValue)return leftValue;
            }
        }
        return default;
    }

    public int FindDuplicate1(int[] nums)
    {
        //排序，相邻两个数相同，即为答案
        Array.Sort(nums);
        int pre = nums[0];
        for (int i = 1; i < nums.Length; i++)
        {
            if(pre == nums[i])return pre;
            pre = nums[i];
        }
        return pre;
    }
}