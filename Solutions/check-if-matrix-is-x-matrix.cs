namespace Solutions;
/// <summary>
/// 2319. 判断矩阵是否是一个 X 矩阵
/// difficulty: Easy
/// https://leetcode.cn/problems/check-if-matrix-is-x-matrix/
/// </summary>
public class CheckXMatrix_2319_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[2,0,0,1],[0,3,1,0],[0,5,2,0],[4,0,0,2]]").ToArray(), true };
            yield return new object[] { StringTo<List<int[]>>("[[5,7,0],[0,3,1],[0,5,0]]").ToArray(), false };
            yield return new object[] { StringTo<List<int[]>>("[[5,0,0,1],[0,4,1,5],[0,5,2,0],[4,1,0,2]]").ToArray(), false };
        }
    }

    [Data]
    public bool CheckXMatrix(int[][] grid)
    {
        for (int i = 0; i < grid.Length; i++)
        {
            for (int j = 0; j < grid[i].Length; j++)
            {
                if (i == j || i + j == grid.Length - 1)
                {
                    if (grid[i][j] == 0)
                    {
                        return false;
                    }
                }
                else
                {
                    if (grid[i][j] != 0)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}