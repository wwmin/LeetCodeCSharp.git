namespace Solutions;
/// <summary>
/// 2032. 至少在两个数组中出现的值
/// difficulty: Easy
/// https://leetcode.cn/problems/two-out-of-three/
/// </summary>
public class TwoOutOfThree_2032_Solution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[]{StringTo<int[]>("[1,1,3,2]").ToArray(), StringTo<int[]>("[2,3]").ToArray(), StringTo<int[]>("[3]").ToArray(), StringTo<List<int>>("[3,2]")};
            yield return new object[]{StringTo<int[]>("[3,1]").ToArray(), StringTo<int[]>("[2,3]").ToArray(), StringTo<int[]>("[1,2]").ToArray(), StringTo<List<int>>("[2,3,1]")};
            yield return new object[]{StringTo<int[]>("[1,2,2]").ToArray(), StringTo<int[]>("[4,3,3]").ToArray(), StringTo<int[]>("[5]").ToArray(), StringTo<List<int>>("[]")};
        }
    }

    [Data]
    public IList<int> TwoOutOfThree(int[] nums1, int[] nums2, int[] nums3)
    {
        var set1 = new HashSet<int>(nums1);
        var set2 = new HashSet<int>(nums2);
        var set3 = new HashSet<int>(nums3);
        var set = new HashSet<int>();
        foreach(var item in set1){
            if(set2.Contains(item) || set3.Contains(item)){
                set.Add(item);
            }
        }
        foreach(var item in set2){
            if(set3.Contains(item)){
                set.Add(item);
            }
        }
        return set.ToList();
    }
}