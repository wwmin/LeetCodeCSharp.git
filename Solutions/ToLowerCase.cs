namespace Solutions;
/// <summary>
/// 742. 转换成小写字母
/// </summary>
public class ToLowerCaseSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "Hello", "hello" };
            yield return new object[] { "here", "here" };
            yield return new object[] { "LOVELY", "lovely" };
            yield return new object[] { "LOVELY@$%&*[", "lovely@$%&*[" };
        }
    }

    [Data]
    public string ToLowerCase(string s)
    {
        //方法一: API法,地球人都知道
        //return s.ToLower();
        //方法二: 十进制比较.
        //分析发现
        //a 的 十进制为 97, A的十进制为 76 ,十进制相差32
        //如果使用十进制法,只需要判断是否为大写,是大写就加32
        //{
        //    StringBuilder sb = new StringBuilder();
        //    for (int i = 0; i < s.Length; i++)
        //    {
        //        if (s[i] <= 'Z' && s[i] >= 'A') sb.Append((char)(s[i] + 32));
        //        else sb.Append(s[i]);
        //    }
        //    return sb.ToString();
        //}
        //方法三: 二进制比较
        //a的二进制为1100001,A的二进制为1000001,二进制相差100000
        //如果使用二进制法,只需要判断是否为大写,是大写就操作位加100000,小写保持不变,
        //此时就应当使用|或100000,直接比较位
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] <= 'Z' && s[i] >= 'A') sb.Append((char)(s[i] | 0b_100000));
                else sb.Append(s[i]);
            }
            return sb.ToString();
        }
    }
}