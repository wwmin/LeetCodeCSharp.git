﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 787. K 站中转内最便宜的航班
/// </summary>
public class FindCheapestPriceSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, StringTo<List<int[]>>("[[0,1,100],[1,2,100],[0,2,500]]").ToArray(), 0, 2, 1, 200 };
        }
    }

    [Data]
    public int FindCheapestPrice(int n, int[][] flights, int src, int dst, int k)
    {
        //不合法的时候的花费,不能用int.MaxValue 因INF+cost 会超出界限
        //不合法的时候的花费,应取大于 航班花费10^4* 航班次数 k+1=101 即 10000*101+1
        const int INF = 10000 * 101 + 1;
        //搭乘航班的次数二维数组
        int[,] f = new int[k + 2, n];
        for (int i = 0; i < k + 2; i++)
        {
            for (int j = 0; j < n; j++)
            {
                //默认都时不合法的花费
                f[i, j] = INF;
            }
        }
        //不搭乘航班,直接到时,花费为0
        f[0, src] = 0;
        for (int t = 1; t <= k + 1; t++)
        {
            foreach (int[] flight in flights)
            {
                //出发城市
                int from = flight[0];
                //到达城市
                int to = flight[1];
                //花费
                int cost = flight[2];
                //状态转移方程, 当前花费与(上一次的搭乘+花费) 取最小值
                f[t, to] = Math.Min(f[t, to], f[t - 1, from] + cost);
            }
        }

        int ans = INF;
        //找到中转次数内的最小花费
        for (int t = 0; t <= k + 1; t++)
        {
            ans = Math.Min(ans, f[t, dst]);
        }
        return ans == INF ? -1 : ans;
    }
}