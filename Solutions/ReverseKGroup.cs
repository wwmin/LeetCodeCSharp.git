﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 25. K 个一组翻转链表
/// </summary>
public class ReverseKGroupSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1]"), 1, ToListNodeWithString("[1]") };
            yield return new object[] { ToListNodeWithString("[1,2]"), 2, ToListNodeWithString("[2,1]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4,5]"), 1, ToListNodeWithString("[1,2,3,4,5]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4,5]"), 2, ToListNodeWithString("[2,1,4,3,5]") };
            yield return new object[] { ToListNodeWithString("[1,2,3,4,5]"), 3, ToListNodeWithString("[3,2,1,4,5]") };
        }
    }
    [Data]
    public ListNode ReverseKGroup(ListNode head, int k)
    {
        if (head == null) return head;
        ListNode hair = new ListNode(0, head);//在head前加一个初始节点
        ListNode res = hair;
        ListNode pre = new ListNode(0);
        ListNode curNode = pre;
        ListNode preTail = pre.next;

        ListNode tail = head;
        while (head != null)
        {
            for (int i = 0; i < k; i++)
            {
                if (tail == null)
                {
                    return res.next;
                }
                if (i == 0) pre.val = tail.val;
                else
                {
                    curNode.next = new ListNode(tail.val, tail.next);
                    curNode = curNode.next;
                }
                preTail = curNode.next;
                tail = tail.next;
            }
            ListNode reverseNode = reverse(pre, preTail);
            pre = new ListNode(0);
            curNode = pre;
            preTail = pre.next;
            hair.next = reverseNode;
            for (int i = 0; i < k; i++)
            {
                hair = hair.next;
                head = head.next;
            }
            hair.next = tail;
        }
        return res.next;
    }

    //[Data]
    public ListNode TestReverseListNode(ListNode head, int k)
    {
        var rn = reverse(head, head.next);
        return rn;
    }


    /// <summary>
    /// 反转链表
    /// </summary>
    /// <param name="head"></param>
    /// <param name="tail"></param>
    /// <returns></returns>
    private ListNode reverse(ListNode head, ListNode tail)
    {
        ListNode pre = null;
        ListNode cur = head;
        while (cur != tail)
        {
            ListNode _next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = _next;
        }
        return pre;
    }
}
