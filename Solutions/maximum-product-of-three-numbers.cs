namespace Solutions;
/// <summary>
/// 628. 三个数的最大乘积
/// https://leetcode.cn/problems/maximum-product-of-three-numbers/
/// </summary>
public class MaximumProductSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), 6 };
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), 24 };
            yield return new object[] { StringTo<int[]>("[-1,-2,-3]").ToArray(), -6 };
        }
    }

    [Data]
    public int MaximumProduct(int[] nums)
    {
        Array.Sort(nums);
        int n = nums.Length;
        int max1 = nums[n - 1];
        int max2 = nums[n - 2];
        int max3 = nums[n - 3];
        int min1 = nums[0];
        int min2 = nums[1];

        return Math.Max(max1 * max2 * max3, max1 * min1 * min2);
    }
}