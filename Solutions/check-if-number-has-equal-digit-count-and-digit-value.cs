namespace Solutions;
/// <summary>
/// 2283. 判断一个数的数字计数是否等于数位的值
/// difficulty: Easy
/// https://leetcode.cn/problems/check-if-number-has-equal-digit-count-and-digit-value/
/// </summary>
public class DigitCount_2283_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "1210", true };
            yield return new object[] { "030", false };
        }
    }

    [Data]
    public bool DigitCount(string num)
    {
        int[] list = new int[10];
        for (int i = 0; i < num.Length; i++)
        {
            list[num[i] - '0']++;
        }
        for (int i = 0; i < num.Length; i++)
        {
            if (num[i] - '0' != list[i]) return false;
        }
        return true;
    }
}