namespace Solutions;
/// <summary>
/// 391. 完美矩形
/// </summary>
public class IsRectangleCoverSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            //yield return new object[] { StringTo<List<int[]>>("[[1,1,3,3],[3,1,4,2],[3,2,4,4],[1,3,2,4],[2,3,3,4]]").ToArray(), true };
            //yield return new object[] { StringTo<List<int[]>>("[[1,1,2,3],[1,3,2,4],[3,1,4,2],[3,2,4,4]]").ToArray(), false };
            //yield return new object[] { StringTo<List<int[]>>("[[1,1,3,3],[3,1,4,2],[1,3,2,4],[3,2,4,4]]").ToArray(), false };
            //yield return new object[] { StringTo<List<int[]>>("[[1,1,3,3],[3,1,4,2],[1,3,2,4],[2,2,4,4]]").ToArray(), false };
            //yield return new object[] { StringTo<List<int[]>>("[[0,0,1,1],[0,0,2,1],[1,0,2,1],[0,2,2,3]]").ToArray(), false };
            yield return new object[] { StringTo<List<int[]>>("[[0,0,4,1],[7,0,8,2],[6,2,8,3],[5,1,6,3],[4,0,5,1],[6,0,7,2],[4,2,5,3],[2,1,4,3],[0,1,2,2],[0,2,2,3],[4,1,5,2],[5,0,6,1]]").ToArray(), true };
        }
    }
    [Data]
    public bool IsRectangleCover(int[][] rectangles)
    {
        Dictionary<(int x, int y), int> points = new Dictionary<(int x, int y), int>();
        double area = 0;
        int minX1 = rectangles[0][0];
        int minY1 = rectangles[0][1];
        int maxX2 = rectangles[0][2];
        int maxY2 = rectangles[0][3];
        for (int i = 0; i < rectangles.Length; i++)
        {
            var rect = rectangles[i];
            var x1 = rect[0];
            var y1 = rect[1];
            var x2 = rect[2];
            var y2 = rect[3];
            minX1 = Math.Min(minX1, x1);
            minY1 = Math.Min(minY1, y1);
            maxX2 = Math.Max(maxX2, x2);
            maxY2 = Math.Max(maxY2, y2);
            area += (x2 - x1) * (y2 - y1);
            var p1 = (x1, y1);
            var p2 = (x1, y2);
            var p3 = (x2, y1);
            var p4 = (x2, y2);
            if (points.ContainsKey(p1)) points.Remove(p1);
            else points.Add(p1, 1);
            if (points.ContainsKey(p2)) points.Remove(p2);
            else points.Add(p2, 1);
            if (points.ContainsKey(p3)) points.Remove(p3);
            else points.Add(p3, 1);
            if (points.ContainsKey(p4)) points.Remove(p4);
            else points.Add(p4, 1);
        }
        //1. 如果面积不相等,则不是完美矩形
        var allArea = (maxX2 - minX1) * (maxY2 - minY1);
        if (allArea != area) return false;
        //2. 如果剩余的点超过4个,则不是完美矩形
        if (points.Count != 4) return false;
        //3. 如果剩余点不是最大矩形的四个点,则不是完美矩形
        if (!points.ContainsKey((minX1, minY1))) return false;
        if (!points.ContainsKey((minX1, maxY2))) return false;
        if (!points.ContainsKey((maxX2, minY1))) return false;
        if (!points.ContainsKey((maxX2, maxY2))) return false;
        return true;
    }
}