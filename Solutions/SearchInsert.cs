﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 35. 搜索插入位置
/// </summary>
public class SearchInsertSolution
{
    [InlineData(new int[] { 1 }, 0, 0)]
    [InlineData(new int[] { 1, 3 }, 2, 1)]
    [InlineData(new int[] { 1, 3 }, 4, 2)]
    [InlineData(new int[] { 1, 3, 5, 6 }, 5, 2)]
    [InlineData(new int[] { 1, 3, 5, 6 }, 2, 1)]
    [InlineData(new int[] { 1, 3, 5, 6 }, 7, 4)]
    [InlineData(new int[] { 1, 3, 5, 6 }, 0, 0)]
    public int SearchInsert(int[] nums, int target)
    {
        int n = nums.Length;
        //二分查找
        int left = 0;
        int right = n - 1;
        while (left <= right)
        {
            int mid = (left + right) / 2;
            if (nums[mid] == target)
            {
                return mid;
            }
            else if (nums[mid] < target)
            {
                left++;
            }
            else
            {
                right--;
            }
        }
        return left;
    }
}
