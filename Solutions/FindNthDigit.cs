namespace Solutions;
/// <summary>
/// 400. 第 N 位数字
/// </summary>
public class FindNthDigitSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { 3, 3 };
            yield return new object[] { 11, 0 };
        }
    }
    /*
     由于任何整数都至少是一位数，因此 dd 的最小值是 11。对于 dd 的上界，可以通过找规律的方式确定。
    1 位数的范围是 1 到 9，共有 9 个数，所有 11 位数的位数之和是 1 x 9=9。
    2 位数的取值范围是 10 到 99，共有 90 个数，所有 2 位数的位数之和是 2 x90 = 180。
    3 位数的取值范围是 100 到 999，共有 900 个数，所有 3 位数的位数之和是 3 x 900 = 2700。
    推广到一般情形，xx 位数的范围是 10^{x-1}  到 10^x - 1，共有 10^x - 1 - 10^{x - 1} + 1 = 9 x 10^{x - 1} 个数，所有 x 位数的位数之和是 x x 9 x 10^{x - 1}。
    ...

     */
    [Data]
    public int FindNthDigit(int n)
    {
        int d = 1, count = 9;
        while (n > (long)d * count)
        {
            n -= d * count;
            d++;
            count *= 10;
        }
        int index = n - 1;
        int start = (int)Math.Pow(10, d - 1);
        int num = start + index / d;
        int digitIndex = index % d;
        int digit = (num / (int)(Math.Pow(10, d - digitIndex - 1))) % 10;
        return digit;
    }
}