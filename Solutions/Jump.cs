﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 45. 跳跃游戏 II
/// </summary>
public class JumpSolution
{
    [InlineData(new int[] { 2, 3, 1, 1, 4 }, 2)]
    public int Jump(int[] nums)
    {
        //目前能跳到的最远位置
        int max_far = 0;
        //跳跃次数
        int step = 0;
        //上次条约可达范围右边界(下次的最右起跳点)
        int end = 0;
        for (int i = 0; i < nums.Length - 1; i++)
        {
            max_far = Math.Max(max_far, i + nums[i]);
            //到达上次跳跃能到达的右边界了
            if (i == end)
            {
                //目前能跳到的最远位置变成了下次起跳位置的右边界
                end = max_far;
                //进入下一次跳跃
                step++;
            }
        }
        return step;
    }
}
