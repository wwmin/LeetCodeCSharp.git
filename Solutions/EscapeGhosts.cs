﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 789. 逃脱阻碍者
/// </summary>
public class EscapeGhostsSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<int[]>>("[[1,0],[0,3]]").ToArray(), StringTo<List<int>>("[0,1]").ToArray(), true };
        }
    }

    [Data]
    public bool EscapeGhosts(int[][] ghosts, int[] target)
    {

        int[] source = { 0, 0 };
        int distance = manhttanDistance(source, target);
        for (int i = 0; i < ghosts.Length; i++)
        {
            var ghost = ghosts[i];
            int ghostDistance = manhttanDistance(ghost, target);
            if (ghostDistance <= distance) return false;
        }

        return true;
    }

    private int manhttanDistance(int[] point1, int[] point2)
    {
        return Math.Abs(point1[0] - point2[0]) + Math.Abs(point1[1] - point2[1]);
    }
}
