namespace Solutions;
/// <summary>
/// 206. 反转链表
/// </summary>
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     public int val;
 *     public ListNode next;
 *     public ListNode(int val=0, ListNode next=null) {
 *         this.val = val;
 *         this.next = next;
 *     }
 * }
 */
public class ReverseListSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToListNodeWithString("[1,2,3,4,5]"), ToListNodeWithString("[5,4,3,2,1]") };
            yield return new object[] { ToListNodeWithString("[1,2]"), ToListNodeWithString("[2,1]") };
            yield return new object[] { ToListNodeWithString("[]"), ToListNodeWithString("[]") };
        }
    }
    [Data]
    public ListNode ReverseList(ListNode head)
    {
        if (head == null) return head;
        ListNode ans = new ListNode(head.val);
        head = head.next;
        while (head != null)
        {
            ListNode ln = new ListNode(head.val);
            ln.next = ans;
            ans = ln;
            head = head.next;
        }
        return ans;
    }
}