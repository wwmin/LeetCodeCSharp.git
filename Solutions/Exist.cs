namespace Solutions;
/// <summary>
/// 79. 单词搜索
/// </summary>
public class ExistSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<char[]>>("[[\'A\',\'B\',\'C\',\'E\'],[\'S\',\'F\',\'C\',\'S\'],[\'A\',\'D\',\'E\',\'E\']]").ToArray(), "ABCCED", true };
            yield return new object[] { StringTo<List<char[]>>("[[\'A\',\'B\',\'C\',\'E\'],[\'S\',\'F\',\'C\',\'S\'],[\'A\',\'D\',\'E\',\'E\']]").ToArray(), "SEE", true };
            yield return new object[] { StringTo<List<char[]>>("[[\'A\',\'B\',\'C\',\'E\'],[\'S\',\'F\',\'C\',\'S\'],[\'A\',\'D\',\'E\',\'E\']]").ToArray(), "ABCB", false };
        }
    }
    [Data]
    public bool Exist(char[][] board, string word)
    {
        int m = board.Length;
        int n = board[0].Length;
        bool[,] visited = new bool[m, n];
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                bool flag = check(board, visited, i, j, word, 0);
                if (flag) return true;
            }
        }
        return false;
    }

    private bool check(char[][] board, bool[,] visited, int i, int j, string s, int k)
    {
        if (board[i][j] != s[k]) return false;
        else if (k == s.Length - 1) return true;
        visited[i, j] = true;
        int[][] directions = new int[][] { new int[] { 0, 1 }, new int[] { 0, -1 }, new int[] { 1, 0 }, new int[] { -1, 0 } };
        bool result = false;
        foreach (var dir in directions)
        {
            int newi = i + dir[0];
            int newj = j + dir[1];
            if (newi >= 0 && newi < board.Length && newj >= 0 && newj < board[0].Length)
            {
                if (!visited[newi, newj])
                {
                    bool flag = check(board, visited, newi, newj, s, k + 1);
                    if (flag)
                    {
                        result = true;
                        break;
                    }
                }
            }
        }
        visited[i, j] = false;
        return result;
    }
}