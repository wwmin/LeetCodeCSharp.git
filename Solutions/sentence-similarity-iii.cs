namespace Solutions;
/// <summary>
/// 1813. 句子相似性 III
/// difficulty: Medium
/// https://leetcode.cn/problems/sentence-similarity-iii/
/// </summary>
public class AreSentencesSimilar_1813_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "My name is Haley", "My Haley", true };
            yield return new object[] { "of", "A lot of words", false };
            yield return new object[] { "Eating right now", "Eating", true };
            yield return new object[] { "Luky", "Lucccky", false };
        }
    }

    [Data]
    public bool AreSentencesSimilar(string sentence1, string sentence2)
    {
        string[] words1 = sentence1.Split(" ");
        string[] words2 = sentence2.Split(" ");
        int i = 0, j = 0;
        while (i < words1.Length && i < words2.Length && words1[i].Equals(words2[i]))
        {
            i++;
        }
        while (j < words1.Length - i && j < words2.Length - i && words1[words1.Length - j - 1].Equals(words2[words2.Length - j - 1]))
        {
            j++;
        }
        return i + j == Math.Min(words1.Length, words2.Length);
    }
}