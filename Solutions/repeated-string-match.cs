namespace Solutions;
/// <summary>
/// 686. 重复叠加字符串匹配
/// difficulty: Medium
/// https://leetcode.cn/problems/repeated-string-match/
/// </summary>
public class RepeatedStringMatch_686_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "abcd", "cdabcdab", 3 };
            yield return new object[] { "a", "aa", 2 };
            yield return new object[] { "a", "a", 1 };
            yield return new object[] { "abc", "wxyz", -1 };
        }
    }

    [Data]
    public int RepeatedStringMatch(string a, string b)
    {
        StringBuilder sb = new StringBuilder();
        int ans = 0;
        int max = 2 * a.Length + b.Length;
        while (sb.Length < max)
        {
            sb.Append(a);
            ans++;
            if (sb.ToString().IndexOf(b) != -1) return ans;
        }
        return -1;
    }
}