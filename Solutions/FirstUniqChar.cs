namespace Solutions;
/// <summary>
/// 387. 字符串中的第一个唯一字符
/// </summary>
public class FirstUniqCharSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "leetcode", 0 };
            yield return new object[] { "loveleetcode", 2 };
            yield return new object[] { "aabb", -1 };
        }
    }

    [Data]
    public int FirstUniqChar(string s)
    {
        Dictionary<char, int> dic = new Dictionary<char, int>();
        for (int i = 0; i < s.Length; i++)
        {
            if (dic.ContainsKey(s[i]))
            {
                dic[s[i]]++;
            }
            else
            {
                dic.Add(s[i], 1);
            }
        }
        for (int i = 0; i < s.Length; i++)
        {
            if (dic[s[i]] == 1) return i;
        }
        return -1;
    }
}