namespace Solutions;
/// <summary>
/// 85. 最大矩形
/// </summary>
public class MaximalRectangleSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<List<char[]>>("[[\'1\',\'0\',\'1\',\'0\',\'0\'],[\'1\',\'0\',\'1\',\'1\',\'1\'],[\'1\',\'1\',\'1\',\'1\',\'1\'],[\'1\',\'0\',\'0\',\'1\',\'0\']]").ToArray(), 6 };
            yield return new object[] { StringTo<List<char[]>>("[]").ToArray(), 0 };
            yield return new object[] { StringTo<List<char[]>>("[[\'0\']]").ToArray(), 0 };
            yield return new object[] { StringTo<List<char[]>>("[[\'1\']]").ToArray(), 1 };
            yield return new object[] { StringTo<List<char[]>>("[[\'0\',\'0\']]").ToArray(), 0 };
        }
    }

    [Data]
    public int MaximalRectangle(char[][] matrix)
    {
        int m = matrix.Length;
        if (m == 0)
        {
            return 0;
        }
        int n = matrix[0].Length;
        int[][] left = new int[m][];
        for (int i = 0; i < m; i++)
        {
            left[i] = new int[n];
        }

        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (matrix[i][j] == '1')
                {
                    left[i][j] = (j == 0 ? 0 : left[i][j - 1]) + 1;
                }
            }
        }

        int ret = 0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (matrix[i][j] == '0')
                {
                    continue;
                }
                int width = left[i][j];
                int area = width;
                for (int k = i - 1; k >= 0; k--)
                {
                    width = Math.Min(width, left[k][j]);
                    area = Math.Max(area, (i - k + 1) * width);
                }
                ret = Math.Max(ret, area);
            }
        }
        return ret;
    }
}