namespace Solutions;
/// <summary>
/// 543. 二叉树的直径
/// https://leetcode.cn/problems/diameter-of-binary-tree/
/// </summary>
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left;
 *     public TreeNode right;
 *     public TreeNode(int val=0, TreeNode left=null, TreeNode right=null) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
public class DiameterOfBinaryTreeSolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { ToTreeNodeWithString("[1,2,3,4,5]"), 3 };
        }
    }

    int max = 0;
    [Data]
    public int DiameterOfBinaryTree(TreeNode root)
    {
        if (root == null) return max;
        dfs(root);
        return max;
    }

    private int dfs(TreeNode root)
    {
        if (root == null)
        {
            return 0;
        }
        int leftSize = dfs(root.left);
        int rightSize = dfs(root.right);
        max = Math.Max(max, leftSize + rightSize);
        return Math.Max(leftSize, rightSize) + 1;
    }
}