namespace Solutions;
/// <summary>
/// 1750. 删除字符串两端相同字符后的最短长度
/// difficulty: Medium
/// https://leetcode.cn/problems/minimum-length-of-string-after-deleting-similar-ends/
/// </summary>
public class MinimumLength_1750_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "c", 1 };
            yield return new object[] { "ca", 2 };
            yield return new object[] { "cabaabac", 0 };
            yield return new object[] { "aabccabba", 3 };
            yield return new object[] { "bbbbbbbbbbbbbbbbbbbbbbbbbbbabbbbbbbbbbbbbbbccbcbcbccbbabbb", 1 };
        }
    }

    [Data]
    public int MinimumLength(string s)
    {
        int left = 0;
        int right = s.Length - 1;
        while (left < right && s[left] == s[right])
        {
            char c = s[left];
            while (left <= right && s[left] == c)
            {
                left++;
            }
            while (left <= right && s[right] == c)
            {
                right--;
            }
        }

        return right - left + 1;
    }
}