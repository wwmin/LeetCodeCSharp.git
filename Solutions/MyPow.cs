﻿namespace LeetCodeCSharp.Solutions;
/// <summary>
/// 50. Pow(x, n)
/// </summary>
public class MyPowSolution
{
    [InlineData(2.00000, 10, 1024.00000)]
    public double MyPow(double x, int n)
    {
        long N = n;
        return N > 0 ? quickMul(x, N) : 1.0 / quickMul(x, -N);
    }

    private double quickMul(double x, long N)
    {
        if (N == 0) return 1.0;
        if (x == 1.0) return 1.0;
        double y = quickMul(x, N / 2);
        return N % 2 == 0 ? y * y : y * y * x;
    }
}
