namespace Solutions;
/// <summary>
/// 1798. 你能构造出连续值的最大数目
/// difficulty: Medium
/// https://leetcode.cn/problems/maximum-number-of-consecutive-values-you-can-make/
/// </summary>
public class GetMaximumConsecutive_1798_Solution : ITest
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,3]").ToArray(), 2 };
            yield return new object[] { StringTo<int[]>("[1,1,1,4]").ToArray(), 8 };
            yield return new object[] { StringTo<int[]>("[1,4,10,3,1]").ToArray(), 20 };
        }
    }

    [Data]
    public int GetMaximumConsecutive(int[] coins)
    {
        int res = 1;
        Array.Sort(coins);
        foreach (int i in coins)
        {
            if (i > res)
            {
                break;
            }
            res += i;
        }
        return res;
    }
}