﻿namespace LeetCodeCSharp.Solutions;

/// <summary>
/// 39. 组合总和
/// </summary>
public class CombinationSumSolution
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,3,6,7]"), 7, StringTo<List<int[]>>("[[2,2,3],[7]]") };
            yield return new object[] { StringTo<int[]>("[2,3,5]"), 8, StringTo<List<int[]>>("[[2,2,2,2],[2,3,3],[3,5]]") };
        }
    }
    [Data]
    public IList<IList<int>> CombinationSum(int[] candidates, int target)
    {
        List<IList<int>> res = new List<IList<int>>();
        Array.Sort(candidates);
        List<int> curr = new List<int>();
        DFS(target, candidates, res, curr, 0);
        return res;
    }

    private void DFS(int target, int[] candidates, List<IList<int>> res, List<int> curr, int i)
    {
        if (target < 0)
        {
            return;
        }
        if (target == 0)
        {
            res.Add(new List<int>(curr));
            return;
        }
        for (int start = i; start < candidates.Length; start++)
        {
            var c = candidates[start];
            curr.Add(c);
            DFS(target - c, candidates, res, curr, start);
            curr.RemoveAt(curr.Count - 1);
        }
    }
}
