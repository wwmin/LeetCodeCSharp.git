namespace Solutions;
/// <summary>
/// 1552. 用栈操作构建数组
/// https://leetcode.cn/problems/build-an-array-with-stack-operations/
/// </summary>
public class BuildArraySolution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,3]").ToArray(), 3, StringTo<List<string>>("[\"Push\",\"Push\",\"Pop\",\"Push\"]") };
            yield return new object[] { StringTo<int[]>("[1,2,3]").ToArray(), 3, StringTo<List<string>>("[\"Push\",\"Push\",\"Push\"]") };
            yield return new object[] { StringTo<int[]>("[1,2]").ToArray(), 4, StringTo<List<string>>("[\"Push\",\"Push\"]") };
        }
    }

    [Data]
    public IList<string> BuildArray(int[] target, int n)
    {
        int m = target[target.Length - 1];
        List<string> res = new List<string>();
        int j = 0;
        for (int i = 1; i <= m; i++)
        {
            if (i != target[j])
            {
                while (i < target[j])
                {
                    res.Add("Push");
                    res.Add("Pop");
                    i++;
                }
                res.Add("Push");
            }
            else
            {
                res.Add("Push");
            }
            j++;
        }
        return res;
    }
}