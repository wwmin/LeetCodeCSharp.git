namespace Solutions;
/// <summary>
/// 1781. 所有子字符串美丽值之和
/// difficulty: Medium
/// https://leetcode.cn/problems/sum-of-beauty-of-all-substrings/
/// </summary>
public class BeautySum_1781_Solution
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { "aabcb", 5 };
            yield return new object[] { "aabcbaa", 17 };
        }
    }

    [Data]
    public int BeautySum(string s)
    {
        int res = 0;
        for (int i = 0; i < s.Length; i++)
        {
            int[] cnt = new int[26];
            int maxFreq = 0;
            for (int j = i; j < s.Length; j++)
            {
                cnt[s[j] - 'a']++;
                maxFreq = Math.Max(maxFreq, cnt[s[j] - 'a']);
                int minFreq = s.Length;
                for (int k = 0; k < 26; k++)
                {
                    if (cnt[k] > 0)
                    {
                        minFreq = Math.Min(minFreq, cnt[k]);
                    }
                }
                res += maxFreq - minFreq;
            }
        }
        return res;
    }
}