﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeCSharp.Solutions
{
    /// <summary>
    /// 5985. 根据给定数字划分数组
    /// </summary>
    public class PivotArraySolution
    {
        private class Data : DataAttribute
        {
            public override IEnumerable<object[]> GetData()
            {
                yield return new object[] { StringTo<int[]>("[9,12,5,10,14,3,10]").ToArray(), 10, StringTo<int[]>("[9,5,3,10,10,12,14]").ToArray() };
                yield return new object[] { StringTo<int[]>("[-3,4,3,2]").ToArray(), 2, StringTo<int[]>("[-3,2,4,3]").ToArray() };
            }
        }
        [Data]
        public int[] PivotArray(int[] nums, int pivot)
        {
            List<int> preList = new List<int>();
            List<int> equList = new List<int>();
            List<int> aftList = new List<int>();
            for (int i = 0; i < nums.Length; i++)
            {
                if (nums[i] == pivot) equList.Add(nums[i]);
                else if (nums[i] > pivot) aftList.Add(nums[i]);
                else preList.Add(nums[i]);
            }
            List<int> ans = new List<int>();
            ans.AddRange(preList);
            ans.AddRange(equList);
            ans.AddRange(aftList);
            return ans.ToArray();
        }
    }
}