# [wwm.LeetCodeHelper](https://gitee.com/wwmin/wwm.leetcode.helper.git)
仓库地址：[https://gitee.com/wwmin/wwm.leetcode.helper](https://gitee.com/wwmin/wwm.leetcode.helper)
# 1. 说明
wwm.LeetCodeHelper是一款帮助在本地用C#做LeetCode题的一个库，具有自动拉取题生成csharp文件，自动生成测试用例，自动完成测试对比等等的功能。

- 适用语言范围：C#
- 使用LeetCode版本：国内版

# 2. 安装
- [.NET CLI](https://www.nuget.org/packages/wwm.LeetCodeHelper)
```
dotnet add package wwm.LeetCodeHelper --version 0.8.6
```

- [Package Manager](https://www.nuget.org/packages/wwm.LeetCodeHelper)
```
Install-Package wwm.LeetCodeHelper -Version 0.8.6
```

# 3. 使用方式：
创建控制台应用程序
只需要创建一次此控制台应用程序，后面的题库内容都是一个一个类文件
在program.cs文件中添加如下内容
```c#
//方式一: 方便自定义刷题
using Microsoft.Extensions.Configuration;
#region 初始化
//添加secrets.json 然后从中读取登录信息
//方法一:
//步骤1: 添加依赖 Microsoft.Extensions.Configuration
//步骤2: 右键项目然后选择管理用户机密,然后在打开的secrets.json中添加
/*
{
"user_password_login": {
    "email": "your email",
    "password": "your password"
  },
  "cookie_login": {
    "LEETCODE_SESSION": "your leetcode_session cookie",
  }
}
*/
// 注意如果项用cookie登录则填充"cookie_login",如果使用用户名密码登录则填充"user_password_login",建议使用cookie，用户名密码模式需要用到模拟登录，且需要下载无头浏览器，配置起来稍微麻烦一些。
//方法二:
//步骤1: 添加依赖 Microsoft.Extensions.Configuration
//步骤2: 启用机密,使用.net cli命令 `dotnet user-secrets init`
//步骤3: 设置机密, `dotnet user-secrets set "cookie_login:LEETCODE_SESSION" "your cookie"`
//        或者设置用户名密码 `dotnet user-secrets set "user_password_login:email" "your email"` `dotnet user-secrets set "user_password_login:password" "your password"`
//步骤4: 查看 `dotnet user-secrets list`
// 更多dotnet cli 可参考:https://docs.microsoft.com/zh-cn/aspnet/core/security/app-secrets?view=aspnetcore-6.0&tabs=windows

var config = new ConfigurationBuilder()
    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
    .AddUserSecrets<Program>().Build();
var config_cookie = config.GetRequiredSection("cookie_login:LEETCODE_SESSION").Value;
if (config_cookie != null)
{
    await LeetCodeHelper.InitAsync(config_cookie);
}
else
{
    var config_email = config.GetRequiredSection("user_password_login:email")?.Value;
    var config_password = config.GetRequiredSection("user_password_login:password")?.Value;
    if (string.IsNullOrEmpty(config_email) || string.IsNullOrEmpty(config_password))
    {
        throw new Exception("cookie或者email/password 其中之一是必须的，建议使用cookie");
    }
    await LeetCodeHelper.InitAsync(config_email, config_password);
}
#endregion
#if !Release  //切换此处的开关可在自定义刷题和每日一题之间切换
string url = "";
if (string.IsNullOrEmpty(url))
{
    TestResultHelper.InvokeAllTest();
}
else
{
    await LeetCodeHelper.GetQuestionAsync(url);
}
#else
//方式二: 方便刷每日一题
await LeetCodeHelper.GetTodayQuestionOrInvokeTestAsync();
#endif
Console.Write("按任意键退出...");
Console.ReadKey();
```
# 4. 初始化用户信息
为了安全起见，建议用户将自己的`cookie` `LEETCODE_SESSION`值存储到`secrets.json`文件中，这样将自己的代码库上传到git远程仓库就不会担心自己的leet-code信息被盗取了

具体说明：参考上段代码中的注释部分

> 如何获取LEETCODE_SESSION?

> 获取leetcode网站cookie方法：
> 登录leetcode网站后，打开控制台，找到控制台的Application面板在左侧的Storage项中找到Cookies然后选中`https://leetcode.cn`
右侧有出现该网站的cookie目录，寻找到`LEETCODE_SESSION`选中后，复制后面的Value值即可。

# 5. 刷题方式：1.自定义url 2.每日一题
## 5.1 自定义url
只需要将 `#if !Release`中Release前加个!即可，这样在Debug模式下系统就执行此处的代码
## 5.2 每日一题模式
与方式1相反，去掉Release前的!，在Debug模式下系统就执行此处的代码

当然这里的方式1和2的组织模式可以自由发挥。

# 6. 获取题目
在选择自定义url模式刷题时，获取题目需要添加url内容
```
string url = "https://leetcode.cn/problems/two-sum/"
```
然后F5运行，
控制台中内容如下：
```
已从接口获取数据
将数据写入文件:two-sum.txt
文件已写入：D:\work\learn\dotnet\wwm.LeetCodeHelper\wwm.LeetCodeHelper.Test\Content\two-sum.txt
文件名已拷贝到剪贴板: two-sum.txt
文件已写入：D:\work\learn\dotnet\wwm.LeetCodeHelper\wwm.LeetCodeHelper.Test\Solutions\two-sum.cs
文件名已拷贝到剪贴板: two-sum.cs
按任意键退出...
```
文件名已经复制到了剪贴板中，在Visual Studio中按`Ctrl+Shift+T`快捷键弹出框中，粘贴文件名后查找到回车即可，此处为了节省文件变多后查找刚下载的文件的时间

内容如下：
```C#
namespace Solutions;
/// <summary>
/// 1556. 通过翻转子数组使两个数组相等
/// difficulty: Easy
/// </summary>
public class CanBeEqualSolution : ITest
{
    private class Data : DataAttribute
    {
        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[1,2,3,4]").ToArray(), StringTo<int[]>("[2,4,1,3]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[7]").ToArray(), StringTo<int[]>("[7]").ToArray(), true };
            yield return new object[] { StringTo<int[]>("[3,7,9]").ToArray(), StringTo<int[]>("[3,7,11]").ToArray(), false };
        }
    }

    [Data]
    public bool CanBeEqual(int[] target, int[] arr)
    {
        return default;
    }
}
```
会在题内容上面加上测试用例，
如果是自定义刷题模式时，将url置空后就会进入执行实现ITest接口的类，
如果是每日刷题，会自动判断题目是否已经下载，如果已下载则进入执行实现ITest的接口类。

# 7. 运行测试
编写你的算法代码
```c#
using wwm.LeetCodeHelper.Servers;

namespace Solutions;
/// <summary>
/// 1. 两数之和
/// difficulty: Easy
/// https://leetcode.cn/problems/two-sum/
/// </summary>
public class TwoSumSolution : ITest
{
    private class Data : DataAttribute
    {
        public Data()
        {
            IgnoreOrder = true;
        }

        public override IEnumerable<object[]> GetData()
        {
            yield return new object[] { StringTo<int[]>("[2,7,11,15]").ToArray(), 9, StringTo<int[]>("[0,1]").ToArray() };
            yield return new object[] { StringTo<int[]>("[3,2,4]").ToArray(), 6, StringTo<int[]>("[1,2]").ToArray() };
            yield return new object[] { StringTo<int[]>("[3,3]").ToArray(), 6, StringTo<int[]>("[0,1]").ToArray() };
        }
    }

    [Data]
    public int[] TwoSum(int[] nums, int target)
    {
        int[] o = new int[2];
        int i, j, k = 0;
        for (i = 0; i < nums.Length; i++)
        {
            for (j = i + 1; j < nums.Length; j++)
            {
                if ((nums[i] + nums[j]) == target)
                {
                    o[0] = i;
                    o[1] = j;
                    k = 1;
                }
            }
            if (k == 1) break;

        }
        return o;
    }
}
```

F5执行，结果如下：
```
------->TwoSumSolution
TwoSum: 执行结果正确: [0,1]
TwoSum: 执行结果正确: [1,2]
TwoSum: 执行结果正确: [0,1]
_______测试结果: 3/3 ,耗时:97ms________

按任意键退出...
```


如果执行中有错误答案，会给出预期值和运行结果值,如下
```
------->TwoSumSolution
TwoSum: 执行结果正确: [0,1]
TwoSum: 执行结果错误.
  预期值:[1,2]
  结果值:[0,0]
TwoSum: 执行结果正确: [0,1]
_______测试结果: 2/3 ,耗时:82ms________

按任意键退出...
```

控制台中也会有颜色提示，正确的为绿色，错误的为红色。

当测试完成后，需要将类的继承ITest去掉，这样下次就不会测试该类了。

该库暂时仍在完善中，如遇到问题可提issue。


# 许可证 / License

[wwm.LeetCodeHelper](https://gitee.com/wwmin/wwm.leetcode.helper.git) 采用 [Apache License 2.0](https://gitee.com/wwmin/wwm.leetcode.helper/raw/master/LICENSE) 开源许可证。

[wwm.LeetCodeHelper](https://gitee.com/wwmin/wwm.leetcode.helper.git) uses the [Apache License 2.0](https://gitee.com/wwmin/wwm.leetcode.helper/raw/master/LICENSE) open source license.

>作者：wwmin
>微信公众号: DotNet技术说
>本文链接：[https://www.jianshu.com/p/a6de22968955](https://www.jianshu.com/p/a6de22968955)
>评论和私信会在第一时间回复。转载请注明出处！
>如果您觉得文章对您有帮助，关注点赞，您的鼓励是博主的最大动力！